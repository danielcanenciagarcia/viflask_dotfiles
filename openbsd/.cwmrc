
# Status bar
#ignore termbar
#autogroup 0 termbar

# Sticky groups: uncomment to enable worskpace-like behavior
sticky                  yes

# Default font
#fontname                "spleen:pixelsize=14:bold"

# Border
borderwidth		4
color activeborder      '#eeeeec'               # Set the color of the active border
color inactiveborder    '#1c1c1c'               # Set the color of the inactive border
color urgencyborder     '#48c6ff'               # Set the color of the border of a window indicating urgency
color groupborder       '#215d9c'               # Set the color of the border while grouping a window
color ungroupborder     '#ff005b'               # Set the color of the border while ungrouping a window

# Menu colors
color font              '#bcbdbc'               # Set menu font color
color selfont           '#eeeeec'               # Set font color for selected menu item
color menubg            '#1c1c1c'               # Set menu background color
color menufg            '#215d9c'               # Set menu foreground color

#Gaps (top, bottom, left, right)
gap                     32 8 8 8
# Leave a gap when snaping windows
snapdist		8

# Size of manually tiled Windows in %
htile                   50
vtile                   60
 
# How many pixels a window gets moved by
moveamount              40

# Define special commands
command term            alacritty
command xlock           xlock
command suspend		zzz
command screenshot	"scrot -s ~/Images/%b%d::%H%M%S.png"
command screenshotcpy	"scrot -os /tmp/shot.png -e 'xclip -selection clipboard -t image/png -i /tmp/shot.png'"
command shutdown	"doas /bin/ksh -c 'shutdown -p now'"
#command quit		quit
command firefox		firefox
command chrome		chrome
command terminal	alacritty
command mail		geary

# Key Bindings (4: super key, M: alt, S: shift)
# Remove all default keybindings
unbind-key      all

bind-key 4-Return	terminal
bind-key 4-space	terminal
bind-key 4-f	browser

# Menus
bind-key 4-w	menu-window
bind-key 4-W	menu-window-hidden
bind-key 4-D	menu-cmd
bind-key 4-d	menu-exec
bind-key 4-g	menu-group
bind-key 4-S	menu-ssh

# Power control
bind-key 4S-r	restart
bind-key 4S-q	quit
bind-key 4S-i	zzz
bind-key 4S-I	lock

# Volume control
bind-key 4-equal	"sndioctl output.level=+0.2"
bind-key 4S-equal	"sndioctl output.mute=1"
bind-key 4-minus	"sndioctl output.level=-0.2"
# Backlight control
bind-key 4S-b		"xbacklight -dec 10 -time 0"
bind-key 4-b		"xbacklight -inc 10 -time 0"

# Keyboard encoding
bind-key 4-i		"sh /home/daniel/ports/viflask_dotfiles/openbsd/scripts/switchkb.sh"

# Toggle visibility of group n, hiding all other groups
bind-key 4-1    group-only-1
bind-key 4-2    group-only-2
bind-key 4-3    group-only-3
bind-key 4-4    group-only-4
bind-key 4-5    group-only-5
bind-key 4-6    group-only-6
bind-key 4-7    group-only-7
bind-key 4-8    group-only-8
bind-key 4-9    group-only-9
 
# Toggle group n's visibility, without hiding active group
bind-key M-1   group-toggle-1
bind-key M-2   group-toggle-2
bind-key M-3   group-toggle-3
bind-key M-4   group-toggle-4
bind-key M-5   group-toggle-5
bind-key M-6   group-toggle-6
bind-key M-7   group-toggle-7
bind-key M-8   group-toggle-8
bind-key M-9   group-toggle-9

# Toggle visibility of all groups
bind-key 4-a    group-toggle-all

# Move window to group n
bind-key 4S-1   window-movetogroup-1
bind-key 4S-2   window-movetogroup-2
bind-key 4S-3   window-movetogroup-3
bind-key 4S-4   window-movetogroup-4
bind-key 4S-5   window-movetogroup-5
bind-key 4S-6   window-movetogroup-6
bind-key 4S-7   window-movetogroup-7
bind-key 4S-8   window-movetogroup-8
bind-key 4S-9   window-movetogroup-9
 
# Add focused window to current group
bind-key 4-G    window-group
bind-key 4-s	window-stick

# What follows is rather self-explanatory
bind-key 4-q    window-close
bind-key 4-u    window-hide
bind-key 4-y	window-lower
bind-key 4S-y	window-raise
 
bind-key 4-f    window-fullscreen
bind-key 4-x    window-maximize
bind-key 4S-m	window-hmaximize
bind-key 4-m	window-vmaximize
bind-key 4S-t	window-htile
bind-key 4-t	window-vtile
bind-key 4-c	"/usr/local/bin/bash -c $HOME/isolate_windows"
 
# Cycle/reverse cycle through windows
bind-key 4-j    window-cycle
bind-key 4-k    window-rcycle
bind-key M-Tab	window-cycle
bind-key 4-Tab	window-cycle
bind-key 4S-Tab	window-rcycle
bind-key 4-h	window-cycle-ingroup
bind-key 4-l	window-rcycle-ingroup

# Move windows
bind-key MS-h   window-move-left
bind-key MS-j   window-move-down
bind-key MS-k   window-move-up
bind-key MS-l   window-move-right

# Resize windows
bind-key 4S-h	window-resize-left
bind-key 4S-j	window-resize-down
bind-key 4S-k	window-resize-up 
bind-key 4S-l	window-resize-right

# Snap windows
bind-key M-h   window-snap-left
bind-key M-j   window-snap-down
bind-key M-k   window-snap-up
bind-key M-l   window-snap-right

# Mouse bindings
bind-mouse SM-1	window-resize
bind-mouse SM-4	window-lower
bind-mouse SM-5	window-raise

# Autogroup some apps
autogroup 1	"alacritty,Alacritty"
autogroup 1	"xterm,XTerm"
autogroup 2	"mupdf,MuPDF"
autogroup 2	"mutt"
autogroup 2	"geary,Geary"
autogroup 2	"libreoffice"
autogroup 3	"Navigator,firefox-default"
autogroup 3	"Navigator,Firefox"
autogroup 3	"Navigator,Firefox-esr"
autogroup 3	"palemoon,Palemoon"
autogroup 3	"chromium,Chromium"
autogroup 4	"spotify,Spotify"

# CWM
wm fwm fwm
wm twm twm
bind-key 4-E	menu-exec-wm

