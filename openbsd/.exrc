""" Traditional Vi .exrc
""" 

" Source common .sexrc
source $HOME/.sexrc

" Additional (vim incompatible) options
" Spaces instead of tabs
"set expandtab
set noerrorbell
" - Set window name to file's namels
set windowname
" - Make searches incremental
set searchincr
" - Make REs case-insensitive
set iclower
" - Extended REs
set extended
" - Enable left and right scrolling
set leftright

"" Maps
map uexpand : %!expand -t 4
" Suspend vi session
map ^V ^V  :w^M:suspend^M

" Auto Insert / Replace
"map uz :.,+4s/^/#4>

" Multiline (yank a word using yw before)
"  !!! Improve it by using the v/pattern/commands
"	1. "myw (yank word into buffer m) or "myt<space>
"	2. Move to the desired line and column
"	2. Execute command
"	4. Use @@ or @* to repeat for the next line
map um h:kzo"mpjB"xddk`z@x

" Auto Comments
map uc# }{E:kc}$B:kC{:'c,'Cs/^/#'c,'C>>
map uc! }{E:kc}$B:kC{:'c,'Cs/^/!'c,'C>>
map uc2! }{E:kc}$B:kC{:'c,'Cs/^/!!'c,'C>>
map uc" }{E:kc}$B:kC{:'c,'Cs/^/"'c,'C>>
map uc// }{E:kc}$B:kC{:'c,'Cs/^/\/\/'c,'C>>
map uc" }{:kc}:kC{EO"""'C$Bo"""
" Alt mapping
map " }{:kc}:kC{EO"""'C$Bo"""

" Edit blocks
map ue0 }$B:'',.s/^([[:space:]]*)/\1
map ue$ }$B:'',.s/$([[:space:]]*)/\1

"" Tags


"" Abbreviations


"" Most frequent keys
"" 	- To start: eE, fF, ^, J, /?, {}, S, s, r
""		    C-^, :n[N] and :prev[Prev] (files as arguments)
""	- yt<space> to yank a whole word
""	- m or m[ove] command
""	- C-T to shift right as much as needed
""	- C-D to erase a tab (and spaces), 0C-D and ^C-D
""	- C-H/del to erase a char, C-W to erase last word
""	- di b[uffers] and di s[creens]
""	- '' to move before and after a movement occurs
""	- '-' to move to the first non-blank char of prev line

" tT and fF and ; and comma, wW, bB and eE, <space>, 0, ^, $
" - (move first char of the prev line)
" (,) and {,} and [,], %
" G, / and ?, d and dd and D, J
" c and C, r and R, s and S, ~ (reverse case)
" mX and gX and 'X and `X
" u and U (or u and . and C-r), repeat .
" C-f and C-b, C-d and C-u, H L and M
" > and <
" C-^ (edit alternate file)
