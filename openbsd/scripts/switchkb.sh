#!/bin/sh
#
# Switch keyboard layout
#
# To Do:
#	- Query kbd variant to introduce a third opt

PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin"

_dkbd="us"  # Default layout
_akbd="es"  # Alternate layout

[[ "$(setxkbmap -query | awk '/^layout:/ { print $2 }')" == "$_dkbd" ]] \
  && setxkbmap -layout $_akbd -option "ctrl:swapcaps" \
  || setxkbmap -layout $_dkbd -option "ctrl:swapcaps"

  #|| setxkbmap -layout $_dkbd -variant "colemak"
  #&& setxkbmap -layout $_dkbd -variant "colemak_dh_wide_iso" \
exit 0
#EOF
