# Remove fish default greeting
#set --erase fish_greeting
set -U fish_greeting ""


# See: https://gist.github.com/denji/9530128
# Theme: https://github.com/matchai/spacefish
