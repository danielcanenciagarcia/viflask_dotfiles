
local wezterm = require("wezterm")

local config = {

    check_for_updates = false,
    default_prog = {"/bin/bash"},
    --font = wezterm.font_with_fallback({"Koruri, Koruri"},{foreground="#b0b0b0"}),
    font_size = 10.5,
    dpi = 96.0,
    --font_dirs = {"/usr/share/fonts/iosevka-term"},
    --font_rules = {
    --    {
    --        italic = true,
    --        font = wezterm.font_with_fallback({"Iosevka Term SS09 Light Italic"},{foreground="#b0b0b0"}),
    --    },
    --    {
    --        intensity = "Bold",
    --        font = wezterm.font_with_fallback({"Iosevka Term SS09 Medium"}, {foreground="#ffffff"}),
    --    },
    --    {
    --        italic = true,
    --        intensity = "Bold",
    --        font = wezterm.font_with_fallback({"Iosevka Term SS09 Medium Italic"}, {foreground="#ffffff"}),
    --    },
    --    {
    --        intensity = "Half",
    --        font = wezterm.font_with_fallback({"Iosevka Term SS09 Extralight"}, {foreground="#c0c0c0"}),
    --    },
    --   {
    --       italic = true,
    --        intensity = "Half",
    --        font = wezterm.font_with_fallback({"Iosevka Term SS09 Extralight Italic"}, {foreground="#c0c0c0"}),
    --    },
    --},
 
    font_shaper = "Harfbuzz",
    --font_shaper = "Allsorts",
    --font_antialias = "Subpixel",
    --harfbuzz_features = {"kern", "liga", "clig", "calt"},
    --font_antialias = "Greyscale",
    --font_antialias = "None",
    --font_hinting = "Full",
    --font_hinting = "Vertical",
    --font_hinting = "VerticalSubpixel",
    --font_hinting = "None",


    -- Disable default keybindings
    disable_default_key_bindings = true,

    --launch_menu = {},
    leader = { key="w", mods="ALT" },


    keys = {

        { key = "S", mods = "LEADER",       action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
        { key = "s", mods = "LEADER",       action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
        { key = "z", mods = "LEADER",       action="TogglePaneZoomState" },
        { key = "[", mods = "CTRL",         action="ActivateCopyMode" },

        { key = "Insert", mods = "SHIFT",   action=wezterm.action{PasteFrom="PrimarySelection"}},
        { key = "Insert", mods = "CTRL",    action=wezterm.action{CopyTo="PrimarySelection"}},
        { key = "w", mods = "CTRL",         action=wezterm.action{CopyTo="Clipboard"}},
        { key = "y", mods = "CTRL",         action=wezterm.action{PasteFrom="Clipboard"}},


        -- Create a new tab in the same domain as the current tab
        {key = "t", mods="LEADER", action=wezterm.action{SpawnTab="CurrentPaneDomain"}},
        -- Create a new tab in the default domain
        --{key = "t", mods="ALT", action=wezterm.action{SpawnTab="DefaultDomain"}},
        { key = "q", mods = "LEADER", action=wezterm.action{CloseCurrentTab={confirm=true}}},
        -- move tabs
        {key="LeftArrow", mods="LEADER", action=wezterm.action{MoveTabRelative=-1}},
        {key="RightArrow", mods="LEADER", action=wezterm.action{MoveTabRelative=1}},
        -- move focus between tabs
        {key="LeftArrow", mods="ALT", action=wezterm.action{ActivateTabRelative=-1}},
        {key="RightArrow", mods="ALT", action=wezterm.action{ActivateTabRelative=1}},
        {key="1", mods="ALT", action=wezterm.action{ActivateTab=0}},
        {key="2", mods="ALT", action=wezterm.action{ActivateTab=1}},
        {key="3", mods="ALT", action=wezterm.action{ActivateTab=2}},
        {key="4", mods="ALT", action=wezterm.action{ActivateTab=3}},
        {key="5", mods="ALT", action=wezterm.action{ActivateTab=4}},
        {key="6", mods="ALT", action=wezterm.action{ActivateTab=5}},
        {key="7", mods="ALT", action=wezterm.action{ActivateTab=6}},
        {key="8", mods="ALT", action=wezterm.action{ActivateTab=7}},
        {key="9", mods="ALT", action=wezterm.action{ActivateTab=8}},
        {key="0", mods="ALT", action=wezterm.action{ActivateTab=-1}},
	{key="LeftArrow", mods="CTRL", action=wezterm.action{AdjustPaneSize={"Left", 1}}},
	{key="RightArrow", mods="CTRL", action=wezterm.action{AdjustPaneSize={"Right", 1}}},
	{key="DownArrow", mods="CTRL", action=wezterm.action{AdjustPaneSize={"Down", 1}}},
	{key="UpArrow", mods="CTRL", action=wezterm.action{AdjustPaneSize={"Up", 1}}},

	{key="f", mods="CTRL", action=wezterm.action{Search={CaseInSensitiveString=""}}},
	{key="f", mods="CTRL|SHIFT", action=wezterm.action{Search={CaseSensitiveString=""}}},
	
	{key="e", mods="LEADER|ALT", action="QuickSelect"}

        --{ key = "x", mods="LEADER", action=wezterm.action{CloseCurrentPane={confirm=true}}},

    },

    set_environment_variables = {
      COLORTERM="truecolor",
    },

   colors = {
      foreground = "#ebdbb2",
      background = "#282828",
      cursor_bg = "#ebdbb2",
      cursor_fg = "#ebdbb2",
      cursor_border = "#ebdbb2",
      scrollbar_thumb = "#504945",
      ansi = {"#282828", "#cc241d", "#98971a", "#d79921", "#458588", "#b16286", "#689d6a", "#ebdbb2"},
      brights = {"#7c6f64", "#fb4934", "#b8bb26", "#fabd2f", "#83a598", "#d3869b", "#8ec07c","#fbf1c7"},
      tab_bar = {
         background = "#1d2021",
         active_tab = {
            bg_color = "#076678",
            fg_color = "#ebdbb2",
         },
         inactive_tab = {
            bg_color = "#458588",
            fg_color = "#3c3836",
         },
         inactive_tab_hover = {
            bg_color = "#83a598",
            fg_color = "#3c3836",
         },
      }
   },


    -- Colorscheme
    color_scheme_dirs = {"~/home/ports/viflask_dotfiles/shells/wezterm/schemes"},
    color_scheme = "Japanesque"
}


return config
