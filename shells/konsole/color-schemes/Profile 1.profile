[Appearance]
ColorScheme=Wbug
DimmValue=37
Font=Hack,10.5,-1,5,50,0,0,0,0,0
TabColor=160,160,160

[Cursor Options]
CursorShape=2

[General]
DimWhenInactive=true
Name=Profile 1
Parent=FALLBACK/

[Interaction Options]
TripleClickMode=0

[Keyboard]
KeyBindings=linux

[Terminal Features]
BlinkingCursorEnabled=true
