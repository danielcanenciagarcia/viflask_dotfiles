
(deftheme smart-mode-line-gruvbox-dark
  "Gruvbox dark theme for smart-mode-line.")

(custom-theme-set-faces
 'smart-mode-line-gruvbox-dark
 '(mode-line-inactive ((t :background "#282C34" :box (:line-width 3 :color "#282C34"))))
 '(mode-line     ((t :background "#2C323C" :box (:line-width 3 :color "#2C323C"))))
 '(sml/global    ((t :inherit font-lock-preprocessor-face)))
 '(sml/filename  ((t :inherit mode-line-buffer-id)))
 '(sml/prefix    ((t :inherit (font-lock-variable-name-face sml/global))))
 '(sml/read-only ((t :inherit (font-lock-type-face sml/not-modified))))
 `(sml/git       ((t :inherit (sml/read-only sml/prefix))))
 '(sml/modes     ((t :foreground nil :inherit sml/filename :weight normal)))
 ;;`(sml/discharging         ((t :inherit sml/global :foreground "Red")))
 `(sml/time      ((t :inherit sml/global :foreground "Red")))

 ;; Helm
 '(helm-candidate-number ((t :background "#2C323C"))))






;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'smart-mode-line-gruvbox-dark)
;;; smart-mode-line-atom-one-dark-theme.el ends here.
