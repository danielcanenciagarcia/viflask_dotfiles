
;; Modeline
(add-hook 'after-init-hook #'doom-modeline-mode)


(use-package doom-modeline
  ;; Config
  :hook (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-buffer-file-name-style 'auto
        ;; Manage icons, show icons everywhere
        doom-modeline-icon (display-graphic-p)
        doom-modeline-major-mode-icon t
        doom-modeline-major-mode-color-icon t
        doom-modeline-buffer-state-icon t
        doom-modeline-buffer-modification-icon t
        ;; General control of buffer and nodes
        doom-modeline-unicode-fallback nil
        doom-modeline-minor-modes t
        doom-modeline-enable-word-count nil
        doom-modeline-indent-info nil
        doom-modeline-buffer-encoding t
        ;; Warning message size limitation
        doom-modeline-number-limit 10
        ;; Limit git messages (branch icon)
        doom-modeline-vcs-max-length 10
        ;; Display things in the modeline
        doom-modeline-workspace-name t
        doom-modeline-persp-name t
        doom-modeline-display-default-persp-name nil
        doom-modeline-persp-icon t
        doom-modeline-lsp t
        ;; Github notifications (pkg ghub)
        doom-modeline-github nil
        ;; Interval checking github
        doom-modeline-github-interval (* 30 60)
        ;; Whether display the modal state icon.
        ;; Including `evil', `overwrite', `god', `ryo' and `xah-fly-keys', etc.
        doom-modeline-modal-icon t
        ;; Whether display the environment version.
        doom-modeline-env-version t
        doom-modeline-env-python-executable "python3"
        doom-modeline-env-load-string "???"
        ;; built-in `project' on 26+
        doom-modeline-project-detection 'project
        ;; or `find-in-project' if it's installed
        ;;doom-modeline-project-detection 'ffip

        doom-modeline-height 3
  )

  ;;(doom-modeline-def-modeline
  ;;)

  ;; setq-default for all nodes
  ;;(setq-default mode-line-format
              ;;(list
              ;;)
  ;;)

  (column-number-mode t)
  ;;;;;;;;;;;;;;;;;;;;;
  ;; Face Attributes ;;
  ;;(set-face-attribute 'mode-line nil :family "Noto Sans" :height 100)
  ;;(set-face-attribute 'mode-line-inactive nil :family "Noto Sans" :height 100)
  (set-face-attribute 'mode-line nil
    :background "#353644"
    :foreground "white"
    :box '(:line-width 4 :color "#353644")
    :overline nil
    :underline nil)

  (set-face-attribute 'mode-line-inactive nil
    :background "#565063"
    :foreground "white"
    :box '(:line-width 4 :color "#565063")
    :overline nil
    :underline nil)


  ;; Justify mode-name to the right
  '(:eval (propertize
         " " 'display
         `((space :align-to (- (+ right right-fringe right-margin)
                               ,(+ 3 (string-width mode-name)))))))

)


;;(defun setup-custom-doom-modeline ()
;;   (doom-modeline-set-modeline 'my-simple-line 'default))
;;(add-hook 'doom-modeline-mode-hook 'setup-custom-doom-modeline)



  (column-number-mode t)
  ;;;;;;;;;;;;;;;;;;;;;
  ;; Face Attributes ;;
  ;;(set-face-attribute 'mode-line nil :family "Noto Sans" :height 100)
  ;;(set-face-attribute 'mode-line-inactive nil :family "Noto Sans" :height 100)
  (set-face-attribute 'mode-line nil
    :background "#353644"
    :foreground "white"
    :box '(:line-width 4 :color "#353644")
    :overline nil
    :underline nil)

  (set-face-attribute 'mode-line-inactive nil
    :background "#565063"
    :foreground "white"
    :box '(:line-width 4 :color "#565063")
    :overline nil
    :underline nil)


  ;; Justify mode-name to the right
  '(:eval (propertize
         " " 'display
         `((space :align-to (- (+ right right-fringe right-margin)
                               ,(+ 3 (string-width mode-name)))))))

)

