#Set background image
#feh --bg-scale /home/daniel/Pictures/.png

#Set cursor style so it isn't just an "X"
xsetroot -cursor_name left_ptr

#Use your keybindings; must have xbindkeys installed
xbindkeys &

#Start Gkrellm
# gkrellm &

# Launch bar
#xbattbar -a

#Start evilwm
evilwm
