-- Run: xmobar /usr/ports/viflask_dotfiles/WMs/xmobar/xmobar.hs -b

-- This is setup for dual 1920x1080 monitors, with the right monitor as primary
Config {
--    position = Static { xpos = 0 , ypos = 0, width = 1806, height = 16 },
    position = Static { xpos = 0 , ypos = 0, width = 1920, height = 22 },
    borderColor = "ivory",
    font = "xft:Fixed-10",
    bgColor = "#000000",
    fgColor = "#ffffff",
    --border = FullB,

   -- general behavior
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      True    -- show on all desktops
   , overrideRedirect = True    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     True    -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled)

    commands = [
        Run MultiCpu ["-t","Cpu: <total0>% ","-L","30","-H","60","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC","-w","3"] 10,
        Run Memory ["-t","Mem: <usedratio>%","-H","8192","-L","4096","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Swap ["-t","Swap: <usedratio>%","-H","1024","-L","512","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Network "enp7s0" ["-t","Net: <rx>, <tx>","-H","200","-L","10","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Date "%a %b %_d %l:%M" "date" 10,

	-- Battery information. This is likely to require some customization
	-- based upon your specific hardware. Or, for a desktop you may want
	-- to just remove this section entirely.
	--	Run Battery  [
	--	"-t", "<acstatus>: <left>% - <timeleft>",
	--	"--",
	--	--"-c", "charge_full",
	--	"-O", "AC",
	--	"-o", "Bat",
	--	"-h", "green",
	--	"-l", "red"
	--	] 10,
        -- Thermal
        --Run ThermalZone 0 ["-t","<id>: <temp>C"] 30,
	--Run Com "/bin/bash" ["-c", "echo `xbacklight -get | grep -oE '^.[0-9]{0,3}'`%"]  "mybright" 1,
	Run Com "/bin/sh" ["/home/daniel/ports/viflask_dotfiles/WMs/xmobar/conky_get_volume.sh"] "myvolume" 10,
	Run Com "/bin/sh" ["/home/daniel/ports/viflask_dotfiles/WMs/xmobar/get_gpu_temp.sh"] "mygputemp" 10,
        Run Com "/bin/sh" ["/home/daniel/ports/viflask_dotfiles/WMs/xmobar/get_cpu_temp.sh"] "mycputemp" 10
        --Rum Com "/bin/sh" ["sensors | sed -n '5p' | grep C | tr -s ' ' | cut -d ' ' -f 2 | sed -s 's/+//g'"] "my_temp2" 30
        --Run StdinReader
	--Run Com "/bin/sh" ["-c", "ls"] "workspaces" 3600

    ],
    sepChar = "%",
    alignSep = "}{",
    --template = "} %multicpu%   %memory%   %swap%   %enp7s0%  Temps: <fc=#FFFFCC>%mygputemp%</fc>, <fc=#FFFFCC>%mycputemp%</fc>   Volume: <fc=#FFFFCC>%myvolume%</fc>     |     <fc=#FFFFCC>%date%</fc> {"
    template = "} Volume: <fc=#FFFFCC>%myvolume%</fc>    |     <fc=#FFFFCC>%date%</fc> {"

}
