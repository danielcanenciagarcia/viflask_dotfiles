#!/bin/bash

FONT='fixed-10'
exec conky -d -c "nar" | dzen2 -fg "#666666" -bg "#151515" -ta c -e -p -w 2000 -h 30 -x 0 -y 0 -fn $FONT &
exit 0
