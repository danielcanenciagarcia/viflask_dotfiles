#!/bin/sh

monitor=${1:-0}
geometry=( $(herbstclient monitor_rect "$monitor") )
if [ -z "$geometry" ] ;then
    echo "Invalid monitor $monitor"
    exit 1
fi
# geometry has the format: WxH+X+Y
x=${geometry[0]}
y=${geometry[1]}
width=${geometry[2]}
height=14
#font="Meslo LG M DZ Bold Italic Nerd Font Complete:pixelsize=14"
#font="NanumMyeongjoBold:pixelsize=14"
font="fixed"
bgcolor='#151515'



####
# Try to find textwidth binary.
# In e.g. Ubuntu, this is named dzen2-textwidth.
if which textwidth &> /dev/null ; then
    textwidth="textwidth";
elif which dzen2-textwidth &> /dev/null ; then
    textwidth="dzen2-textwidth";
else
    echo "This script requires the textwidth tool of the dzen2 project."
    exit 1
fi
####



function uniq_linebuffered() {
    awk '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

herbstclient pad $monitor $height
{
    while true ; do
        date +'date ^fg(#505050)%a %d %b ^fg(#151515)%H:%M'
        sleep 1 || break
    done > >(uniq_linebuffered)  &
    childpid=$!
    herbstclient --idle
    kill $childpid
} 2> /dev/null | {
    TAGS=( $(herbstclient tag_status $monitor) )
    unset TAGS[${#TAGS[@]}]
    date=""
    song="nothing to see here"
    windowtitle="what have you done?"

    while true ; do
        # bordercolor="#151515"
        hintcolor="#ff8f00"
        separator="^fg(#141414)^ro(1x$height)^fg()"

        echo -n " "
        # draw tags
        for i in "${TAGS[@]}" ; do
            case ${i:0:1} in
                '#')
                    echo -n "^bg(#ff8f00)^fg(#141414)"
                    ;;
                '+')
                    echo -n "^bg(#808080)^fg(#141414)"
                    ;;
                ':')
                    echo -n "^bg(#404040)^fg(#141414)"
                    ;;
                '!')
                    echo -n "^bg(#ff8f00)^fg(#141414)"
                    ;;
                *)
                    echo -n "^bg()^fg()"
                    ;;
            esac
            echo -n "^ca(1,herbstclient focus_monitor $monitor && "'herbstclient use "'${i:1}'") '"${i:1} ^ca()"
            echo -n "$separator"
        done

        #echo -n "^p(_CENTER)^p(-$center_width)"

        #window_title="^fg(#917154)$st${windowtitle//^/^^}"
        #window_title_only=$(echo -n "$window_title" | sed 's.\^[^(]*([^)]*)..g')
        # get width of right aligned text.. and add some space..
        #center_width=$($textwidth "$font" "$window_title_only")
        #echo -n "$window_title"

        # small adjustments
        #battery=$(expr $(expr $(cat /sys/class/power_supply/BAT*/charge_now) \* 100) / $(cat /sys/class/power_supply/BAT*/charge_full))
        #mpd=$(pidof mpd)

	right="$separator ^bg(#ff8f00) ^fg(#000000)"

        #if [ "$mpd" != "" ] ;then
	#				right="$right $song $separator ^fg(#151515)"
        #fi

        #if [ "$battery" != "/" ] ;then
	#				right="$right $battery% $separator ^fg(#151515)"
        #fi


        right="$right $date $separator"
        #right_text_only=$(echo -n "$right")
        # get width of right aligned text.. and add some space..
        #right_width=$($textwidth "$font" "$right_text_only")
        echo -n "^p(_RIGHT)^p(-168)"
        echo -n "$right"
        echo

	# wait for next event
        read line || break
        cmd=( $line )
        # find out event origin
        case "${cmd[0]}" in
            tag*)
                TAGS=( $(herbstclient tag_status $monitor) )
                ;;
            date)
                date="${cmd[@]:1}"
                ;;
	    #focus_changed|window_title_changed)
            #    windowtitle="${cmd[@]:2}"
            #    ;;
            quit_panel)
                exit
                ;;
            reload)
                exit
                ;;
            #player)
            #    song=$(mpc current)
            #    ;;
        esac
    done
} 2> /dev/null | dzen2 -w $width -x $x -y $y -h $height -ta l -fn "$font" -bg "$bgcolor" -fg '#efefef'
