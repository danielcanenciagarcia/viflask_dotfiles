#!/usr/bin/env bash

# Execute this (e.g. from your autostart) to obtain basic key chaining like it
# is known from other applications like screen.
#
# E.g. you can press Mod1-i 1 (i.e. first press Mod1-i and then press the
# 1-button) to switch to the first workspace
#
# The idea of this implementation is: If one presses the prefix (in this case
# Mod1-i) except the notification, nothing is really executed but new
# keybindings are added to execute the actually commands (like use_index 0) and
# to unbind the second key level (1..9 and 0) of this keychain. (If you would
# not unbind it, use_index 0 always would be executed when pressing the single
# 1-button).

hc() { "${herbstclient_command[@]:-herbstclient}" "$@" ;}
Mod=Mod1

# Create the array of keysyms, the n'th entry will be used for the n'th
# keybinding
keys=(c d s Shift-s e Shift-r p)

# Build the commands to unbind the keys
unbind=(  )
for k in "${keys[@]}" Escape ; do
    unbind+=( , keyunbind "$k" )
done
resizekeys=($Mod-h $Mod-j $Mod-k $Mod-l)
for k in "${resizekeys[@]}" Escape ; do
    unbindresize+=( , keyunbind "$k" )
done
statuskeys=(t p x f d)
for k in "${statuskeys[@]}" Escape ; do
    unbindstatus+=( , keyunbind "$k" )
done


# Add the actual bind, after that, no new processes are spawned when using that
# key chain. (Except the spawn notify-send of course, which can be deactivated
# by only deleting the appropriate line)
hc keybind $Mod-a chain \
    '->' spawn notify-send "Alt-a Map..." \
    '->' keybind "${keys[0]}" chain "${unbind[@]}" , spawn "${TERMINAL:-xterm}" \
    '->' keybind "${keys[1]}" chain "${unbind[@]}" , close \
    '->' keybind "${keys[2]}" chain "${unbind[@]}" , split bottom 0.5 \
    '->' keybind "${keys[3]}" chain "${unbind[@]}" , split right  0.5 \
    '->' keybind "${keys[4]}" chain "${unbind[@]}" , explode \
    '->' keybind "${keys[5]}" chain "${unbind[@]}" , remove \
    '->' keybind "${keys[6]}" chain "${unbind[@]}" , spawn dmenu_run \
    '->' keybind Escape       chain "${unbind[@]}"



#var=(
#"floating \
#toggle"
#)
resizestep=0.04
var2="chain \
\"->\" spawn notify-end \"Resize Map...\" \
\"->\" keybind "${resizekeys[0]}" chain "${unbindresize[@]}" , resize left +$resizestep \
\"->\" keybind "${resizekeys[1]}" chain "${unbindresize[@]}" , resize down +$resizestep \
\"->\" keybind "${resizekeys[2]}" chain "${unbindresize[@]}" , resize up +$resizestep \
\"->\" keybind "${resizekeys[3]}" chain "${unbindresize[@]}" , resize right +$resizestep \
\"->\" keybind Escape             chain "${unbindresize[@]}""

# Resize Map
hc keybind $Mod-r $var2


# State Map
hc keybind $Mod-q chain \
    '->' spawn notify-end "Status Map..." \
    '->' keybind "${statuskeys[0]}" chain "${unbindstatus[@]}" , floating toggle \
    '->' keybind "${statuskeys[1]}" chain "${unbindstatus[@]}" , pseudotile toggle \
    '->' keybind "${statuskeys[2]}" chain "${unbindstatus[@]}" , fullscreen toggle \
    '->' keybind "${statuskeys[3]}" chain "${unbindstatus[@]}" , set_attr clients.focus.floating toggle \
    '->' keybind "${statuskeys[4]}" chain "${unbindstatus[@]}" , set_attr clients.focus.decorated toggle \
    '->' keybind Escape             chain "${unbindstatus[@]}"

