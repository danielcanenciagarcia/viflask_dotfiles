#!/bin/bash

herbstclient --idle '(focus_window|focus_changed|focus_frame|tag_changed|window_title_changed)' | while read hook name winid
do
  case $name in
    max)  herbstclient set_layout max ;;
    *)    case $hook in
            focus_window)   rpbarsend && focus_window & ;;
            focus_changed)  rpbarsend && set_border &
                            set_root &
                            ;;
            focus_frame)    rpbarsend && focus_frame & ;;
            tag_changed)    rpbarsend && focus_frame & ;;
            window_title_changed) rpbarsend & ;;
          esac
          ;;
  esac
done

