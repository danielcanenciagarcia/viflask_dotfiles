#lxsession &
pulseaudio --start
#redshift &
#test -f $HOME/.Xresources && xrdb -merge $HOME/.Xresources

# Set black background and cursor
xsetroot -solid "#000000" -cursor_name left_ptr

# Xrdb Xresources
xrdb ~/.Xresources

# Xscreensaver
# xscreensaver -no-splash &

# Wallpaper
feh --bg-scale /home/daniel/ports/viflask_dotfiles/customize/theming/wallpapers/nord.jpg
~/.fehbg &

# Set DPI
# Better flat acceleration (not compatible with resizing in wms)
#xinput set-prop  "HID-compliant Mouse Trust Gaming Mouse" "Coordinate Transformation Matrix" 0.7 0 0 0 0.7 0 0 0 1
# Mouse Acceleration
#xset m 2/2 0

# Keyboard acceleration
xset r rate 220 40

# Start ssh agent
#[ -z "$SSH_AUTH_SOCK" ] && eval "$(ssh-agent -s)"
#add keys  to ssh-agent
#sudo /bin/sh -c "ssh-add ~/.ssh/id_rsa_dcanencia"
#sudo /bin/sh -c "ssh-add ~/.ssh/id_rsa_danielcanenciagarcia"
#sudo /bin/sh -c "ssh-add ~/.ssh/id_rsa_viflask"
#sh /home/daniel/ports/viflask_dotfiles/init_files/ssh_add.sh

# Xmodmap
#[[ -f /usr/ports/viflask_dotfiles/init_files/Xmodmap ]] && xmodmap /usr/ports/viflask_dotfiles/init_files/Xmodmap
# God mode + normal mode
#sh /usr/ports/viflask_dotfiles/init_files/emacs.sh
#[[ -f /usr/ports/viflask_dotfiles/init_files/escape_xmodmap ]] && xmodmap /usr/ports/viflask_dotfiles/init_files/escape_xmodmap

# restore options with setxkbmap -option

# Sticky Keys
#/usr/bin/xkbset exp -bell -sticky -twokey -latchlock -accessx -feedback -stickybeep -led
#xkbset -bell -feedback sticky -twokey latchlock
# To disable in expr secons
#xkbset -bell -feedback sticky -twokey latchlock

# XBindKeys
xbindkeys -f ~/ports/viflask_dotfiles/WMs/keys/.xbindkeysrc &
# Fetch Mail
#fetchmail -d 500
#getmail

# Exec Picom
#picom --config ~/ports/viflask_dotfiles/.config/picom/picom.conf
# My bar (xmobar)
#xmobar /usr/ports/viflask_dotfiles/WMs/xmobar/xmobar.hs &
#~/.cabal/bin/xmobar /home/daniel/ports/viflask_dotfiles/WMs/xmobar/xmobar.hs &
