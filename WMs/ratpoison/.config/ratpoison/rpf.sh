#!/bin/sh
#
# $Id: rpf.sh,v 0.01.1 2013/11/04 00:34:29 emss Exp $
#
# Copyright (c) 2013 Edward M. S. Scholtz
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

# -START-[ MODIFIABLE VARIABLES ]----------------------------------- #
# Path to alternative ratpoison binary
ratpoison=""
# -END-[ MODIFIABLE VARIABLES ]------------------------------------- #

# -START-[ USEFUL FUNCTIONS ]--------------------------------------- #
# Run a ratpoison command
rpc() {
    ${ratpoison:-ratpoison} --command "$*"
}

# Output from ratpoison -c/--command fdump nicely formatted
# NOTE: NF-1 in awk(1) for statement to handle the empty last field caused by
#+ fdump output containing a trailing "," (comma)
frames() {
    rpc fdump |
    awk -F "," '
        {
        for (i = 1; i <= NF-1; i++)
                printf "%s\n", $i
        }
    '
}

# Current screen frame numbers
frame_numbers() {
    frames | awk '{ printf "%s\n", $3 }'
}

# Current screen frame numbers reversed
frame_numbers_reversed() {
    frame_numbers |
    awk '
    {
        frame_number[NR] = $0
    }

    END {
        for (i = NR; i >= 1; i--)
            printf "%s\n", frame_number[i]
    }
    '
}

# Blank all frames on current screen
# NOTE: stdout redirection of ratpoison -c/--command fselect
#+ to /dev/null to handle ratpoison printing the frame number
#+ that was selected
blank_frames() {
    curframebkp=$(rpc curframe)
    for a in $(frame_numbers); do
    rpc fselect $a > /dev/null
    rpc select -
    done
    rpc fselect $curframebkp > /dev/null
}

# Unblank all frames on current screen
# NOTE: stdout redirection of ratpoison -c/--command fselect
#+ to /dev/null to handle ratpoison printing the frame number
#+ that was selected
unblank_frames() {
    curframebkp=$(rpc curframe)
    for a in $(frame_numbers_reversed); do
    rpc fselect $a > /dev/null
    rpc other
    done
    rpc fselect $curframebkp > /dev/null
}
# -END-[ USEFUL FUNCTIONS ]----------------------------------------- #

# -START-[ POSITIONAL PARAMETERS ]---------------------------------- #
while getopts :bBhiI opt; do
    case $opt in
    (b)
        blank=yes
    ;;
    (B)
        unblank=yes
        ;;
    (h)
        help=yes
    ;;
    (i)
        initialize=yes
    ;;
    (I)
        uninitialize=yes
    ;;
    (?)
        usage=yes
    ;;
    esac
done
# -END-[ POSITIONAL PARAMETERS ]------------------------------------ #

# -START-[ BLANK FRAMES ]------------------------------------------- #
if [ "X$blank" = Xyes ]; then
    blank_frames
fi
# -END-[ BLANK FRAMES ]--------------------------------------------- #

# -START-[ UNBLANK FRAMES ]----------------------------------------- #
if [ "X$unblank" = Xyes ]; then
    unblank_frames
fi
# -END-[ UNBLANK FRAMES ]------------------------------------------- #

# -START-[ HELP ]--------------------------------------------------- #
if  [ "X$help" = Xyes ]; then
    echo "  -b blank all frames on current screen"
    echo "  -B unblank all frames on current screen"
    echo "  -h print this help"
    echo "  -i setup useful ratpoison aliases"
    echo "      (aliases are prepended with \"rpf-\")"
    echo "  -I remove useful ratpoison aliases"
fi
# -END-[ HELP ]----------------------------------------------------- #

# -START-[ INITIALIZE SCRIPT ]-------------------------------------- #
if [ "X$initialize" = Xyes ]; then
    rpc alias rpf-blank-all-frames exec sh $0 -b
    rpc alias rpf-unblank-all-frames exec sh $0 -B
fi
# -END-[ INITIALIZE SCRIPT ]---------------------------------------- #

# -START-[ UNINITIALIZE SCRIPT ]------------------------------------ #
if [ "X$uninitialize" = Xyes ]; then
    rpc unalias rpf-blank-all-frames
    rpc unalias rpf-unblank-all-frames
fi
# -END-[ UNINITIALIZE SCRIPT ]-------------------------------------- #

# -START-[ USAGE ]-------------------------------------------------- #
if [ "X$usage" = Xyes ]; then
    echo "usage: rpf.sh [-bBhiI]"
fi
# -END-[ USAGE ]---------------------------------------------------- #




