set tabstop=4
set shiftwidth=4

set verbose showmode
set showmatch
set matchtime=7
set ruler
set autoindent
set leftright
set nu
set errorbells
set noflash
set beautify
" Report even if 1 line was yanked
set report=1
" Print number of lines before opening a file
" set lines list
" Display lines unambiguously
set list

" Wrapping Solutions
" 1. Turn on Wrap
"    set wraplen=80
"    set wrapmargin=7
" 2. Sidescrolling
"    Do left-right scrolling
     set leftright
"    Left-right scroll
     set sidescroll=16


" Alternative word erase algorithm
"set altwerase
" Alternative erase algorithm
"set ttywerase
" In Ex display curr line automatically
set autoprint
" Remember c and g suffixed in s, &, ~ cmds
set edcompatible
" Backup
"set backup=~/.ovi/backup/


" EREs instead of BREs (use extended regular expressions)
"set extended
" Make REs case insensitive
set iclower
" Treat all characters as ordinary characters except ^ and $. You should escape them with /
"set nomagic
" Can change from ex to vi mode
set noopen
" Ex prompt
"set noprompt

" Read startup files in local dirs
set exrc
" Shell
set shell=/bin/bash
" Completion
set cedit=\	
set filec=\	
" Search
set searchincr
" Wrap searches around beginning/end of file
set wrapscan

" Tags
set tags=./tags,tags,.git/tags
"set taglength=0


" Set unprintable chars
"set noprint=""
" Chars always handle as printable chars
"set print=""

" Maps
map uwu : %!expand -t 4
"map v mv ;; Just some custom marks
"map V mV
map uf :.,.s/^/ 
map ul :.,.s/$/

