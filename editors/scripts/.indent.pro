// Indent config file. Place it in your $HOME path

// Use 4 spaces
-nut
-ts4
-i4
-il4
// Use 4 width tabs
//-ut -ts4 -in4 -iln4
// See man page: https://linux.die.net/man/1/indent
// Gnu defaults:
//    -nbad -bap -nbc -bbo -bl -bli2 -bls -ncdb -nce -cp1 -cs -di2
//    -ndj -nfc1 -nfca -hnl -i2 -ip5 -lp -pcs -nprs -psl -saf -sai
//    -saw -nsc -nsob
