#!/bin/sh
# Deletes all empty lines within a range given a file
# 	Usage: sh <script> <filename> <col1> <col2>
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"

exec awk -i inplace 'FNR>='"$2"' && FNR<='"$3"' && NF==0{next}1' "$MY_PATH/$1"
