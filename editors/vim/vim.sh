#!/bin/bash

# Set alacritty/urxvt bg vim color
declare -a vim_aux="#000000"
declare -a aux="#000000"
echo -ne "\033]11;$vim_aux\007"

# my editor
vim "$@"

# Set alacritty/urxvt bg color
echo -ne "\033]11;$aux\007"
