" !!! Necessary
" Execute: mkdir -p ~/.vim/swapfiles ~/.vim/backups ~/.vim/undodir
" To use registers *, + compile with +X11 option

"No Vi like compatibility
set nocompatible

" If not vim-tiny
if has("eval")
    let skip_defaults_vim = 1
endif 

" Source vim and nvi common .exrc
source $HOME/.cexrc

" Read startup files in local dirs
if getcwd() != $HOME
    set exrc
endif
" Shell
set shell=/bin/bash


" Indent
set tabstop=4
set shiftwidth=4
set expandtab
" Always add a newline add the end of a file
set fixeol


" Disable visual mode
nnoremap V <Nop>
nnoremap v <Nop>
nnoremap <C-v> <Nop>
set mouse-=a
" Disallow backspace over everything
set backspace=""
" Change buffers without saving them (prevents E37)
set hidden
" Remember buffers (use vim with no arguments)
set viminfo+=%


" Make some movement cmds dont respect cur column
set startofline
" Show command characters
set showcmd
" Show current mode and matches
set showmode showmatch
"10ths of a second vi pauses on matching characters
set matchtime=15
" Set end-of-line formats
set fileformats="unix,dos"
" Use a backup file while editing ()
set writebackup
" Fix error E10
set cpoptions-=C
" Vi like undo
set cpoptions+=u
" Undo levels
set undolevels=1000
" Mantain undo history between sessions
set undofile
set undodir=$HOME/.vim/undodir//
" No wrap
set nowrap

" Configure messages
"   - a: Some abbreviations
"   - tT: truncate some command line msgs
"   - I: Disable intro message
set shortmess=tTI
set verbose=0
set numberwidth=8
" Disable the bell completely
set belloff=all
" Show line/column numbers
set nu ruler
" Display signs in the number column (diff)
set signcolumn=number
set noflash
set beautify
" Redraw screen at least as pos
set lazyredraw
" Report even if 1 line was yanked
set report=7
" Display lines unambiguously ($ end chars)
set nolist

" Scrolling
" Left-right scroll
set sidescroll=16
" Number of lines scrolled
set scroll=15
" Keep 5 lines visible before/after cursor
set scrolloff=5

" Autoindent new lines
"set autoindent
"set smartindent copyindent
set nomodeline
set formatoptions+=r,o,j,/,c,q
" Emacs like indent (only when you press tab)
set noautoindent
" using tabs
" set cinkeys=0{,0},:,0#,!<Tab>,!^F
" using spaces
set cinkeys=0{,0},:,0#,!,!^F

" Backup
set nobackup
set backupdir=$HOME/.vim/backups/
" Prevent backups from overwriting each other. Like // in undodir
" since I'm using the 'backupext' variable to append the path.
" So the file '/home/docwhat/.vimrc' becomes '.vimrc%home%docwhat~'
au BufWritePre * let &backupext ='@'.substitute(substitute(substitute(expand('%:p:h'), '/', '%', 'g'), '\', '%', 'g'),  ':', '', 'g').'~'
set path=.
" Swap dir
set directory=$HOME/.vim/swapfiles//

" EREs instead of BREs (use extended regular expressions)
"   - use \v to make regex 'very magic' 
set magic
" Make REs case insensitive, except if expression contains upper case chars
set ignorecase smartcase
" Incremental search
set incsearch
" Dont start searching until <CR>
set nowrapscan
" Highlight searches
set nohlsearch
" Time out on mappings and keycodes
set timeout ttimeout
set timeoutlen=1200

" Spelling
set nospell
set spelllang=en_us

" Movement
"  - Change what vim considers a word (w, b, W, B, cmds)
set iskeyword+=224-235
" Display @@@ in the last line of a window
set display=lastline

" Text completion
" Display all matching files when tab complete
set wildmenu
" Use tab for completion
set wildchar=<Tab>
" Ignore files matching this patterns in cmd line completion
set wildignore+=.pyc,.swp
" Set completion opts
set complete+="kspell, k"
set completeopt+="menuone,noselect"

" Splits
set equalalways


" Tags

" Change leader key
let mapleader =" "
nnoremap <leader>i iHello World<esc>
" Status line
set laststatus=2

"" Abbreviations
ab #d #define
ab #i #include
" C like Comment
iab com /*<CR>/<Up>

" **** COMMANDS TO USE FRECUENTLY ****
" The arguments list
"   - :args to show curr arg list
"   - :[count]arge [names] to add names to arg list and edit it
"   - :[count]arga [name] to add name to arg list. Count => spec. pos.
"   - :[count]arga to add curr buffer
"   - :[range]argd[names] to delete names from arg list
"   - :[count]argu[count] to edit file count in the argument list
"   - :[count]n[ext]/[pN] to edit next/prev file
"   - :[count]wn[ext]/w[pN] to write curr file and edit next/prev
"   - :rew/la[st] to edit last file in arg list
"   - :argl[ocal] to make a local copy of arg list in the curr window
"   - :argl[ocal]! [names] to make a new local arg list
"   - :argg[lobal] to use the global arg list on the curr window
"   - :argg[lobal]! to create a new global arg list
" :e [filename] to open a buffer on curr window (E to split)
"       - :vs[plit] split vertically, sv[iew] for readonly
"               - C-w C-w to cycle splits, C-w = to equal sizes
"               - C-w [motion] to move btw splits
"       - :badd to open it on the bg
"       - :b[uffers]/:files/:ls to list buffers
"       - :b[uffer] [filename] o :buffer[N] to select one
"       - :bfirst / :blast
"       - :bg to bg a buffer, fg to switch to it
" /,? to search upwards/downwards
"       - n, N to show next/prev search result
"       - # to search for the word under cursor
"       - g# search top for the word under cursor
"       - *, g* serve the same purpose
" f, F to search for/backwards for a char. f[, f(
"       - ; to repeat this searches, , to reverse the order
"       - use t, T to stop one char before
"       - use / and ? to move around curr line
" Use ^, and $ to move to the start/end of a line
"       - 0 moves to first (blank) char
"       - use 2$ to move to the end of the second line
" Use 1G/1gg to move to the first line (also [N] G/gg)
"       - G/gg moves to the last line
" % Move between matchpairs, parenthesis, brackets, ...
"       - also works to search a matchpair forwards
"       - [N]% moves to a spec line (percentage)
" H to move the first line on cur screen
"       - M for middle and L for last
" C-u, C-d, C-f, C-b to scroll
" zz puts cursor line at the center, zt and zb for top, bottom
" Use Ctrl-G to see where you are (line, column, file ...)
" Use {, } to move between paragraphs
"       - [[ and ]] to move to the start/end of an outer block
"       - [{ moves to the start/end of the current block
"       - [m to find prev/next start of a method
"       - [] moves backward to the end of a function
"       - ]] moves fw to the start of the next function
" C-n in insert mode to autocomplete using headers,functions,vars...
" Use =[block] to indent a block(b), paragraph(p), %, i{, a}, ...
"       - Ex: =ab, =%, =i{, =ip
"       - [N]<, [N]>, <<, >> to shiftwidth lines
" deletions
"       - dw to delete word fw, diw to delete whole word
"       - db to delete word bw, dvb to delete also curr char
"       - v after {motion} force exclusivity
"       - V force command to work linewise
"       - dt[char] to delete till char bw, dT[char] fw
" m[a-z] to set a mark on the curr line. [A-Z} for global marks
"       - use `[a-zA-Z] to jump to a mark in a specific column
"       - use '[a-zA-Z] if that's not necessary
"       - :marks to list all marks
"       - can d'[a-z] and y'[a-z]
"       - m' => cur position before jumping (`` to use this mark)
"       - m" => when last editing a file
"       - m[, m] => start/end of the last change
" :reg[isters] to list registers, or :di[splay] r
"       - "xdd deletes curr line into buffer x. use with d, y, Y, p, put...
"       - use [a-z] to replace, [A-Z] to append to prev contents
"       - *, + register have clipboard contents (X11 primary, sec)
"       - use "0 to jump back from a scroll
" Macros => Use registers. Ex: delete 3dd using "add, and @c
" Finding identifiers
"       - [I, [i, ]I, ]i and the include option, allows finding global
"         identifiers
"       - [D, [d, ]D, ]d ant the define option, allows finding defined identifiers
" Tags
"       - 
" undo (:u) and redo (C-r).
"       - Use U to restore line to the state it was before the cursor was
"         placed on top of it
"       - :undolist to list undo states, :undo [number] to retriever
"       - :earlier [number][seconds(s), mins(m), days(d), hours(s)]
"       - :later [number][...]
"       - :echo undotree() to show it in a clear way
" q: => Command history (^R doenst work, use for mappings only)
"       - C-r in : command line 
" :%s/pattern/replacep/ to replace words (remove % for curr line)
" Use gU, gu[section] to make section upper/lowercase, g~ to swap
"       - repeat to match whole lines. gugu / gUgU
"       - u, U to match a char, ~ swap a char case
" Use ![motion][shell cmd] to use an external program to manipulate
" manipulate the selection
"       - !5Gsort<Enter> sorts the first 5 lines of the file
" Use [range]read ![shell cmd] to paste content (opposite: write)
"       - 0read !date -u  inserts the current time
" :[range]read/write [filename] to read/write text to a file
" Ctrl-L to redraw the screen
" :mksession [filename].vim to store curr session (mks! to save it back)
"       - :source [filename] to restore it, or vim -S [filename]
" :vim -r [filename | swapfile path] to recover from a crash


" **** MAPPING (should use leader key) ****
" Open cmd history for buffer macros => N
" Open man page under cursor => K

" Edit a file in a splitted window
cmap E :split
"Disable arrow keys
nnoremap <up> :echoerr "Umm, use k instead"<CR>
nnoremap <down> :echoerr "Umm, use j instead"<CR>
nnoremap <left> :echoerr "Umm, use h instead"<CR>
nnoremap <right> :echoerr "Umm, use l instead"<CR>

" Make <Esc> behave as expected (Just one <Esc> needed)
nnoremap <Esc> <Esc><Esc>

" Make j and k respect startofline
nnoremap <silent> j j0
nnoremap <silent> k k0

" Move a paragraph down/up
" nnoremap <leader>j }
" nnoremap <leader>k {
nnoremap <leader>d }
nnoremap <leader>u {

" Scroll horizontally
nnoremap <leader>h 20zh
nnoremap <leader>l 20zl

" Start inserting one line up or down
nnoremap <leader>j ji
nnoremap <leader>k ki

"Just insert one line after the curr one
" Add k at the end to move cursor
nnoremap <silent> <leader>O :<C-u>call append(line(".")-1, repeat([""], v:count1))<CR>
" Add j to move cursor
nnoremap <silent> <leader>o :<C-u>call append(line("."),   repeat([""], v:count1))<CR>


" Make Y consitent with D and C (yank til end)
map Y y$
" Deletions
"nnoremap <silent> dd :<C-U>exe "normal!". v:count1 . "ddj"<CR>

" Replace visual mode
nnoremap <leader>f :s/^\s\+/&
"map <leader>f :s/^/
nnoremap <leader>l :s/$/
nnoremap <leader>d :s/^\w\+\s\+//g<CR>
nnoremap <leader>b :s/\S\*$//g<CR>
nnoremap <leader>r :s/^\w\+/
nnoremap <leader>R :s/\S\*$/

" **** TOP COMMAND ****
" Repeat any command over a range (dw, diw, $diw) (accept ranges)
nnoremap <leader>. : norm

" Append text before and after lines
nnoremap <leader>i : s/^/
nnoremap <leader>a : s/$/

" Center 
nnoremap n nzz
nnoremap N Nzz
nnoremap * *zz
nnoremap # #zz
nnoremap g* g*zz
nnoremap g# g#zz

" Enclose a word in <arg>
"command -nargs=1 Enclose s/\(\w\+\)/\<args>\1\<args>/ | normal! \<CR>
"nnoremap <silent> <leader> en : Enclose 
function! Enclose(arg1, arg2)
    execute "normal gewi" . a:arg1
    execute "normal ea" . a:arg2
endfunction
nnoremap <silent> <leader>en" : call Enclose('"', '"')
nnoremap <silent> <leader>en' : call Enclose("'", "'")
nnoremap <silent> <leader>en( : call Enclose('(', ')')

""" Autocomment
"   - Write a string/char at the beggining of everyline
" (To Do) Join these two commands in one using a function
command -nargs=1 -range=1 Comment .,+<count>-1s/^/<args> 
"command -nargs=1 -range Comment <line1>,<line2>s/^/<args> 
function! CommentF(count, arg)
    execute a:count . "Comment " . a:arg
endfunction
nnoremap <silent> co" : <C-u>call CommentF(v:count1, '\"')<CR>
nnoremap <silent> co/ : <C-u>call CommentF(v:count1, '\/\/')<CR>
nnoremap <silent> co! : <C-u>call CommentF(v:count1, '!')<CR>
"nnoremap ) I# <Esc>j
"nnoremap ( k^2x
"   - Enclose a block as /* */ like comment
"       {, } => start/end of paragraph
"map cob dipi/*<Esc>gpi*/<CR><CR><Esc>
"nnoremap cob/ {I/*<Esc>}I*/<CR><Esc>
"nnoremap cob" {I"""<Esc>}I"""<CR><Esc>
"nnoremap cob' {I'''<Esc>}I'''<CR><Esc>

"   - Enclose functions
function! CommentFunction(arg1, arg2)
    execute "normal O" . a:arg1
    execute "normal j$f{"
    execute "normal ]}o" . a:arg2
    "execute "normal ][o" . a:arg2
endfunction
nnoremap cof/ : <C-u>call CommentFunction('/*', '*/')<CR>
"   - Enclose blocks
function CommentBlock(count, arg1, arg2)
    execute "normal O" . a:arg1
    execute "normal " . a:count . "j"
    " If line is empty find last non-empty line    
    if getline('.') =~ '^\s*$'
        execute "?^."
    endif
    execute "normal o" . a:arg2
endfunction
nnoremap cob" : <C-u>call CommentBlock(v:count1, '"""', '"""')<CR>
nnoremap cob' : <C-u>call CommentBlock(v:count1, "'''", "'''")<CR>


""" Manipulation of text
nnoremap <silent> mu : .m-2<CR>
nnoremap <silent> md : .m+1<CR>
nnoremap <silent> dx : s/^.//<CR>

""" Snippets
" inoremap { {<CR>}<Esc>O<TAB>
" inoremap ( ()<Esc>i
" inoremap [ []<Esc>i
" inoremap < <><Esc>i
" inoremap " ""<Esc>i
" inoremap ' ''<Esc>i

" Alternative mapping for auto closing
"imap "<tab> ""<Left>
"imap '<tab> ''<Left>
"imap (<tab> ()<Left>
"imap [<tab> []<Left>
"imap {<tab> {}<Left>
"imap {<CR> {<CR>}<ESC>O
"imap {;<CR> {<CR>};<ESC>O

" Completion => C-n
" ****************

"" Splits:
" Horizontal => :E [filename]
" Vertical => :hsplit [filename]
" Control-W => switch to next screen
" :bg => background curr screen
" :fg [name] => fg specified screen
" :di s => list bg screens

" Vimdiff (vim [-d, diff] file1 file2 / :diff f1 f2)
" Ivoke diff mode on curr file => :diffthis
" Diff between curr file and file1 => :diffs[plit] file1
" Apply a patchfile on curr file => :diffp[atch] patchfile
if &diff
    " Setup for vimdiff
    set diffopt+=vertical
    " Program to execute when entering diff mode
    " set diffexpr=
endif

" **** LANGUAGE SPECIFIC OPTIONS ****
"autocmd BufRead,BufNewFile *.py set noexpandtab


" **** STATUSLINE ****
" Show git branch name
function! GitGetCurrentBranch()
    let s:branch_name = system("git rev-parse --abbrev-ref HEAD")
    let s:notidx = match(s:branch_name, 'fatal: not a git repository')
    if s:notidx == -1
        let s:branch_name = strtrans(s:branch_name)
        let s:branch_name = s:branch_name[:-3]
        return '(' . s:branch_name . ') '
    endif
    return ''
endfunction

function! s:set_statusline_with_branch()
    let g:branch_name = GitGetCurrentBranch()
    if g:branch_name != ''
        setlocal statusline=%1*[%n]
        setlocal statusline+=\ %.100F\ %m%r%h%w
        setlocal statusline+=\ %{g:branch_name}
        setlocal statusline+=%=
        setlocal statusline+=%l,%v\ (%L)
    else
        setlocal statusline=%1*[%n]
        setlocal statusline+=\ %.100F\ %m%r%h%w
        setlocal statusline+=%=
        "setlocal statusline+=%l,%v\ (%p%%)
        setlocal statusline+=%l,%v\ (%L)
    endif
endfunction

autocmd BufEnter * silent! lcd %:p:h
autocmd BufEnter * call s:set_statusline_with_branch()

" **** THEMES ****
syntax on
"colorscheme lunaperche
" see xterm-true-color 
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" **** Colorscheme
set background=dark
highlight clear
syntax reset

" use terminal colors
"set notermguicolors
" 24-bit color
set termguicolors

" Nothing should be highlighted
for hl in getcompletion('', 'highlight')
    execute 'hi '.hl.' ctermfg=7 ctermbg=NONE guifg=White guibg=NONE'
endfor

" Make bg transparent
hi Normal guibg=NONE ctermbg=NONE
" Basic keywords are yellow (for, while, if).
hi Statement ctermfg=3 guifg=yellow
" Comments are green.
highlight Comment ctermfg=2 guifg=green
" Numbers are blue.
"hi Number ctermfg=4 guifg=blue
" Strings are magenta.
"hi String ctermfg=5 guifg=magenta
" Text color
"hi Normal ctermfg=7 guifg=white
" Statusline

hi clear StatusLine
hi clear StatusLineNC
hi User1 cterm=bold ctermfg=3 ctermbg=0 guifg=yellow guibg=#542C2F
hi StatusLineTerm ctermfg=0 ctermbg=7
hi StatusLine ctermbg=0 ctermfg=3 guibg=#542C2F guifg=yellow
hi StatusLineNC ctermbg=Grey ctermfg=0 gui=reverse guibg=#586e75 guifg=#000000
hi! link StatusLineTermNC StatusLineNC
" Show ^^^ in inactive windows
"hi! link StatusLine StatusLineNC

