#!/bin/bash

# Sed to get bg and fg colors along with the cursor
theme_file=`cat /home/daniel/.config/alacritty/themes/.selected_theme`
declare -a my_var="background: "
aux=`cat $theme_file | sed -n -e '/^ *'"$my_var"'/p' | awk -F "'" '{print $2}' | head -n 1`
echo $aux

# Set alacritty/urxvt bg vim color
declare -a vim_aux="#FFFFFF"
#declare -a aux="#FFFFFF"
echo -ne "\033]11;$vim_aux\007"

# my editor
vim "$@"

# Set alacritty/urxvt bg color
echo -ne "\033]11;$aux\007"
