#!/bin/bash

# Sed to get bg and fg colors along with the cursor
theme_file=`cat /home/daniel/.config/alacritty/themes/.selected_theme`
#cat $theme_file


declare -a my_var="background: "

aux=`cat $theme_file | sed -n -e '/^ *'"$my_var"'/p' | awk -F "'" '{print $2}' | head -n 1`
#echo $aux | xargs

echo -ne "\033]11;$aux\007"
