-- load standard vis module, providing parts of the Lua API
require('vis')


local plug = require('plugins/vis-plug')

-- configure plugins in an array of tables with git urls and options 
local plugins = {
	{ url = 'erf/vis-cursors' },
	{ url = 'fischerling/vis-spellcheck' },
	-- { url = 'erf/vis-highlight', alias = 'hi' },
	-- { url = 'samlwood/vis-gruvbox', theme = true },
}

-- access plugins via alias
-- plug.plugins.hi.patterns[' +\n'] = { style = 'back:#444444' }

-- require and optionally install plugins on init
plug.init(plugins, true)


vis.events.subscribe(vis.events.INIT, function()
	-- Your global configuration options
	
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	-- Your per window configuration options e.g.
	-- vis:command('set number')

        vis:command('set shell /bin/bash')
	vis:command('set tabwidth 4')
        vis:command('set autoindent on')
        vis:command('set number on')
        vis:command('set expandtab on')
--      vis:command('set layout v')
        vis:command('set numbers on')
end)
