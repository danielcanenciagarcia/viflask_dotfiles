-- symlink init.lua to ~/.config/init.lua
-- and colorschemes to ~/.config/colorschemes
-- MICRO_TRUECOLOR=1 micro ?


local micro = import("micro")
local config = import("micro/config")
local shell = import("micro/shell")
local buffer = import("micro/buffer")

--VERSION = "2.0.10"

function init()
    -- true means overwrite any existing binding to Ctrl-m
    -- this will modify the bindings.json file
    config.TryBindKey("Ctrl-m", "lua:initlua.makerun", true)
    config.TryBindKey("Alt-b", "lua:initlua.build", true)
    config.TryBindKey("Alt-s", "lua:initlua.vsplit_left", true)
    config.TryBindKey("Alt-S", "lua:initlua.new_view", true)
    config.TryBindKey("Alt-l", "lua:initlua.lint", true)
end

function makerun(bp)
    shell.RunInteractiveShell("make", true, false)
end

-- utils

function setContains(set, key)
    return set[key] ~= nil
end

-- below code adds new buffer with "hello content"
-- micro.CurPane():VSplitIndex(buffer.NewBuffer("hello", "filename"), true)

-- actions

function vsplit_left(bp)
    -- Open a new Vsplit (on the very left)
    -- false means right=false
    micro.CurPane():VSplitIndex(buffer.NewBuffer("", ""), false)
end

function new_view(bp)
    -- Open same file Vsplit (on the very right)
    -- true means right=true
    -- bp.Buf.Type.Readonly = true
    micro.CurPane():VSplitIndex(bp.Buf, true)
    micro.InfoBar():Message("New View same file")
end

function build(bp)
    micro.Log("init.lua build")
    bp:Save()
    local buf = bp.Buf

    _command = {}
    _command["go"] = "go run " .. buf.Path
    -- cargo install cargo-play
    _command["rust"] = "cargo play " .. buf.Path
    _command["python"] = "python3 " .. buf.Path

    -- the true means run in the foreground
    -- the false means send output to stdout (instead of returning it)
    shell.RunInteractiveShell(_command[buf:FileType()], true, false)

end

function sort_imports(bp)
    micro.Log("init.lua sort_imports")

    local buf = bp.Buf
    local filetype = buf:FileType()

    _command = {}
    _command["go"] = "goimports -w " .. buf.Path
    _command["python"] = "isort " .. buf.Path

    if not setContains(_command, filetype) then
        return
    end

    bp:Save()  -- TODO: is it saving twice
    run_action(bp.Buf, _command, "SortImports", false) -- false=no bottom panel
    buf:ReOpen()
end


function lint(bp)
    micro.Log("init.lua lint")

    bp:Save()
    _command = {}
    -- _command["go"] = "go ? " .. buf.Path
    _command["rust"] = "cargo-clippy "
    _command["python"] = "flake8 " .. bp.Buf.Path
    run_action(bp.Buf, _command, "Linter", true)
end

function onBufferOpen(buf)
	local filetype = buf:FileType()
    micro.Log("Filetype is:" .. filetype)
    _tabs = {}
    _tabs["makefile"] = true
    _tabs["go"] = true
    _tabs["snippets"] = true

    if not setContains(_tabs, filetype) then
        buf:SetOption("tabstospaces", "off")
    else
        buf:SetOption("tabstospaces", "on")
    end
end


function run_action(buf, commands, identifier, bottom_panel)

    micro.Log("init.lua run_action" .. identifier)

    local filetype = buf:FileType()

    if not setContains(commands, filetype) then
        return  -- if filetype does not support action just return
    end

    local output, err = shell.RunCommand(commands[filetype])

    local msg = output
    if err ~= nil then
        msg = msg .. tostring(err)
    end

    if msg ~= "" then
        if bottom_panel then
            local new_buffer = buffer.NewBuffer(msg, '')
            -- -- Looks like it is not a good idea to have it readonly
            -- new_buffer.Settings["filetype"] = filetype
            -- new_buffer.Settings["readonly"] = true
            -- new_buffer.Type.Readonly = true
            micro.CurPane():HSplitIndex(
                new_buffer,
                true -- means bottom split
            )
        else
            if err ~= nil then
                micro.InfoBar():Error(msg)
            else
                micro.InfoBar():Message(msg)
            end
            return
        end
    else
       micro.InfoBar():Message(identifier .. ": all good :)")
    end

end
