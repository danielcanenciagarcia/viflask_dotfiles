#!/bin/bash

# Sed to get bg and fg colors along with the cursor
theme_file=`cat /home/daniel/.config/alacritty/themes/.selected_theme`

declare -a my_var="background: "

aux=`cat $theme_file | sed -n -e '/^ *'"$my_var"'/p' | awk -F "'" '{print $2}' | head -n 1`
#echo $aux

# Set alacritty bg vim color (yang=FFFFF8, yin=00)
declare -a emacs_aux="#FFFFF8"
echo -ne "\033]11;$emacs_aux\007"

# my editor
emacs "$@"

# Set alacritty bg color
echo -ne "\033]11;$aux\007"
