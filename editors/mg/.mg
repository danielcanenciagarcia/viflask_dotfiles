# Backups
#make-backup-files 0
backup-to-home-directory 1
leave-tmpdir-backups 1

# Custom tab width
set-tab-width 4
# Insert spaces instead of tabs
no-tab-mode 1
# Automatic indent
set-default-mode indent

# Enable wrap mode
# set-default-mode fill
# wrap current line at 72 cols
set-fill-column 72
audible-bell 0
visible-bell 1
undo-enable 1
# undo everything
undo-boundary 1

# Global working dir
global-wd-mode 1

# Modeline
column-number-mode 1
line-number-mode 1

# Use meta key to insert extended ascii (8-bit) chars
#meta-key-mode 1
# for fill-paragraph (format)
sentence-end-double-space 1
#auto-execute *.c c-mode
#auto-execute *.json no-tab-mode
#auto-execute *.json set-tab-width 4

# Keys
#global-set-key "\^x\^f" find-file
#global-set-key "\e[Z" backward-char
global-set-key "c" overwrite-mode 
#global-unset-key

# Guide
# Repeat next command 4 times => C-u [command]
# Commands
#suspend, push-shell
