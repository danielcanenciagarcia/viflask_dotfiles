#!/bin/bash

# Selected theme
theme="my_mg_theme"


# Get current alacritty theme
fallback_theme=`cat /home/daniel/.config/alacritty/themes/.selected_theme | sed 's/^.*\//\//' | sed 's/^.//'`


# Foreground
#echo -ne "\033]10;#7b8c55\007"
# Background
#echo -ne "\033]11;#000000\007"
# Terminal cursor
#echo -ne "\033]12;#deb124\007"
# Selection fg
# Selection bg

# Change color 7 to #FFFFFF
#echo -ne "033]4;7;#9700ff\007"
# Change color 14 to #333333
#echo -ne "\033]4;14;#9700ff\007"

# Change colors using alacritty
ats -s $theme.yml >/dev/null 2>/dev/null

# my editor
mg "$@"

# Defaults settings
# Change the terminal foreground to #FFFFFF
#echo -ne "\033]10;#FFFFFF\007"
# Change the terminal background to #000000
#echo -ne "\033]11;#000000\007"

# If using alacritty-theme-switch select
# last used theme
ats -s $fallback_theme >/dev/null 2>/dev/null
# Fallback: ats -s Japanesque.yml

# Sed to get bg and fg colors along with the cursor
#theme_file=`cat /home/daniel/.config/alacritty/themes/.selected_theme`
#cat $theme_file


#declare -a var=("foreground: "
#                "background: "
#                "cursor: "
#                )
#declare -a code=("10"
#                 "11"
#                 "12"
#		)

## now loop through the above array
#for ((i=0;i<3;i++))
#for i in "${var[@]}"
#do
#   my_var=`echo "${var[$i]}"`
#   my_code=`echo "${code[$i]}"`


   #my_var+=4
   #echo $my_var
   #echo $my_code
   # We restore the default alacritty theme
   # colors changed before

#   if [ "$my_var" == "selec bg" ]
#   then
#     aux=`cat $theme_file | sed -n -e '/^ *background: /p' | awk -F "'" '{print $2}' | head -n 2`
#   else
#     aux=`cat $theme_file | sed -n -e '/^ *'"$my_var"'/p' | awk -F "'" '{print $2}' | head -n 1`
#   fi

#   echo -ne "\033]$my_code;$aux\007"

#done

# Cursor
#cursor=`cat $theme_file | sed -n -e '/^ *cursor: /p' | awk -F "'" '{print $2}'`
#echo -ne "\033]12;$cursor\007"
