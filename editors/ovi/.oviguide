###### General ######
# :w [name] => write buffer to <name> file or cur file
# :q => quit
# :q! => quit, discarting modifications
# ZZ => write the file and exit vi if there are no more files to edit (two cmds in a row ignore this)
# C-z => suspend cur editor session
# C-c => interrupt the current operation
# C-^ => switch to the most recently edited file
# [count] . => repeat last vi cmd (see undo)
# Q => exit visual mode and enter ex mode
#####################


###### Insert ######
# [count] a => append text one char after cur position (repeat inserted chars count-1 times)
# [count] A => append text at the end of the line (repeat inserted chars count-1 times)
# [count] i => insert text (repeat inserted chars count-1 times)
# [count] I => insert text at the start of the line (repeat inserted chars count-1 times)
# [count] o => append text in a new line below the cur one (repeat inserted chars count-1 times)
# [count] O => append text in a new line above the cur one (repeat inserted chars count-1 times)
####################

###### Change ######
# [count] r <char> => replace count chars by char
# [count] R => enter input mode replacing chars in cur line (if count chars are repeated count-1 times)
# [buffer] [count] c motion => change region of text describe by count and motion. cw, cW, c0, c$. If buffer, yank the deleted text into buffer.
# [buffer] C => change text from cur position to eol. If buffer, yank the deleted text into buffer
# [buffer] [count] s => substitute count chars in cur line .If buffer, yank the deleted text into buffer
# [buffer] [count] S => substitute count lines. If buffer, yank the deleted text into buffer
####################

###### Delete ######
# [buffer] [count] x/X => delete count chars fw/bw. If buffer, yank the deleted text into buffer
# [buffer] [count] d motion => dw, dW, d0, d$, d^, etc. If buffer, yank the deleted text into buffer.
# dd => delete cur line
# [buffer] D => delete text from cur position to eol. If buffer yank deleted text into buffer
# <count> J => join count lines with the cur line 
####################

###### Copy/Yank/Paste ######
# [buffer] [count] y motion => yank region specified by count and motion
# [buffer] [count] Y => Copy, yank count lines into buffer
# yy => yank cur line
# [buffer] p => append text from buffer after the cur column if buffer is character-oriented or before if line-oriented
# [buffer] P => append text from buffer before the cur column if buffer is character-oriented or after if line-oriented
#############################

###### Search ######
# C-a => search fw for word at point
# [count] t / T <char> => search fw/bw N times through the cur line for char before/after the specified char
# [count] f / F <char> => search N times fw/bw through the cur line for char 
# [count] , => reverse find char (the last f,F,t,T cmd) N times
# /re => search fw reg-exp
# /re/ [offset] [z] => offset means place cursor before/after search. z means reposition cur line after search
# ?re => search bw reg-exp
# ?re? [offset] [z] => idem
# n / N => repeat the last search in the last / opposite dir
####################


###### Display / Scroll ######
# [count] C-f / C-b => page fw/bw
# [count] C-d / C-u => scroll fw/bw half screen
# [count] C-e / C-y => scroll fw/bw leaving cur line as it is
# [count] H => move to the screen line count-1 lines below the top of the screen
# [count] L => move to the screen line count-1 lines above the bottom of the screen
# M => move to the screen line in the middle of the screen
# C-g => display cur file info
# C-l / C-r => repaint the screen
# [count1] z [count2] type => redraw repositioning and resizing screen. Limit screen size by count2 lines.
#          type chars => + if count1 given place the line count1 at the top of the screen, otherwise display screen after current screen
#                        <cr> place line count1 at the top of the screen
#                        . place line count1 at the center of the screen
#                        - place line count1 at the bottom of the screen
#                        ^ if count1 is given, display the screen before the screen before count1, otherwise display screen before current screen
##############################


###### Movement ######
# <count> g => 
# <count> G => move to line <count> or last line
# <count> | => move to a specific column position in cur line
# <count> h, C-h => move cursor bw N chars without changing cur line
# <count> j, C-j, C-n => move cursor down N lines (see k)
# <count> l, space => move cursor fw N chars without changing cur line
# <count> +, C-m => move the cursor down N lines to first non blank char
# <count> - => move the cursor up N lines to first non blank char
# <count> _ => move down count lines to the first non blank char
# 0 / $ => move the cursor to the start/end of cur line
# ^ => move the cursor to the first non blank char of cur line
# % => move to the parenthesis,bracket,... matching the one found near the cursor
# <count> w / b => move fw/bw N words
# <count> W / B => move fw/bw N bigwords
# <count> e => move fw N end-of-words
# <count> E => move fw N end-of-bigwords
# <count> (, ) => move N sentences bw/fw
# <count> {, } => move N paragraphs bw/fw
# <count> [[ / ]] => move bw/fw count section boundaries
# 
# '' => return to the position of the cursor (first non blank char) before one of the fw cmds: C-a⟩, C-t⟩, C-], %, ', `, (, ), /, ?, G, H, L, [[, ]], {, }
# `` => idem (line and column marked)
#####################


###### Undo #######
# u => undo / redo
# <count> . => re-undo
# U => undo the the state the line was before cursor was moved to it
###################


###### Marks #######
# m <char> => save cur context (line & column) as <char>. ma, mb , ...
# '<char> => return to the position marked by the charecter <char> (fisrt non blank char)
# `<char> => return to the position marked by the charecter <char> (line and column marked)
####################

###### Buffer Macros ######
# @ buffer => execute a named buffer as vi commands. (expressed as :<ex cmd>)
# @@, @* => execute the last buffer macro
# 
###########################

###### Tags #####
# C-t => return to the most recent tag context
# C-] => push a tag reference onto the tag stack
# 
#################


###### Windows ######
# C-w => Switch to the next lower screen in the window, or next screen
# 
#####################

###### Cmd line related ######
# : => execute an ex cmd
# escape => execute ex command or cancel if partial
# [count] ! motion shell args => replaces lines scanned by output and motion with shell's cmd output
#        % means current filename, # means previous filename, ! means cmd text of prev :! or ! cmds
# [count] & => repeat last substitution cmd
##############################

###### Ex Commands / Ex mode ######
# :[range] ! args => execute/filter a shell cmd
# " => a comment
# :[range] nu[mber] [count] [flags] => display the selected lines, each preceded with its line number
# :[range] # [count] [flags] => idem
# :@ buffer, :* buffer => execute a buffer
# :[range] < [<...] [count] [flags] => shift lines left. idem with > (shift right)
# :[lines] = [flags] => 
# :ab[breviate] 1 2 => add abbreviation (vi mode only)
# :[line] a[ppend][!] => the input is appended after the expecified line
# :ar[gs] => display the arguments list
# :bg => background the current screen. vi mode only
# :fg => foreground the specified screen. vi mode only
# :[range] c[hange][!] [count] => the input text replaces the specified range
# :chd[ir][!] [directory] => change the cur dir
# :cd[!] [directory] => idem
# :[range] co[py] line [flags] => 
# :[range] t line [flags] => copy specified lines after the destination line
# :[range] d[elete] [buffer] [count] [flags] => delete the lines from the file
# :di[splay] b[uffers] | s[creens] | t[ags] => display buffers, screens, tags
# :e[dit][x][!] [+cmd] [file] => edit a different file
# :exu[sage] [command] => display usage for an ex cmd
# :f[ile] [file] => display and optionally change the filename
# :[range] g[lobal] /pattern/ [commands] => apply commands to lines matching a pattern
# :[range] v /pattern/ [commands] => apply commands to lines not matching a pattern
#                      commands => g for global, c for confirm
# :he[lp] => display a help name
# :[line] i[nsert][!] => 
# :[range] j[oin][!] [count] [flags] => join lines together
# :[range] l[ist] [count] [flags] => display the lines unambiguously
# :map[!] => define or display maps. vi only
# :[line] ma[rk] <char> => mark the line with the mark <char>
# :[line] k <char> => idem
# :[range] m[ove] line => move the specified lines after the target line
# :mk[exrc][!] file => write the abbreviations, editor options and maps to the specified file
# :n[ext][!] [file ...] => edit the next file from the argument list
# :prev[ious][!] => edit the previous file from the argument list
# :pre[serve] => save the file in a form that can be recover later using the ex -r option
# :[range] p[rint] [count] [flags] => display the specified lines
# :[line] pu[t] [buffer] => append buffer contents to the current line
# :q[uit][!] => end the editing session. In split-mode only close the cur screen
# :[line] r[ead][!] [file] => read a file
# :rec[over] file => recover file if it was previously save
# :res[ize] [+|-]lines => grow or shrink cur screen. vi mode only
# :rew[ind][!] => rewind the argument list
# :[range] s[/pattern/replace/[options][count][flags]] => substitute the regular exp pattern with replace. If /pattern/replace/ is omitted like &
# :[range] &[options][count][flags] => /pattern/replace/ for the last s command is used
# :[range] ~[options][count][flags] => like & but the pattern is the last used by any command
#   The replace field may contain any of the following sequences:
#     ‘&’ the text matched by pattern.
#     ‘~’ the replacement part of the previous s command.
#     ‘%’ if this is the entire replace pattern, the replacement part of the previous s command.
#     ‘\#’ where ‘#’ is an integer from 1 to 9, the text matched by the #'th subexpression in pattern.
#     ‘\L’ causes the characters up to the end of the line of the next occurrence of ‘\E’ or ‘\e’ to be converted to lowercase.
#     ‘\l’ causes the next character to be converted to lowercase.
#     ‘\U’ causes the characters up to the end of the line of the next occurrence of ‘\E’ or ‘\e’ to be converted to uppercase.
#     ‘\u’ causes the next character to be converted to uppercase.
#   The options field may contain any of the following characters:
#     ‘c’ prompt for confirmation before each replacement is done.
#     ‘g’ replace all instances of pattern in a line, not just the first. 
# :sh[ell] => run a shell program
# :so[urce] file => read and execute commands from a file
# :su[spend][!], :st[op][!], C-z => suspend the editing session
# :ta[g][!] tagstring => edit the file containing the specified tag
# :tagn[ext][!] => edit the file containing the next context for the current tag
# :tagpr[ev][!] => edit the file containing the previous context for the cur tag
# :tagp[op][!] [file | number] => pop to the specified tag in the tags stack
# :tagt[op][!] => pop to the least recent tag on the tags stack, clearing the stack
# :unab[breviate] => delete an abbreviation. vi mode only. 
# :u[ndo] => undo the last change made to the file
# :unm[ap][!] => unmap a specific string
# :ve[rsion] => display the version of the ex/vi editor
# :[line] vi[sual] [type] [count] [flags] => enter vi. ex mode only
# :vi[sual][!] [+cmd] [file] => edit a new file. vi mode only
# :viu[sage] [command] => display usage for vi command
# :[range] w[rite][!] [>>] [file] => write the file
# :[range] wn[!] [>>] [file] => write the file and edit the next line from the argument list
# :[range] wq[!] [>>] [file] => write the file and exits the editor. if split-mode close cur screen
# :[range] x[it][!] [file] => exit the editor, write the file if it was modified. if split-mode ...
# :[range] ya[nk] [buffer] [count] => copy the specified lines to a buffer
# :[line] z [type] [count] [flags] => adjust the window
# Vi mode only => 
#   - For e, fg, n, prev, ta, and vi, if the first letter of the command is capitalized,
#     the current screen is split
#########################


###### Indentation ######
# [count] > / < => shift N lines by the amount of shiftwidth
# 
#########################

###### Registers ######
# [count] # #/+ => increment number under the cursor by N
# [count] #- => decrement number under the cursor by N

#######################

###### Case ######
# 
# [count] ~ motion => if tildeop option is not set, reverse the case of the next N chars and no motion can be
#                     speficied. Otherwise, ~ reverse the case of the next chars especified by count and motion
##################

###### Vi text input ######
# C-d => erase the prev shiftwidth column boundary
# ^C-d => erase all the autodindent chars and reset the autoindent level
# 0C-d => erase all the autoindent chars
# C-t => insert tabs and spaces to match prev line indentation
# C-h, erase => erase the last char
# C-v (literal next) => escape next char from any especial meaning
# escape => resolve all text input into the file and return to cmd mode
# line erase => erase the current line
# C-w (word erase) => erase the last word
# C-x [0-9A-Fa-f]+ => insert char hexadecimal value
###########################

