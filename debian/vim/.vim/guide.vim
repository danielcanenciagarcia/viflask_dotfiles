"" Vi Improved Guide

""" Most frequent keys
" 	- To start: eE, fF, ^, J, /?, {}, S, s, r
"		    C-^, :n[N] and :prev[Prev] (files as arguments)
"	- yt<space> to yank a whole word
"	- m or m[ove] command
"	- C-T to shift right as much as needed
"	- C-D to erase a tab (and spaces), 0C-D and ^C-D
"	- C-H/del to erase a char, C-W to erase last word
"	- di b[uffers] and di s[creens]
"	- '' to move before and after a movement occurs
"	- '-' to move to the first non-blank char of prev line

" tT and fF and ; and comma, wW, bB and eE, <space>, 0, ^, $
" - (move first char of the prev line)
" (,) and {,} and [,], %
" G, / and ?, d and dd and D, J
" c and C, r and R, s and S, ~ (reverse case)
" mX and gX and 'X and `X
" u and U (or u and . and C-r), repeat .
" C-f and C-b, C-d and C-u, H L and M
" > and <, (.,.) > count
" C-^ (edit alternate file)
" See: https://ex-vi.sourceforge.net/ex.html

""" List of Text-Object Selection Commands """
"
""""""""""""""""""""""""""""""""""""""""""""""

""" Usefull commands
" :ab[breviate]		  >> List all abbreviations
" :r !cat                 >> Paste text without autoindenting it
" Alt-[N]		  >> Prepend .,+N range to next ex command
" dvb / dve		  >> V Toggles exclusiveness of extra space (dvb works like 'dw backwards')

" INSERTING:
" gI			  >> Like I, no matter what the first chars are (I first non-blank)
" gi			  >> Move to the last insertion position and switch to insert mode
" "0p			  >> Paste the last yank. Ignore deletions (!! USEFUL !!)
" UNDOES:
" uu			  >> Undo last undo
" C-r			  >> Change undo direction (start redoing or undoing again)
" :changes		  >> Show a list of changes (> indicates the current position)
" g;			  >> Jump to last edit
" g,			  >> Jump forward in last edit
" "1pu.			  >> Paste text from register "1 (u. puts register "2 and so on)
"			     (this special registers "1-"9, store the last deletions)
" OPENING FILES:
" gx			  >> Opent the file located at the filepath under the cursor
" gf			  >> Edit the file located at the filepath under the cursor
"			     (Can open an URL if netrw is enabled)
" REPEATING:
" .			  >> Repeat the last change
" @:, :*	          >> Repeat the last command executed (the : register stores it)
" &			  >> Repeat the last substitution :s/ command
" g&			  >> Repeat the last substitution on every line
" COPYING:
" C-r C-w		  >> Copy the word under the cursor
" C-r C-a		  >> Copy the WORD under the cursor
" C-r C-l		  >> Copy the line under the cursor
" C-r C-f		  >> Copy the filename under the cursor
" :[range] co [line]	  >> Copy text in a given range to a target line
" :[range] mv [line]	  >> Move text in a given range to a target line
" :y [register]		  >> Yank text to a register (or append it capital letter specified)
" 			     (Ex: y a, y A)

" DIFFS:
" :DiffGitCached	  >> Show a diff of the current commit (ft-gitcommit-plugin)
" :vert diffsplit [file]  >> Execute vimdiff
" SEARCHES:
" * / C-a		  >> Search for the word under cursor
" g*			  >> Search fot partial word under cursor (repeat with n)
" C-o / C-i		  >> Go through jump locations (!! USEFUL !!)
" `` or ''		  >> Go back to the original position before starting a search
" gn			  >> Select the last searched match (can use n,N or gn after)
" // , ??		  >> Search for the last search
" G?foo?		  >> Go to the last matching pattern (instead of the first one)
" :g// 			  >> List all lines matching the last search
" ?carrot?+3,/pepper/-2s/ >> Substitute from the last carrot occurrence to the next pepper
" occurrence
" GLOBAL:
" g/carrot/'{+,'}d	  >> Delete every paragraph that contains the word carrot
" g/carrot/+3;/pepper/- > >> Indent 3 lines after carrot to one before the following pepper
"
" INCLUDE SEARCH COMMANDS (i search for text and d for text plus an argument):
"   - i jumps to maches, and d to classes or whatever is defined by the define option
" [N] [i		  >> Display the first line that contains the keyword under cursor
" ]i			  >> Sames as [i, but starts at the cur cursor position
" [I, ]I		  >> Like [i and ]i, but display all lines
" [N] [ C-I and ] C-I	  >> Like [i and ]i, but jumps to matches
" :[range]is[earch] /pat/ >> Like [i and ]i, but search in ranges lines and uses a pattern
" :[range]ij[ump] [count] >> Like [ C-I and ] C-I with ranges and pattern based
" C-w i			  >> Open a new window starting a search for the word under the cursor
" :[range]isp[lit] /pat/  >> Like C-w i, with a range and a pattern
" [d, ]d, :dsearch, [D, ]D, >> Like the above, but works with macros instead of keywords
" :dlist, [ C-d, ] C-d, :djump, C-w d, :dsplit
" :che[ckpath] [!]	  >> List all the included files ([!] that couldnt be found)
" GREP:
" :grep			  >> Use grep from within vim
" INDENTING:
" =			  >> Reindent lines based on motion
" ==			  >> Reindent curr line
" >% / <%		  >> Increase indent of bracketed block (place cursor on brace first)
" =%			  >> Reindent a brackedted block (cursor on brace)
" [N]=i{ /[N]=a{	  >> Reindent N blocks 'inner / a block'
" >i{ / <a{		  >> Increase / Decrease inner block indent
" gg=G			  >> Indents whole buffer
" BUFFERS:
" C-^			  >> Switch to alternate file
" :b[uffer]N / :e #N   	  >> Switch to buffer N
" :bd[elete]N		  >> Remove buffer N from the buffer list
" :bw[ipeout]N		  >> Close buffer N
" :bn[ext] / :bp[revious] >> Next and previous buffer
" :bN[ext]		  >> Same as bp[revious]
" :bf[irst] / :bl[ast]    >> First and last buffer
" :br[ewind]N		  >> Same as first buffer
" :bm[od]N		  >> Switch to the Nth modified buffer
" !!! Note: This commands can take [count], thus jumpting to the Nth buffer
" :sb[n|p|f|l]N		  >> open buffer in a split window
" :ls / buffers		  >> list all buffers
" !!! Note: The above command work for file args too, removing the letter b
" :args			  >> Show argument list
" :arga[dd] [fnames..]	  >> Add a file to the argument list
" :argd[elete] [fnames..] >> Remove a file from the arg list
" :argde[dupe]		  >> Remove duplicates from the arg list
" SPLITTING:
" C-w s	(:sp[lit])	  >> Split the current window hor (and edit a file)
" C-w v (:vs[plit])	  >> Split the current window ver (and edit a file)
" :vs|view [file]	  >> View a file in an splited window (vs or s)
" C-w n	(:new)		  >> Split the current window hor and edit an empty file
" :vnew			  >> Split the current window ver and edit an empty file
" C-w ^ / C-6		  >> Split the current window with the alternate file
" <BID> C-w ^		  >> Split the current window with the buffer id
" [N] C-w w		  >> Go to the 'next' window, below/right of the cur window
" C-w p			  >> Go to prev window
" C-w W			  >> Go to the window above/left of the cur window
" C-w C-w		  >> Move cursor to another window
" C-w {h,j,k,l}		  >> Cycle between windows
" C-w r	/ C-w C-r	  >> Rotate windows downwards/rightwards
" C-w R			  >> Rotate windows upwards/leftwards
" C-w x			  >> Exchange current window with the next one
" C-w {H,J,K,L}		  >> Move the current window
" C-w =			  >> Make all windows equally high and wide
" C-w _			  >> Increase a window to its maximum height
" C-w |			  >> Increase a window to its maximum width
" C-w - / +		  >> Decrease window height
" :res[ize] {-,+}[N]	  >> Resize window
" C-w < / >		  >> Decrease window width
" C-w c			  >> Close a window
" C-w q			  >> Quit current window (cant be viewed using :ls)
" C-w o			  >> Close all other windows
" QUICKFIX LIST, LOCATION LIST
" :lgrep [text] [files]	  >> Execute grep over a list of files and populate the location list
"			     (Use cgrep to use the quickfix list, for all windows)
" :lcl[ose]		  >> Close the location list window
" :lopen		  >> Opent the location list window
" :lnext / :lprev	  >> Go to the next / prev item on the list
" :lfirst / :llast	  >> Go to the last item on the list
" :ll [N]		  >> Go to the Nth item
" :windo [cmd]		  >> Execute command an every window (:window $)

" TAGS:
" :tags			  >> Show the content of the tag stack
" :[N]ta[g] {ident}	  >> Jump to the definition of {ident} (can take reg expr)
" :[N]ta[g] /^get	  >> Jump to the first tag that starts with get
" C-] / g] (splits)	  >> Jump to the definition of the keyword under the cursor
" :[N]ta[g]		  >> Jump to the Nth newer entry in the tag stack
" C-t			  >> Go to previous spot where you called tag
" :[N]po[p]		  >> Jump to the Nth older entry in the tag stack
" :pta[g] [tagname]	  >> Show current tag in a preview window
" :pclose		  >> Close preview window
" :ts[elect] {ident}	  >> List the tags that match {ident} or last tag
" :sts[elect] {ident}	  >> Same as tselect, but splits the window
" :{s}tj[ump] {ident}	  >> Same as tselect. Jumps directly when only one match
" :[N]tn[ext]	  	  >> Jump to the next matching tag
" :[N]tp / tN		  >> Jump to the previous matching tag
" :[N]tr[ewind] / tf[irst]>> Jump to the first matching tag
" :tl[ast]		  >> Jump to the last matching tag
" :lt[ag] {ident}	  >> Jump to the tag {ident} and add matching tags to a new location list
" PREVIEW WINDOW:
" C-w z / C-w C-z	  >> Close any open preview window
" C-w P			  >> Go to the preview window
" pts[elect] {ident}	  >> :tselect on a preview window
" 	- Works for ptj, ptn, ptN, ptp, ptr, ptf, ptl
" [N]pp[op]		  >> Does ppop     on a preview window
" C-w }			  >> Same as C-w ] on a preview window
" C-w g }		  >> Same as g]    on a preview window
" [range]ps[earch] /pat/  >> Like :ijump, but shows found match in the prev window
" :ped[it]		  >> Edit a file in a preview window
" COMMAND LINE WINDOW:
" q:			  >> Open command line history
" :history [:]		  >> Open command line history
" q/ and q?		  >> Open search history
" :history / or ?	  >> Open search history
" C-f (depends on cedit)  >> Open command line history while in command line mode
" SCROLLING TEXT:
" zt / zb		  >> Place current line at the top / bottom of the screen
" z.			  >> Place current line at the middle of the screen
" zs(ZL) / ze(ZJ)	  >> Scroll text horizontally or vertically (!! USEFUL !!) (nowrap)
" [N] zl / zh		  >> Move the view N char to the left / or right (nowrap)
" MOVING AROUND:
" 1G / GG		  >> Jump to first and last line
" [N]G			  >> Jump to line N
" 0 and $		  >> Move to the start and end of a line
" g_ / ^		  >> Move to the first non-whitespace char
" :ju[mps]		  >> Show the jump stack
" C-o			  >> Go to the previous older cursor position
" C-i			  >> Go to the next cursor position
" `. / '.		  >> Jump to last insertition position (last change in curr buffer)
" :gi / `"		  >> Jump to last insertition position and enter insert mode
" `0			  >> Jump to position in last edited file
" `1			  >> Jump to position in last,last edited file (same with `2, `3...)
" '' / ``		  >> Jump back to line where we jumped from (in curr buffer)
" '[ / `[		  >> To the first char of the previously changed or yanked text
" '] / `]		  >> To the last char of the previously changes or yanked text
" :g; / g,		  >> Jump to the next / prev change
" [m / ]m		  >> Jump to the start / end of a method
" MARKS:
" :k[M] / :ma[rk] / m[M]  >> Set mark M. Lowercase marks are local, and Uppercase global
" :marks [list of marks]  >> List all current marks or selected marks
" d,c,y 'a		  >> Delete, Change or yank text from cursor to mark a)
" 'a,'b[operation]	  >> Execute a command in the ranged given by marks a and b
" [N] ]' / ['		  >> Jump to the next / prev line with a lowecase mark
" [N] ]` / [`		  >> Jump to the next / prev lowercase mark (exact pos)
" '[, ']		  >> Start/end of last change or yank
" '.			  >> position where last change was made
" '^			  >> Position of cursor where vim left insert mode
" ''			  >> Position of cursor before last jump
" CASE:
" ~			  >> Change case of cur line
" gu			  >> Lowercase using a motion cmd (guiw / gu2w / gu$)
" gU			  >> Uppercase using a motion cmd (gUiw)
" MACROS:
" "a[cmd]		  >> Store yanked or deleted text into register a (or append when A)
" q<lowercase_letter>	  >> Begin recording keystrokes in a register
" q			  >> Stop the macro recording
" @<lowercase_letter>	  >> Execute a recorded macro
" @@			  >> Repeat recorded macro
" COMPLETING:
" C-n / C-p		  >> Next/previous word completion
" C-x C-l		  >> Line completion (!!! SO USEFUL !!!)
" C-x C-k		  >> Dictionary completion (!!! SO USEFUL !!!) (set dictonary)
" C-e			  >> Close popup window and put back original text (!! USEFUL !!)
" C-x s			  >> Complete with spelling suggestions (Insert mode)
" C-x C-v		  >> Complete with command line history
" C-x C-f		  >> Complete the file path under the cursor
" C-x C-i		  >> Complete with the keywords in the current and files
"			     included  in the path variable (set path)
" z=			  >> Change current word from a menu filled with suggestions
" INSERT MODE AND COMMANDLINE MODE:
" C-t C-d		  >> Indent cur line forward or backwards (insert mode)
" 0C-d			  >> Remove all indentation from line (insert mode)
" C-w			  >> Erase a word
" C-u			  >> Erase a line
" C-x { C-y / C-e }	  >> Scroll up / down in insert mode
" REGISTERS:
" "kyw			  >> Yank a word into register k
" "kp			  >> Put contents of register k
" "kdw			  >> Replace register k with what the cursor is on

" EXECUTING VIM:
" vim +N {file}		   >> Edit a file and place the cursor in the line N
" vim {file1} {file2} ...  >> Edit more than one file and add them to the arg list
" C-o C-o		   >> Open last edited file (When using $vim without args)
" g`"			   >> Restore last cursor position after opening a file
" :cq			   >> Exit vim with a non zero status (useful for git commit messages)
" vim `grep -l {word} *.c` >> Edit every C files that contain the word {word}. This can be a regular expression
" vim -t '/sha1$ {file}	   >> Edit a file and search for tags ending in sha1
""""""""""""""""""""""""""""""""""
