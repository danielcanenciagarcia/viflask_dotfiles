"" Vi Improved Colors

""" Syntax highlighting
syntax on

""" Themes
if !has('gui_running') && &term =~ '^\%(xterm-color\|xterm-256color\)'
	" Enable true colors
	if exists('+termguicolors')
		let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
		let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
		set termguicolors
	endif
elseif !has('gui_running') && &term =~ '^\%(xterm\)'
	if exists('+termguicolors')
		set notermguicolors
	endif
endif

" Tmux fixes
if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
endif

" Colorscheme
" Set number of color to be used by the terminal
set notermguicolors
set t_Co=256
"set t_Co=16
set background=dark
highlight clear

" Set interface colors
"color delek
" Make bg transparent
hi Normal       cterm=NONE ctermfg=NONE ctermbg=NONE guifg=White guibg=Black
" Link every other group to normal
for hl in getcompletion('', 'highlight')
	execute "hi! link " . hl . " Normal"
endfor

" Cursor color
" ICursor and Cursor do not work in terminals
" Cursor identifiers
hi CursorColumn	cterm=bold ctermbg=Black ctermfg=78
"hi CursorColumn  cterm=bold ctermfg=Green ctermbg=Black
"hi CursorColumn  cterm=bold ctermbg=Black ctermfg=71
"hi CursorColumn cterm=bold ctermbg=235 ctermfg=NONE
"hi CursorColumn  cterm=bold ctermbg=Black ctermfg=NONE
hi CursorLine 	cterm=underline ctermfg=White ctermbg=NONE
" Change Color when entering Insert Mode
autocmd InsertEnter * highlight CursorColumn cterm=NONE ctermfg=NONE ctermbg=NONE
" Revert Color to default when leaving Insert Mode
autocmd InsertLeave * highlight CursorColumn ctermbg=Black ctermfg=Green

" Some modifications
hi Comment	cterm=bold ctermbg=NONE ctermfg=Blue
"hi Comment	cterm=NONE ctermbg=NONE
hi LineNr 	ctermfg=Brown guifg=LightYellow
hi Error	cterm=bold ctermbg=Red ctermfg=White gui=bold guibg=Red guifg=White
hi MoreMsg	cterm=bold ctermfg=DarkGreen gui=bold guifg=SeaGreen
hi Question	cterm=bold ctermfg=DarkGreen gui=bold guifg=SeaGreen
hi Search	cterm=reverse ctermbg=Yellow ctermfg=NONE guibg=Yellow guifg=NONE
hi DiffText	cterm=reverse cterm=bold ctermfg=Black ctermbg=Gray gui=bold guifg=Black guibg=Gray
hi DiffAdd	cterm=bold ctermfg=Black ctermbg=LightGray guibg=LightGray guifg=Black
hi DiffChange	cterm=bold ctermfg=Black ctermbg=LightGray guibg=LightGray guifg=Black
hi DiffDelete	cterm=bold ctermfg=Black ctermbg=DarkGray gui=bold guifg=DarkGray guibg=Black
hi SignColumn 	cterm=bold ctermfg=NONE ctermbg=NONE
hi Search	guifg=Black gui=NONE ctermfg=Black cterm=NONE
hi IncSearch 	gui=reverse cterm=reverse
hi! link	CurSearch IncSearch
hi MatchParen	guifg=Gray guibg=Yellow ctermbg=Yellow ctermfg=DarkGray
hi! link 	VertSplit IncSearch
hi TabLine	gui=bold guibg=NONE guifg=White cterm=bold ctermbg=NONE ctermfg=White
" Popup menu (completion)
"hi Pmenu	cterm=bold ctermbg=LightGray ctermfg=Black guibg=LightGray guifg=Black
hi Pmenu        cterm=standout
"hi PmenuSel	cterm=bold ctermbg=Black ctermfg=White
hi PmenuSel	cterm=standout ctermfg=NONE
"hi PmenuSel	cterm=reverse ctermbg=None	ctermfg=None
hi PmenuSbar	gui=NONE cterm=NONE
hi! link	PmenuSbar PmenuThumb
" Folding
hi Folded	cterm=bold ctermbg=DarkGray ctermfg=Black
hi FoldColumn	cterm=bold ctermbg=DarkGray ctermfg=Black
hi! link	FoldColumn SignColumn
" Statusline colors
hi StatusLine	cterm=bold ctermbg=LightGray ctermfg=Black guibg=LightGray guifg=Black
"hi StatusLine   cterm=bold ctermbg=NONE ctermfg=Gray guibg=White
hi StatusLineNC cterm=bold ctermbg=DarkGray ctermfg=Black guibg=DarkGray guifg=Black
hi! link	StatusLineTerm StatusLine
hi! link	StatusLineTermNC StatusLineNC

"""""" Theme modification """"""
" Clear colors
"highlight clear
"if exists("syntax_on")
"	autocmd VimEnter * syntax clear
"endif


"color delek
" Text is yellow
"hi Normal     ctermfg=LightYellow guifg=LightYellow guibg=NONE ctermbg=NONE
"" Basic keywords are yellow (for, while, if).
"hi Statement  ctermfg=3 guifg=yellow
"" Comments are green.
"hi Comment    ctermfg=Blue guifg=Green
"" Numbers are blue.
"hi Number     ctermfg=Blue guifg=Green
"" Strings are magenta.
"hi String     ctermfg=5 guifg=magenta
"hi Constant   cterm=NONE ctermfg=White guifg=White
"hi Identifier cterm=NONE ctermfg=White guifg=White
"hi Function   cterm=NONE ctermfg=White guifg=White
"hi PreProc    cterm=NONE ctermfg=White guifg=White
"hi Type	      cterm=NONE ctermfg=White guifg=White
"hi Special    cterm=NONE ctermfg=White guifg=White
"hi Delimiter  cterm=NONE ctermfg=White guifg=White
""""""""""""""""""""""""""""""""
