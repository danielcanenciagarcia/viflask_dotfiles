"""""""""" TEXT INSERTION """"""""""""
function! Enclose(type, ...)
	"let argument=nr2char(getchar())
	if s:right != 1
		let leftargument=input("String: ")
		let rightargument=leftargument
		if leftargument=="("
			let rightargument=")"
		elseif leftargument=="{"
			let rightargument="}"
		elseif leftargument=="["
			let rightargument="]"
		endif
	else
		let leftargument=input("Left String: ")
		let rightargument=input("Right String: ")
	endif


	" Linewise motions (j, k, {, }, etc)
	if a:type == 'line'
		silent! execute ":normal! j{WO" . leftargument
		silent! execute ":normal! }Bo" . rightargument
		" Dont enclose, just insert a character at the beginning
		" of the line
		"silent! execute ":.,+" . s:count . "s/^/" . leftargument . "/g"
	" Charwise motions (w, b, h, l, etc)
	elseif a:type == 'char'
		silent! execute ":normal! i" . leftargument
		silent! execute ":normal! " . s:count . "E"
		silent! execute ":normal! a" . rightargument
	endif
endfunction
function! s:SetupEn(count, right)
	let s:count = a:count
	let s:right = a:right
	set operatorfunc=Enclose
	return 'g@1'
endfunction

"" Enclose text based on count and motion
"  Usage: [count].n[motion][char]
"	Ex: 2.enw", 2.enb< or .en{"""
nnoremap <expr> ,en <SID>SetupEn(v:count1, 0)
nnoremap <expr> ,en2 <SID>SetupEn(v:count1, 1)

function! InsertThis(type, ...)
	let argument=input("String: ")

	" Linewise motions (j, k, {, }, etc)
	if a:type == 'line'
		if s:before
			silent! execute "'[,']s/^/" . argument . "/g"
		else
			silent! execute "'[,']s/$/" . argument . "/g"
		endif
	endif
endfunction
function! s:SetupInsertThis(count, before)
	let s:count = a:count
	let s:before = a:before
	set operatorfunc=InsertThis
	return 'g@1'
endfunction
" Insert lines at the beg/end of lines
"  (or insert text at the beg/end of words)
" Ex: 2.iak"
nnoremap <expr> .ia <SID>SetupInsertThis(v:count, 1)
nnoremap <expr> .ie <SID>SetupInsertThis(v:count, 0)

function! DeleteThis(type, ...)
	let argument=input("Movement to delete: ")
	echo argument
	execute ":normal! j"

	" Linewise motions (j, k, {, }, etc)
	if a:type == 'line'
		if s:before
			let cmd=':normal! ^d' . argument . 'k'
		else
			let cmd=':normal! $d' . argument . 'k'
		endif
		let s:count += 1
		while s:count > 0
			execute cmd
			let s:count -= 1
		endwhile
	endif
endfunction
function! s:SetupDeleteThis(count, before)
	let s:count = a:count
	let s:before = a:before
	set operatorfunc=DeleteThis
	return 'g@1'
endfunction
" Delete things based on movement at te beginning or end of lines
nnoremap <expr> .da <SID>SetupDeleteThis(v:count, 1)
nnoremap <expr> .de <SID>SetupDeleteThis(v:count, 0)

""""""""""""""""""""""""""""""""""""""

""""""""""""" MOVEMENT """""""""""""""
" Make the command :k compatible with the normal command m{mark}
"  - Example of command to execute:
"      cnoreabbrev <expr> kc getcmdtype() == ":" && getcmdline() == 'kc' ? 'normal! mc' : 'kc'
function! FixK()
	for i in range(65,90) " ASCII codes
		let c1 = nr2char(i)  " Character
		let c2 = tolower(nr2char(i))

		" Map :k[a-Z] to :normal! m[a-Z]
		let command1 = "cnoreabbrev <expr> k" . c1 . " getcmdtype() == \":\" && getcmdline() == 'k" . c1 . "' ? 'normal! m" . c1 . "' : 'k" . c1 . "'"
		let command2 = "cnoreabbrev <expr> k" . c2 . " getcmdtype() == \":\" && getcmdline() == 'k" . c2 . "' ? 'normal! m" . c2 . "' : 'k" . c2 . "'"
		execute command1
		execute command2

	endfor
endfunction
call FixK()

""""""""""""""""""""""""""""""""""""""

""" Indentation """
" Command to indent a block using normal command ==
function! IndentInARange() range
	" Do some more things
	execute a:firstline . "," . a:lastline . " :norm =="
endfunction
command! -range Format <line1>,<line2>call IndentInARange()


""" Multiple Cursor """
"" Executes last used register over the current block's lines
"" Usage:
""	1. Store a command in a register o2dd"myy
""	2. Execute it. @m
""	3. Call this function
function! RepeatMultipleCmd()
	let linenr=line(".")
	let colnr=col(".")
	let lastline=line("'}")
	let rcount=lastline - (linenr + 1)
	exec ":normal! " . rcount . "@@"
	exec "call cursor(" . linenr . "," . colnr . ")"
endfunction

"" Repeats last change over the current block's lines
function! RepeatOnBlock(count)
	let linenr=line(".")
	let colnr=col(".")
	let rcount=line("'}") - (linenr + 1)
	" Use current block as a motion
	let motion='`[' . 'j'

	" Restore last cursor position
	exec ":normal " . motion
	" Use a specific range if given
	if a:count != 0
		let rcount=a:count
		let motion='j'
	endif

	" Execute command on the same column number
	exec ":call SameCursor('.', motion , rcount)"
	" Restore initial cursor position
	exec "call cursor(" . linenr . "," . colnr . ")"
endfunction

"" This function executes a command on the same line and column
"" an accepts a count (Ex: makes 2dw work on multiple lines)
function! SameCursor(cmd, motion, count)
	let linenr=line(".")
	let colnr=col(".")
	" Create a macro
	let @r='' . a:cmd . a:motion
	" Execute given command
	exec "normal! " . a:count . '@r'
	exec ":" . linenr
endfunction

""" SEARCHING FILES """
function! FileSearchFind(fname, cmd)
	" Convert $HOME and other env variables
	let parsedName = strtrans(substitute(system('echo ' . a:fname), '\n', '', ''))

	" Search recursively if the file doesnt exists
	if !filereadable(parsedName)
		echo "Doesn´t"
		" If the filename given contains slashes, use
		" iwholename
		let scmd = "!find " . getcwd()
		if stridx(a:fname, '/') && stridx(a:fname, '//') == -1
			let scmd = scmd . " -iwholename '*" . a:fname . "*'"
		else
			let scmd = scmd . " -iname '*" . a:fname . "*'"
		endif

		echo scmd

		let tmp = tempname()
		silent! execute scmd . ' | fzf >'.tmp
		" If Esc is given

		let path = readfile(tmp)[0]
		silent! execute '!rm '.tmp
		execute a:cmd . ' ' . path
		return
	endif
	" Open the file directly if it exists
	execute a:cmd . ' ' . a:fname
	return
endfunction

""" DIFF """

""" GIT """
function! FzfSelect()
	let tmp = tempname()
	silent! execute '!fzf >'.tmp
	let fname = readfile(tmp)[0]
	silent! execute '!rm '.tmp
	execute 'split ' . fname
endfunction

function! FzfGitGrep()
	let tmp = tempname()
	silent! execute "!git grep -n '" . expand('<cword>') . "' | fzf >" . tmp
	let ret = split(readfile(tmp)[0], ':')
	let fname = ret[0]
	let linenr = ret[1]
	echo fname
	echo linenr
	silent! execute '!rm ' . tmp
	silent! execute 'split +' . linenr . ' ' . fname
endfunction

""" TAGS """
func PreviewWord()
	if &previewwindow			" don't do this in the preview window
		return
	endif

	let w = expand("<cword>")		" get the word under cursor

	if w =~ '\a'			" if the word contains a letter
		" Delete any existing highlight before showing another tag
		silent! wincmd P			" jump to preview window
		if &previewwindow			" if we really get there...
			match none			" delete existing highlight
			wincmd p			" back to old window
		endif

		" Try displaying a matching tag for the word under the cursor
		try
			exe "ptag " . w
		catch
			return
		endtry

		silent! wincmd P			" jump to preview window
		if &previewwindow		" if we really get there...
			if has("folding")
				silent! .foldopen		" don't want a closed fold
			endif
			call search("$", "b")		" to end of previous line
			let w = substitute(w, '\\', '\\\\', "")
			call search('\<\V' . w . '\>')	" position cursor on match
			" Add a match highlight to the word at this position
			hi previewWord term=bold ctermbg=green guibg=green
			exe 'match previewWord "\%' . line(".") . 'l\%' . col(".") . 'c\k*"'
			wincmd p			" back to old window
		endif
	endif
endfun

