"" Vi-ish colorscheme

let g:colors_name = 'huno'

" No syntax highlighting
syntax off
highlight clear

let s:t_Co = has('gui_running') ? -1 : (&t_Co ?? 0)
" Set gui colors
if (has('termguicolors') && &termguicolors) || has('gui_running')
	let g:terminal_ansi_colors = ['#ffffff', '#0000ff', '#00cd00', '#cd00cd', '#008b8b', '#0000ff', '#ff1493', '#bcbcbc', '#ee0000', '#0000ff', '#00cd00', '#cd00cd', '#008b8b', '#0000ff', '#ff1493', '#000000']
endif


" Make bg transparent
hi Normal       cterm=NONE ctermfg=NONE ctermbg=NONE guifg=White guibg=Black
" Link every other group to normal
for hl in getcompletion('', 'highlight')
	execute "hi! link " . hl . " Normal"
endfor

" Make Visual selection work
" (avoids message warning: terminal cannot highlight)
hi Visual guifg=#000000 guibg=#d0d0d0 gui=NONE cterm=NONE ctermfg=DarkYellow ctermbg=DarkGray
hi VisualNOS guifg=NONE guibg=#ee0000 gui=NONE cterm=NONE

" Cursor color
" ICursor and Cursor do not work in terminals
" Cursor identifiers
hi CursorColumn	cterm=bold ctermbg=Black ctermfg=NONE
hi CursorLine	cterm=underline ctermfg=White ctermbg=NONE
" Change Color when entering Insert Mode
autocmd InsertEnter * highlight CursorColumn cterm=NONE ctermfg=NONE ctermbg=NONE
" Revert Color to default when leaving Insert Mode
autocmd InsertLeave * highlight CursorColumn cterm=bold ctermbg=Black ctermfg=NONE

" Diff Mode
hi DiffText	cterm=bold ctermfg=NONE ctermbg=NONE gui=bold guifg=Black guibg=Gray
hi DiffAdd	cterm=bold ctermfg=Green ctermbg=NONE guibg=LightGray guifg=Black
hi DiffChange	cterm=bold ctermfg=Red ctermbg=NONE guibg=LightGray guifg=Black
hi DiffDelete	cterm=nocombine ctermfg=NONE ctermbg=NONE gui=bold guifg=DarkGray guibg=Black
hi SignColumn 	cterm=bold ctermfg=NONE ctermbg=NONE gui=bold guifg=NONE guibg=NONE

" Some modifications
hi LineNr 	cterm=NONE ctermfg=NONE guifg=NONE
hi CursorLineNr	cterm=bold ctermfg=NONE ctermbg=NONE
hi Error	cterm=bold ctermbg=Red ctermfg=White gui=bold guibg=Red guifg=White
hi MoreMsg	cterm=bold ctermfg=DarkGreen gui=bold guifg=SeaGreen
hi ModeMsg	cterm=bold ctermfg=DarkGreen gui=bold guifg=SeaGreen
hi Question	cterm=bold ctermfg=DarkGreen gui=bold guifg=SeaGreen
hi Search	cterm=bold ctermbg=DarkGray ctermfg=Yellow gui=bold guibg=Black guifg=Yellow
"hi Search	guifg=Black gui=NONE ctermfg=Black cterm=NONE
hi IncSearch 	gui=standout cterm=standout
hi MatchParen	cterm=NONE ctermbg=DarkYellow ctermfg=Black
hi! link	CurSearch IncSearch
hi! link 	VertSplit IncSearch
hi TabLine	gui=bold guibg=NONE guifg=White cterm=bold ctermbg=NONE ctermfg=White
hi NonText 	guifg=#bcbcbc guibg=NONE gui=NONE cterm=bold ctermbg=NONE ctermfg=NONE
hi SpecialKey 	guifg=#bcbcbc guibg=NONE gui=NONE cterm=bold ctermbg=NONE ctermfg=NONE
" Spelling
hi SpellBad	cterm=bold ctermfg=Red ctermbg=NONE
hi SpellCap	cterm=bold ctermfg=Magenta ctermbg=NONE
hi SpellLocal	cterm=bold ctermfg=Blue	ctermbg=NONE
hi SpellRare	cterm=bold ctermfg=Green ctermbg=NONE
" Popup menu (completion)
hi Pmenu	cterm=NONE ctermbg=NONE ctermfg=NONE
hi PmenuSel	cterm=bold ctermbg=NONE ctermfg=NONE
hi PmenuSbar	gui=NONE cterm=NONE
hi! link	PmenuSbar PmenuThumb
" Folding
hi Folded	cterm=bold ctermbg=DarkGray ctermfg=Black
hi! link	FoldColumn SignColumn
" Statusline colors (see fillchars' stl, slnc options)
hi StatusLine	cterm=bold
hi StatusLineNC	cterm=nocombine
hi! link	StatusLineTerm StatusLine
hi! link	StatusLineTermNC StatusLineNC
""""""""""""""""""""""""""""
