"##################################
"##          MAPPINGS            ##
"##################################

" Leader key
let mapleader = " "

"##################################
"## Essential nvi / vi commands ##"
"##################################

" Remap jj as <Esc>
imap <nowait> jj <ESC>

" Move to line N using N<CR>
nnoremap <CR> Gz.
augroup GotoLine
  autocmd!
  autocmd CmdwinEnter * nnoremap <buffer> <CR> <CR>
  autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>
augroup END

" Place search result at the middle of the screen
nnoremap n nzz
nnoremap N Nzz

" Save a mark when jumping to the beg/end of the file
"nnoremap 1G mj1G
"nnoremap <nowait> gg mjgg
"nnoremap <nowait> GG mkG

" Hit HH or LL multiple times to move the screen
" one page forwards
nnoremap <silent> H :let p=getline('.')<CR>H:if (getline('.') == p)<bar>normal! zbM<CR>endif<CR>
nnoremap <silent> L :let p=getline('.')<CR>L:if (getline('.') == p)<bar>normal! ztM<CR>endif<CR>

" Remove Visual selection
nnoremap v <Nop>
nnoremap V <Nop>

" Delete lines without storing them in any register
"nnoremap ,d :normal! "_dd<CR>
"" Delete things, ,daw / ,di( / ,di" / ,dap
nnoremap ,d :normal! "_d
nnoremap ,D :normal! "_D<CR>

" Change jump to alternate file mapping
nnoremap <bs> <c-^>
" Jumpt to marks easily
"nnoremap ' `

" In command mode, %% translates to the path of the current buffer.
" It allows you to open files in the same directory as the current
" buffer. Legacy: %% translates to the absolute path ot the current
" file.
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

"##################################
"##    Yanking and Pasting      ###
"##################################
" Paste mode
" map pa <Esc>i<C-R>=systemlist('xsel --clipboard --output')<CR><ESC>kJ

"##################################
"##    File/Buffer Management   ###
"##################################
" Search buffers easily (type ,b and a few chars followed by a couple of tabs)
" 	- Paths will be ignored (most times)
nnoremap <leader>b :ls<CR>:b
" Switch buffers easily
nnoremap <leader>bs :b #<CR>
nnoremap <leader>bp :bp <bar> bd #<CR>
" Add and delete files to the argument list
nnoremap <leader>A :argadd<space>
nnoremap <leader>a :args<CR>
nnoremap <leader>ad :argdel<space>
" Edit a file in split mode
nnoremap <leader>s :split<space>
nnoremap <leader>S :vsplit<space>
" Search files to edit in every directory (depends on set wildcharm=<Tab>)
" You can use :find instead of edit, and it will search in the directories
" specified with set path too (recommend doing set path-=/usr/include)
" 	-See: https://vimways.org/2018/death-by-a-thousand-files/
nnoremap <leader>es :e **/*<Tab><S-Tab>
" Open file under the cursor
nnoremap <leader>f yiW:<C-E>o:split **/<Esc>p<Esc>A*
"nnoremap <leader>f :call FileSearchFind()<CR>

" Open files with an specific extension
nnoremap <leader>* :args **/*.

"##################################
"##    SEARCHING / REPLACING    ###
"##################################
"Delete the line containing the first search match
"" (Add a count to delete next N lines)
function! CmdLinesFrom(cmd)
	execute "normal! m`"
	let cmd="://" . a:cmd . '' . v:count1 . " | normal! ``"
	call feedkeys('q:', 'in')
	call feedkeys('i')
	call feedkeys(cmd)
	call feedkeys("\<ESC>F/i")
endfunction
nnoremap D/ :call CmdLinesFrom('d')<CR>
nnoremap Y/ :call CmdLinesFrom('y')<CR>

" Toggle highlighting in search matches
nnoremap \h :set hlsearch!<CR>

" Search for the word under the cursor in a split window
nnoremap .s* :set hls<CR>:pedit %<CR><C-w><C-w>*zt5<C-y>
nnoremap .s# :set hls<CR>:pedit %<CR><C-w><C-w>#zt5<C-y>

"" Replace the word under the cursor (keep pressing . to keep replacing)
" Not case sensitive
nnoremap .r* :set hls<CR>*``cgn
nnoremap .r# :set hls<CR>#``cgN
" Case sensitive
nnoremap .r*c :set hls<CR>/\<<C-R>=expand('<cword>')<CR>\>\C<CR>``cgn
nnoremap .r#c :set hls<CR>/\<<C-R>=expand('<cword>')<CR>\>\C<CR>``cgN
" Change all ocurrences of the word under the cursor
nnoremap .ra :%s/\<<C-R>=expand('<cword>')<CR>\>/
nnoremap .rac :%s/\c\<<C-R>=expand('<cword>')<CR>\>/

"" Delete the word under the cursor (keep pressing . to keep deleting)
" Not case sensitive
nnoremap .d* :set hls<CR>*``dgn
nnoremap .d# :set hls<CR>#``dgN
" Case sensitive
nnoremap .d*c :set hls<CR>/\<<C-R>=expand('<cword>')<CR>\>\C<CR>``dgn
nnoremap .d#c :set hls<CR>/\<<C-R>=expand('<cword>')<CR>\>\C<CR>``dgN

"##################################
"##          SPELLING           ###
"##################################
nnoremap \s :set spell!<CR>

"##################################
"##       CHANGING CASE         ###
"##################################
" Change current line case
nnoremap <leader>cl ~~
" Change current {word, line, etc} case (Can take motions)
" 	Ex: cww / cwj
nnoremap <leader>cc g~
" Convert line to Title Case (change the first letter of every word)
nnoremap <leader>ctl :silent s/\<\(\w\)\(\S*\)/\u\1\L\2/g<CR>
" Title Case current word
"  (have to be placed after the last mapping for nowait to work)
nnoremap <leader>ct ~l

"##################################
"##        COMPLETION           ###
"##################################
" Cycle though completion popup window
"inoremap <expr> j pumvisible() ? "\<C-n>" : "j"
"inoremap <expr> k pumvisible() ? "\<C-p>" : "k"

"##################################
"##        INDENTATION          ###
"##################################
" Indentation Jumping (jump to the next line with the same indentation level)
"	- Alt-j / Alt-k
nnoremap <silent> <M-k> :call search('^'. matchstr(getline('.'), '\(^\s*\)') .'\%<' . line('.') . 'l\S', 'be')<CR>
nnoremap <silent> <M-j> :call search('^'. matchstr(getline('.'), '\(^\s*\)') .'\%>' . line('.') . 'l\S', 'e')<CR>

" Indentation
"nnoremap <leader>f :execute "'',.Format"<CR>
" Format the current paragraph (see :h '(, ']. etc)
" nnoremap <leader>t :'{,'}Format<CR>
" Autoindent the current paragraph (can use >ap to shift right afterwards)
" nnoremap <leader>f :'{,'}g/.*/:normal! ==
nnoremap <leader>f }{E:ka<CR>}:kp<CR>:'a,'pg/.*/:normal! ==<CR>
" Shiftright current paragraph
" nnoremap <leader>	>ap
nnoremap <leader><Tab> }{E:ka<CR>}:kp<CR>:'a,'pg/.*/:normal! >><CR>


"##################################
"##      MULTIPLE CURSORS       ###"
"##################################

" Repeat last change on current block (same column number)
nnoremap ,r :<C-u>call RepeatOnBlock(v:count)<CR>

" MULTIPLE CURSORS (use ,r instead)
" 	- Insert or delete wanted text
" 	- Use @: to keep repeating it
" 	- mj and mk suppose you used the append command to insert text
nnoremap ,mj :let @m='BjE.'<CR>@m<ESC>
nnoremap ,mk :let @m='BkE.'<CR>@m<ESC>
nnoremap ,md :let @m='dWj'<CR>@m<ESC>
nnoremap ,mc :let @m='jB.'<CR>@m<ESC>
nnoremap ,mr :call RepeatMultipleCmd()<CR>

" MULTIWORD
" 	- Insert text at start/end of word
" 	- Use @: to repeat the change
" 	- ,mw suppose you used a to insert text
" 	- ,mb suppose you used i to insert text
"nnoremap ,mw E.
"nnoremap ,mb BB.
"nnoremap ,mC W.

"" ENCLOSE THINGS (see .vim/functions.vim)
"nnoremap ,en => 2.enw" enclose the next two words using the char "
"nnoremap ,en2 => same as .en but prompts for the left and right string

"" Beginning / End of lines (defined in .vim/functions.vim)
"nnoremap .ia => 2.iaj" inserts char " in the next 3 lines
"nnoremap .ie => 2.iej" inserts char " at the end of the next 3 lines
"nnoremap .da => 2.daw deletes the first word of the next 3 lines
"nnoremap .de => 2.deb deletes last word at the end of the next 3 lines

"" Multiline (yank a word using yw before)
"	1. "myw (yank word into buffer m) or "myt<space>
"	2. Move to the desired line and column
"	2. Execute command
"	4. Use . to repeat for the next line
"nnoremap <leader>m i "mphBXj

" MULTICURSOR
" Copy last inserted text in the next line and on the same column
" 	- Use @: to repeat this commands
"map <leader>mi :normal! .bj<CR>
"map <leader>md :normal! dwj<CR>
"map <leader>mc :normal! .dt bj<CR>


"##################################
"##         COMMENTING          ###"
"##################################

"" !!! Note: To clear register a, use qaq or let @a = ''
" Enclose text based on motion (Ex: 2.enw)
"nnoremap .en :<C-U>call <SID>Enclose(v:count1)<CR>
"nmap .n :set opfunc=Enclose<CR>g@

"##################################
"##          PARSERS            ###"
"##################################
" Remove trailing whitespaces
map \t :set nomagic <bar> :%s/\s\+$//g<CR>``z.

"##################################
"##           DIFFS             ###"
"##################################
"
" Vimdiff mode
command -nargs=1 -complete=file -bar Diff :vert diffsplit <args>
" Show a diff of the current commit
command DiffGitCached :!git diff --cached
" See the diffs with the file a buffer was loaded from
command! DiffOrig let $fname=expand('%:p') | vert new | set bt=nofile | r ++edit $fname | 0d_
	\| set readonly | diffthis | let $gobuffer=bufnr("%") | wincmd p | diffthis

" Get current buffer name (and later delete it)
command! GetBuffNr let $gobuffer=bufnr("%")
command! BdBufferNr execute 'bd' . $gobuffer | unlet $gobuffer

" Diff origin
nnoremap <silent> go :DiffOrig<CR>
" Diff changes since last commit
nnoremap <silent> gh :new<CR>:r !git diff HEAD<CR>:diffthis<CR>:setlocal ro nomodified<CR>:GetBuffNr<CR>
" Diff Current Commit staged changes
nnoremap <silent> gc :new<CR>:r !git diff --cached<CR>:setlocal ro nomodified<CR>:GetBuffNr<CR>
" Diff current file and its HEAD revision
nnoremap <silent> gd :new<CR>:r !git diff HEAD %<CR>:setlocal ro nomodified<CR>:GetBuffNr<CR>
" Diff current file and its HEAD revision side-by-side
nnoremap <silent> gs :new<CR>:r !git difftool -y -x "diff -yt" HEAD %<CR>:setlocal ro nomodified<CR>:GetBuffNr<CR>
" Destroy diff buffer
nnoremap <silent> gD :BdBufferNr<CR>

"##################################
"##            GIT              ###"
"##################################
"" GIT Related Commands
" Navigating files in a git project
nnoremap <silent> gf :call FzfSelect()<CR>
" Git Blame
nnoremap <silent> gb :terminal<CR>git blame %
" Multifile search (search for a word in every known file in current path)
map gg wb"zye:<C-r>o:!git grep <ESC>"zp<ESC><CR>
" Start editing the file at the correct line number
"	- ^V| inserts |
"	- :edit inserts the file selected in fzf
"	- git grep uses -n to show the line number
nnoremap <silent> ge :call FzfGitGrep()<CR>

"##################################
"##           TAGS             ###"
"##################################
" Search for a tag automatically on .c and .h files
"au! CursorHold *.[ch] nested exe "silent! ptag " . expand("<cword>")
" Simulate the existance of a tag (can be slow)
au! CursorHold * nested exe "silent! psearch " . expand("<cword>")

" Close the preview window
nnoremap Q <C-w><C-z>
nnoremap ,Q <C-w><C-z>

"##################################
"##       ABBREVIATIONS         ###"
"##################################
iab <expr> idate strftime('%m/%d/%Y')

"##################################
"##         SNIPPETS            ###
"##################################
inoremap {<cr> {<CR>};<ESC>O<ESC>z.
