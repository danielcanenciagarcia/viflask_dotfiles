"""
" Source common .sexrc
source $HOME/.sexrc

""" Notes """
" - Don't use relative numbers. Guess numbers and
"   learn touch typing
" - Use d/<search>/+0 and y/<search>/+0 to
"   yank and delete lines till the search match (works charwise)
"   (d//<CR> to delete to the next occurence of the last search,
"   d//e<CR> to include the match in the deletion) (Same with yank)
" - Or q:/:<C-e> .,/<search> d,y (Can use autocompletion after <C-e>
"   (C-e cancels autocompletion, C-y/C-m/C-g/Enter selects a match)
" - Use :[l1]m/<search>+N to move line l1 to next search match
"   (works linewise)
" - Use --remote to open vim as a client (needs +clientserver).
"   Use --servername to specify a name fot the server.
"   Send files to this remote session using --remote-send
"       - vim --remote-send ":e %/<name>" or ":echo 'hello'<CR>"
" - diff -qr <dir1> <dir2> to list the differences
"   between two directories. Add | grep Files to
"   list only common file names
" - git difftool -d <branch1> <branch2> to list the differences
"   between two branches using a diff tool, such as vimdiff
"   diff -y or diff -u (git config difftool='diff -u')
"   Ex: git difftool --extcmd "diff -yt --color"

""" Local Vimrc suggestions ""
" * Set directories commands like :e and :find look into
" (Note: Better include this command in a local .vimrc
" setlocal path+=.front/src/js
" * Go to file command (gf or gF(more powefull)), which is super useful
" 	- To look for files with a .js extension:
" setlocal suffixesadd+=.js
" This options define how include search commands work
" See: https://vimdoc.sourceforge.net/htmldoc/tagsrch.html#include-search
" 	* Tell vim how an include command looks like
" 	  (to be used with fplugins, like javascript.vim)
"		setlocal include=from
"		setlocal include=^\\s*[^\/]\\+\\(from\\\|require(['\"]\\)
"	* See includeexpr
"	* Tell vim how a function signature looks like
" 		setlocal define=^\\s*function
" 		setlocal define=class\\s
""""""""""""""""""""""""""""""

"""" Vi Improved specific """"
" Default to shitfwidth's value
set softtabstop=0


""" Unset Vi Compatibility """
set nocompatible
" Vi fixes (Make vim behave more like vi)
" Down allow backspacing
set backspace=""
" Or only allow backspacing over tabs
set backspace=indent
" Use C-R to open cmd line history
" (C-S or C-R because it doesnt get in the way of other commands)
set cedit=
" Copy last line indentation when autoindenting
set copyindent
" Reset cpoptions to vi default
set cpoptions&vi
" Remove some vi compatible options
set cpoptions-=b
set cpoptions-=C
set cpoptions-=D
set cpoptions-=H
set cpoptions-=i
set cpoptions-=I
set cpoptions-=j
set cpoptions-=J
set cpoptions-=k
set cpoptions-=l
set cpoptions-=L
set cpoptions-=m
set cpoptions-=o
set cpoptions-=O
set cpoptions-=p
set cpoptions-=q
set cpoptions-=r
set cpoptions-=w
set cpoptions-=W
set cpoptions-=X
set cpoptions-=y
set cpoptions-=Z
set cpoptions-=%
set cpoptions-=-
set cpoptions-=<
set cpoptions-=;

" Final Options included
"set cpoptions="aABcdeEfFgKMnPsStuvx!$+*>"

" Make :s% repeat last subtitution command
set cpoptions+="/"
set cpoptions+="{"
"""""""""""""""""""""""""""""

""" Vim defaults plugins
filetype plugin on
let g:plugin_exec = 1
" Map plugin (:Man command)
" Can pressed C-] to jump to the info page
" for the word under cursor (word(1))
" Use K to get vim help for the word under cursor
" Use \K to view the man page for the word under cursor
runtime ftplugin/man.vim
" Git plugins
runtime ftplugin/git*.vim
" If you want to use and ftplugin add its files to .vim
"	- See: https://www.vim.org/scripts/script.php?script_id=1654
" Matchit package
packadd matchit

""" Miscellaneous
" Faster scrolling
set ttyfast
" Don´t reload the screen too often (in the middle of macros)
set lazyredraw
" Update Time (10 seconds)
set updatetime=10000
" Leader key timeout (ms)
set timeoutlen=400
" Ensure <Esc> is recognized assap
" (keys starting with <Esc> not recognized in Insert mode)
set noesckeys
" Clipboard (copy yank text to system's clipboard automatically)
"	- "*p to paste from the system's clipboard,
"	  and "*y to copy from it
"	- "*y not available when compiled without +clipboard
set clipboard=unnamed

""" Undos
" Write undo history to a file
set undodir=~/.vim/undos//
if has('persistent_undo')
	set undofile
endif
" Maximum number of undos
set undolevels=500
" Maximum saved cmds
set history=100

""" Scrolling
" Scroll horizontally when cursor moves off the screen
set sidescroll=1
set sidescrolloff=0
" Keep cur line in the middle of the window
"set scrolloff=999
" Dont move the window when not needed
set scrolloff=0

""" Movement
"set jumpoptions=stack
set iskeyword=@,48-57,_,128-167,224,235

""" Searches
" Don´t wrap around the file (use ggn to search from the top)
set nowrapscan
" Hightlight searches temporarely
set incsearch
" Mark trailing spaces as errors
"match IncSearch '\s\+$'
" Case insensitive if not upper case chars are used
set ignorecase smartcase
" Enable magic mode
set magic
" Use \v to use Extended Regular Expressions (egrep like)
" Example: %s/\v([[:space:]])/

""" Tags
set tags+=./tags;
" Follow smartcase and ignore case options
set tagcase=followscs
" Show more info
set showfulltag
" Height of the preview window
set previewheight=15 winheight=13
""" Autogenerate tags file
" Auto generate tags file on file write of *.c and *.h file
"autocmd BufWritePost *.c,*.h silent! !ctags . &

""" Less specific options
func LessInitFunc()
	set nocursorcolumn nocursorline
endfunc

""" Managing files

" Use curr directory when browsing files
set browsedir=buffer
" Ignore case when searching files
set fileignorecase

""" Backups and Recovering files (see patchmode)
set backup
set backupdir=~/.vim/backups//
" Don't create swap files
set noswapfile
" Meaningful backup names
autocmd BufWritePre * let &bex = '@' . strftime("%F.%H:%M")
" Make temporal backups
set writebackup
" Overwrite original backups
set backupcopy=yes,breaksymlink,breakhardlink

""" Info to be saved
set viminfo='100,<50,s10,%,!,:10
" Preserve cursor position
autocmd BufReadPost *
			\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
			\ |   exe "silent! normal g`\"z."
			\ | endif

""" Sessions
set sessionoptions+=buffers,curdir,globals
set sessionoptions+=options,resize,tabpages
set sessionoptions+=terminal,winpos,winsize

""" Buffers
set switchbuf=usetab,vsplit,uselast
" Disable warning when changing buffers
set hidden

""" Splits
set equalalways
set eadirection=hor
set fillchars=vert:\|,fold:-,eob:~,diff:\ ,stl:\ ,stlnc:\=

""" Autocompletion
"" Wildmenu
" Enable wildmenu
set wildmenu wildignorecase
set wildchar=<Tab>
set wildcharm=<Tab>
set wildmode=longest,list:longest
"set wildoptions=fuzzy,tagfile
set wildoptions=tagfile
" File extensions to be ignored
set wildignore=*.o,*.obj,*.git
"" Insert mode completion
set completeopt=preview,menu
"" Fuzzy search finding
"" :find <fname> will search for files with the name fname in
"" subsequents directories
"" :find ./**/*<Tab> will give suggestions recursively
set path+=**

" Clever Tabs
"function! CleverTab()
"   if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
"      return "\<Tab>"
"   else
"      return "\<C-N>"
"   endif
"endfunction
"inoremap <Tab> <C-R>=CleverTab()<CR>

""" Autoindent
" !! Note: Use :r !cat to paste text
filetype indent off
" Enable autoindent
" 	- Use C-D in insert mode to delete a tab
" 	- Use 0C-D (in insert mode) to erase every tab
set autoindent smartindent
set fixendofline

""" Wrapping
set nowrap
set list listchars=nbsp:*,extends:>,precedes:<,trail:-,tab:\ \ 
set textwidth=100

""" Diff
"set diff

""" Spelling
" Use \s to toggle spell checking
set nospell
set spelllang=en_gb
" Set dictionary to use by page on word completion (C-x C-k)
set dictionary=/usr/share/dict/words
" Dont apply the langremap option to mappings
set nolangremap

""" Folding (dont really use it)
set nofoldenable
set foldcolumn=2
set foldmethod=indent

""" Interface specific
" Dont highlight matching parenthesis
let g:loaded_matchparen=1
" Display signs in the number column
set signcolumn=number
set shortmess-=flnxOS
set shortmess+=otiIT
" Number of screens to use for the command line
"set cmdheight=2
" Use a different cursor whitin insert mode
" Insert mode (change cursor shape)
let &t_SI = "\e[3 q"
" Replacement mode color
let &t_SR = "\e]12;#a0a0a0\a\e[2 q"
" Normal mode cursor color (match terminal cursor color)
let &t_EI = "\e]12;#bba03d\a\e[2 q"

" Ruler
set ruler
" Numbers (!! don´t use them, guess them, and learn touch typing)
"set relativenumber
" Set cursor identifiers
set cursorline cursorcolumn
set cursorlineopt=number
""" Modeline
set modeline

""" Disable vim plugins
" Netrw
let loaded_netrwPlugin = 1

""" Platform Specific """
" requires PLATFORM env variable set (in ~/.bashrc)
if $PLATFORM == 'mac'
  " required for mac delete to work
  set backspace=indent,eol,start
endif
"""""""""""""""""""""""""

""""""""""" Theme  """""""""""""""
set notermguicolors
"source $HOME/.vim/theme.vim
source $HOME/.vim/huno.vim
""""""""""""""""""""""""""""""""""

"""""""""" Statusline """"""""""""
" Always show the tabline
" Show the argument list index and
" the date on the tabline
"set showtabline=2 tabline=%a\ %f%=%{strftime('%c')}

" Nvi like statusline
set laststatus=2
"set statusline=%q%t\ %LL%=%l:%c\ %P%=
"set statusline=
"set statusline+=%w%q
"set statusline+=%t
"set statusline+=%04{&modified?'':'\:\ unmodified'}
"set statusline+=%04{&readonly?'\:\ readonly':''}
""set statusline+=\ %04r
""set statusline+=\ \ line\ %l\ of\ %L
""set statusline+=\ %LL
"set statusline+=%=
""set statusline+=%S
"set statusline+=\ %a
"set statusline+=\ %l:%c
"set statusline+=%05P

" Use the ruler instead of the statusline
autocmd BufRead,BufNewFile * set laststatus=0 " This will work instead
" Show file info in the last line
"autocmd InsertLeave * :file!
" Rulerformat
set ruler rulerformat=%-25(%t\ \ %=%c%V\ %p%%%)
" Show partial command in the statusline
" (use %S to place this text in the statusline)
"set showcmd
" Show current mode in the statusline
set showmode
" Show current window title
set title
" Show usefult information in the titlestring
" 	- Like arguments, path, modified flag
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:.:h\")})%)%(\ %a%)
""""""""""""""""""""""""""""""""""

"""""""""""  Maps  """""""""""""""
" Source functions
source $HOME/.vim/functions.vim
" Source mappings
source $HOME/.vim/mappings.vim
""""""""""""""""""""""""""""""""""

"""""""""""  Guide """""""""""""""
source $HOME/.vim/guide.vim
""""""""""""""""""""""""""""""""""

" Read other configurations (keep last)
set rtp^=~/.vimwork
set rtp^=~/.vimtesting
