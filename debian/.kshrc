# .kshrc file

# skip this setup for non-interactive shells
[[ -o interactive && -t 0 ]] || return

# handle bash/zsh SHLVL variable
(( SHLVL++ ))

# disable core dumps
ulimit -c 0

## Set ENV if there is an rc file
#if [ -f $HOME/.kshrc -a -r $HOME/.kshrc ]; the
#	ENV=$HOME/.kshrc
#	export ENV
#fi

# Mailbox location
#MAIL=/usr/spool/mail/$LOGNAME
#export MAIL

# Environment variables. These could go in .profile if you prefer
export VISUAL=vi
export EDITOR=$VISUAL
export PAGER=less
export XDG_CONFIG_HOME="$HOME/.config"


##### Pagers #####
alias most='most -w -r +u'
# +s => secure mode
alias diff="diff --color=always"
alias man="man -P less"

### Less configuration
export LESS="-RNWSs --incsearch -FigJd -x4 -z28 -j20 --window=5 \
	--quit-if-one-screen --status-col-width=1 --no-histdups \
	Ps(file %i/%m) =>  %f (%lm)$ -P=(%Pb\%) (file %i/%m) =>  %f (%lm)$"
# Less as the Git pager
git config --get core.pager "less"
# Use global tags
export LESSGLOBALTAGS="global"
export LESSQUIET=1
# Make the v command open the cur and next files
export LESSEDIT="%E ?lm+%lm. %g ?x%x."
# Make the v command respect initial arguments
alias aless='f(){ export ARGLIST="${@}"; LESSEDIT="%E \$ARGLIST" less ${@}; unset -f f; }; f'
###
#################


# Avoid certain file types in completion
FIGNORE='@(*.o|~*)'

# save more commands in history
HISTSIZE=500
HISTEDIT=$EDITOR

###### ALIASES ######
# Aliases for editors
export vi="/bin/bash avi"
alias ed="sed -ruf ~/.edrc | /bin/ed -p '>> ' -v"
#alias ed="ed -p '>> ' -v"

### Utilities
# Trim whitespaces (usage: trim **/*.php)
alias trim='printf "set extended\n%%s/[[:space:]]+$//g\nwq\n" | ex -'
# Change tabs to spaces (usage: retab **/*.php)
alias retab='printf "%%!expand -t 2\nwq\n" | ex -'

### Browsers
alias br="lynx-google"

# Aliases for various command shortcuts
alias dir='dir --color'
alias ls='ls --color -lFhv --author --time-style=long-iso'
alias ll='ls --color -alFhv --author --time-style=long-iso'
alias la='ls --color -AFhv --author --time-style=long-iso'
alias l='ls --color -CFhv --author --time-style=long-iso'
#alias ll='ls -lFb'
#alias la='ls -LaFb'

# Avoid problems with long argument lists for some commands (like xargs)
alias cp='command -x cp'  mv='command -x mv'  grep='command -x grep'
####################

# Some functions
function pid { ps -e | grep $@ | cut -d" " -f1; }
function back {
	cd $OLDPWD
	echo $CWD $OLDPWD
}
empty() { echo $'\e[3J'; }
mere() { nroff -man -Tman $1 | ${MANPAGER:-less}; }
setenv() { export "${1}${2:+=$2}"; }

### Fix some control keybindings
set -o emacs
keybd_trap () {
  case ${.sh.edchar} in
    $'\f')    .sh.edchar=$'\e\f';;  # clear-screen (THIS QUESTION)
    $'\e[1~') .sh.edchar=$'\001';;  # Home = beginning-of-line
    $'\e[4~') .sh.edchar=$'\005';;  # End = end-of-line
    $'\e[5~') .sh.edchar=$'\e>';;   # PgUp = history-previous
    $'\e[6~') .sh.edchar=$'\e<';;   # PgDn = history-next
    $'\e[3~') .sh.edchar=$'\004';;  # Delete = delete-char
  esac
}
trap keybd_trap KEYBD
###

# Vi-like keybindings
set -o vi
# Custom vi normal mode commands
set keymap vi-command
# Custom vi insert mode commands
set keymap vi-insert


# Options
set -o markdirs posix tabcomplete trackall xtrace
# Don'let C-d logout
set -o ignoreeof
#showme

# put the current directory and history number in the prompt
HOST=`hostname`
#PS1='${USER}:${HOST} (${PWD})$_pwd [!]\$ '
PS1='${USER}:${HOST}$_pwd [!]\$ '
