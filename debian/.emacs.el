;; Emacs init.el

;; Prevent the glimpse of un-styled Emacs by disabling these UI elements early.
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    	Colors	          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Disable syntax highlighting
(global-font-lock-mode 0)
;; Load default theme
(load-theme 'wheatgrass t)
(add-to-list 'default-frame-alist '(foreground-color . "FloralWhite"))

;; Minibuffer color
(set-face-foreground 'minibuffer-prompt "white")
;; Line number color
(set-face-foreground 'line-number "gray")
(set-face-foreground 'line-number-current-line "brightyellow")
;; Highlight current line
(global-hl-line-mode)
;; Set current line colors
(set-face-background hl-line-face "gray10")
(set-face-foreground hl-line-face "spring green")
;;(set-face-foreground hl-line-face "LightGoldenrod1")
;;(set-face-foreground hl-line-face "snow1")

;; Don't change minibuffer colors
(copy-face 'default 'viper-minibuffer-vi-face)
(copy-face 'default 'viper-minibuffer-insert-face)
(copy-face 'default 'viper-minibuffer-emacs-face)
;; Change replacement colors
;;(set-face-foreground viper-replace-overlay-face "DarkSlateBlue")
;;(set-face-background viper-replace-overlay-face "yellow")


;;;;; Terminal specific
;; Transparent background
(defun on-after-init ()
  ;; Make background transparent (tty only)
  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))
(add-hook 'window-setup-hook 'on-after-init)
;; Change cursor shape (TTY)
(add-hook 'viper-insert-state-hook (lambda () (send-string-to-terminal "\033[4 q")))
(add-hook 'viper-vi-state-hook  (lambda () (send-string-to-terminal "\033[2 q")))
(add-hook 'viper-emacs-state-hook (lambda () (send-string-to-terminal "\033[2 q")))

;; X specific
;; Make cursor reflect current mode (X mode)
(setq viper-insert-state-cursor-color "DarkSlateBlue")
(setq viper-emacs-state-cursor-color "Red")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      General           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Show relative line numbers
(setq display-line-numbers-type 'relative) 
(global-display-line-numbers-mode)
;; Show column number
(setq column-number-mode t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Indentation       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
;;(setq         tab-width 4)
;;; Custom indent settings for different modes
;; Makefile
(defun makefile-settings-hook ()
  (setq indent-tabs-mode t))
(add-hook 'makefile-mode-hook 'makefile-settings-hook)


;; Use C-q to insert a literal tab
;; <Tab> is bound to indent-for-tab-command, bind it to (insert "\t")
;; to use just like you would do in every other editor

;; Use C-j to autoindent newlines, or uncomment this
;;(define-key global-map (kbd "RET") 'newline-and-indent)
;; Use C-x C-o to delete tabs (even if using tabs as spaces)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Autocomplete      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use C-tab to complete
;; See dabbrev-completion
;;(global-set-key (kbd "C-<tab>") 'dabbrev-expand)
;;(define-key minibuffer-local-map (kbd "C-<tab>") 'dabbrev-expand)

;; Use hippie expand with M-/, instead of dabbrev mode
(global-set-key [remap dabbrev-expand] 'hippie-expand)
;; Find references using C-S-/
;; Find definitions using M-.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Searching         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use ido mode. For C-x b and C-x f
(ido-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Modal Editing     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq viper-mode t)
(require 'viper)
;; Enable autoindent on viper
(setq-default viper-auto-indent 't)
;; Make Ret, o and O autoindent according to current major mode
(setq-default viper-electric-mode 't)
;; Make search ignore cases
(setq viper-case-fold-search 't)
;; Use vi-like magic regex expressions
(setq viper-case-fold-search 't)
(setq viper-re-search 't)
(setq viper-re-query-replace 't)
;; Enable buffer search
;;   Use gw, ge or gb to search for word under cursor
;;	 Use n or N after that
(viper-buffer-search-enable)
(setq viper-buffer-search-char ?g)
;; Match parenthesis
(setq blink-matching-paren 't)
;; Indentation settings
(setq-default viper-shift-width 'tab-width)
;; Don´t wrap searches
(setq viper-search-wrap-around 'nil)
;; Don´t move my cursor when searching on the same page
(setq viper-search-scroll-threshold 0)
;; Tags file name
(setq viper-tags-file-name "tags")
;; Bring back emacs C-h keybinding
;;(setq viper-want-ctl-h-help 'nil)
;; Vi keys in minibuffer
(setq viper-vi-style-in-minibuffer 'nil)
;; Use the Esc key as a meta in normal mode
;;(setq viper-no-multiple-ESC 'nil)
;; Function used by the command #c<move> to spell
(setq viper-spell-function 'ispell-region)
;; Move the cursor when repeting commands using the dot
;;(setq viper-keep-point-on-repeat 'nil)
;; Don't move point when undoing commands
(setq viper-keep-point-on-undo 't)
;; Make DEL work while replacing text
;;(setq viper-delete-backwards-in-replace 't)
;; Replace region delimiters
(setq viper-use-replace-region-delimiters 't)
(setq viper-replace-region-end-delimiter "$")
(setq viper-replace-region-start-delimiter "")
;; Allow multiple replace regions
(setq viper-allow-multiline-replace-regions 't)
;; Try these suffixes first when searching file names
(setq viper-smart-suffix-list '("" ".py" ".sh" "tex" "c" "cc" "el" "p"))
;; Viper insertion and deletion rings
(setq viper-insertion-ring-size 14)
(setq viper-command-ring-size 14)

;;; MACROS
;; Macros to repeat the second-last, nth-last destructive command
(setq viper-repeat-from-history-key 'f12)


;;;; Insert mode commands
;; Make C-n and C-N insert from insertion ring
(define-key viper-insert-global-user-map (kbd "C-p")
  'viper-insert-prev-from-insertion-ring)
(define-key viper-insert-global-user-map (kbd "C-n")
  'viper-insert-next-from-insertion-ring)
;; Make C-N and C-P insert from deletion ring
(define-key viper-vi-global-user-map (kbd "C-P")
  'viper-prev-destructive-command)
(define-key viper-vi-global-user-map (kbd "C-N")
  'viper-next-destructive-command)

;;;; Ex mode commands
;; Better buffer switching
(push '("b" (ido-switch-buffer)) ex-token-alist) 


;; Remains: M-x, C-x and C-x
;; Own: C-c / switch regex searching
;;		Use S-Tab or C-q Tab to insert a literal tab
;; Changes: C-\ is M-x
;;		Insert mode:
;;			* C-\ is M-x
;;      Normal mode:
;;          * Nvi style undo
;;          * 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Formating / Aligning  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use M-x align-regexp to vertically allign a region
;;  Ex: align-regexp =

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Buffers           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use C-x f to open a file
;; Use C-x C-b to change buffers, and C-x o to switch to other window
;; C-x b to look for another buffer, or quickly jump to next one
;; C-x C-<left> and C-<right> to switch between buffers quickly
;; C-x 3 to split vertically, and C-x 2 to split vertically
;; C-x 0 to close a window, C-x 1 to close every other window
;; C-x k / C-x 4 0 to kill a buffer/file

;; When using C-x C-b open next buffer in a dedicated window
(setq switch-to-buffer-in-dedicated-window t)
;; Use buffer-menu instead of list-buffers (see ibuffer)
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Several-Buffers.html
(global-set-key [remap list-buffers] 'buffer-menu)
;; Open buffers in a splitted window
(global-set-key (kbd "C-x C-B") 'buffer-menu-other-window)
(global-set-key (kbd "C-x B") 'switch-to-buffer-other-window)

;; Open a file in another window
(defun find-file-other-window-vertically nil
  "Edit a file in another window, split vertically."
  (interactive)
  (let ((split-width-threshold 0)
        (split-height-threshold nil))
    (call-interactively 'find-file-other-window)))
(global-set-key (kbd "C-x f") 'find-file-other-window)
(global-set-key (kbd "C-x F") 'set-fill-column)

;; Close and kill other window (for popups, help window, etc)
(defun close-and-kill-next-pane ()
      "If there are multiple windows, then close the other pane and kill the buffer in it also."
      (interactive)
      (other-window 1)
      (kill-this-buffer)
      (if (not (one-window-p))
          (delete-window)))
(global-set-key (kbd "C-x K") 'close-and-kill-next-pane)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Sessions          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stop creating # files
(setq create-lockfiles nil)
;; Disable autosave, dont create # files
(setq auto-save-default nil)

;; Restore cursor position
(cond
 ((< emacs-major-version 25)
  (require 'saveplace)
  (setq-default save-place t))
 ((>= emacs-major-version 25)
  (save-place-mode 1)))
;; Open the first file in the argument list at startup,
;; and hide every other file
(add-hook 'emacs-startup-hook '(lambda () (other-window 1) (delete-other-windows)))

;; Make periodic backups
;;(defvar --backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p "~/.emacs.d/backups"))
        (make-directory "~/.emacs.d/backups" t))
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
   '(("." . "~/.backups/"))    ; don't litter my fs tree
   delete-old-versions t
   vc-make-backup-files t	; make backups for git managed files
   kept-new-versions 10
   kept-old-versions 4
   auto-save-default t               ; auto-save every buffer that visits a file
   auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
   auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
   version-control t)       ; use versioned backups


;; Make special "per session" backups
(defun force-backup-of-buffer ()
  ;; Make a special "per session" backup at the first save of each
  ;; emacs session.
  (when (not buffer-backed-up)
    ;; Override the default parameters for per-session backups.
    (let ((backup-directory-alist '(("" . "~/.emacs.d/backups/per-session")))
          (kept-new-versions 3))
      (backup-buffer)))
  ;; Make a "per save" backup on each save.  The first save results in
  ;; both a per-session and a per-save backup, to keep the numbering
  ;; of per-save backups consistent.
  (let ((buffer-backed-up nil))
    (backup-buffer)))
(add-hook 'before-save-hook  'force-backup-of-buffer)

;; Disable backups for some files
(setq auto-mode-alist
      (append
       (list
        '("\\.\\(vcf\\|gpg\\)$" . sensitive-minor-mode)
        )
       auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Theme             ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      Modeline          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(setq-default mode-line-format nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    PATH SPECIFIC      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq image-dired-cmd-create-thumbnail-program "/usr/bin/convert")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      FUNCTIONS        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(byte-recompile-directory package-user-dir nil 'force)
