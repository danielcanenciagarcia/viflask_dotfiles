############################
#	      ALIASES		   #
############################

###### DEFAULT PROGRAMS #####
# Environment variables. These could go in .profile if you prefer
export VISUAL=vi
export EDITOR=$VISUAL
export PAGER="less --quit-if-one-screen"
export XDG_CONFIG_HOME="$HOME/.config"
export BROWSER=/usr/bin/firefox

# Mailbox location
#MAIL=/usr/spool/mail/$LOGNAME
#export MAIL

# Git related
#export GIT_EXTERNAL_DIFF='/usr/bin/diff -yt "$2" "$5"'

############################

######### Pagers ############
# Vim as a pager
# alias lessc="/usr/local/share/vim/vim90/macros/less.sh "$@""

alias man="man -P less"
alias most='most -w -r +u'
# +s => secure mode
alias more="LESS_IS_MORE=1 less -FE --tag-file=tags \
	-j.3 -P\"m--More-- (%Pb\\%) [%i of %m]\""
# Diff
alias diff="diff --color=always -t"
alias diffm="diff --color=always -t -U2 --minimal"
alias diffs="diff --color=always -yt --suppress-common-lines"
alias diffw="git diff --word-diff=color"

### Less configuration
#	- Specify a different tags file (just one)
#	  with the -T[tagsfile] option
#	- Jump to a specific tag using -t[tag]. Ex: -twhich
#	- Use -k to read a lesskey file
#	- + to execute a command after reading the file
#	  +4 will jump to the 4th line, +/for will search
#	  for the word for, etc
#	- Ng to go to the Nth line, Np go N% into the file,
#	  and NP to go to the line containing the Nth byte.
export LESS="-RNWJs --incsearch -igd -x4 -z28 -j.3 --window=5 \
	--status-col-width=1 --no-histdups \
	-Ps(file %i/%m) =>  %f (%lm)$ \
	-Ps(%Pb\%) %f\:%lm  | (file %i/%m)$ \
	-PM%f lines %lt-%lb/%L %Pb\% (%i/%m)$ \
	--tag-file=tags"
#export LESS="-RNWSs --incsearch -igd -x4 -z28"
# Exit if file not long enough (-F)
alias less="less --quit-if-one-screen --use-color -DNbk -DEWR -DPkw -DSyK -DWkw"
#alias less='less --quit-if-one-screen'

# Less as the Git pager
git config --get core.pager "less"
# Use global tags
#export LESSGLOBALTAGS="global"
export LESSQUIET=1

# Make the v command open the cur and next files
export LESSEDIT="%E ?lm+%lm. %g ?x%x."
# Make the v command respect initial arguments
alias aless='f(){ export ARGLIST="${@}"; LESSEDIT="%E \$ARGLIST" less ${@}; unset -f f; }; f'

#############################

## Aliases for editors
export vi="vim"
#export vi="/bin/bash avi"
alias ew="emacs -nw"
alias ed="sed -ruf ~/.edrc | /bin/ed -p '>> ' -v"
#alias ed="ed -p '>> ' -v"

### Utilities
# Trim whitespaces (usage: trim **/*.php)
alias trim='printf "set extended\n%%s/[[:space:]]+$//g\nwq\n" | ex -'
# Change tabs to spaces (usage: retab **/*.php)
alias retab='printf "%%!expand -t 2\nwq\n" | ex -'
# List files and directories recursively (like the tree command)
alias tree="ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'"

### Browsers
alias br="lynx-google"
alias brh="lynx -book"
alias pdf='f() { pdftotext "${@}" - | less; unset -f f; }; f'
alias mupdf="mupdf -I -C FFFAF0 -r 120"
# Use lynx as a fileviewer using the o[pen] command
#	- Lynx output the selected file to the correct program
#	  using the ~/.mailcap file
#	- See mailcap(1), mailcap_order(5), and update-mime(8)
#alias o="lynx file://localhost/~/"

# Aliases for various command shortcuts
alias dir='dir --color'
alias ls='ls --color -lFhv --author --time-style=long-iso'
alias ll='ls --color -alFhv --author --time-style=long-iso'
alias la='ls --color -AFhv --author --time-style=long-iso'
alias l='ls --color -CFhv --author --time-style=long-iso'
#alias ll='ls -lFb'
#alias la='ls -LaFb'

##############################
######	    NOTES       ######
##############################
# note => creates a file with todays date as the name (inside $NOTES)
# note [filename] => creates a file inside the $NOTES directory
# note {s,show} => show a list of taken notes (ls -l | more)
export NOTES='~/notes'

##############################
######	    SYSTEM      ######
##############################
alias night-light='redshift -P -O 1700'

##############################
######	     FZF        ######
##############################

# Can use vi $(fzf) to send the output of fzf to a program (vi in this case)
#
# Use the CLI find to get all files, excluding any filepath
# containing the string "git".
export FZF_DEFAULT_COMMAND='find . -type f ! -path "*git*"'
export FZF_DEFAULT_OPTS='-i --height=80% --min-height=8 --margin=5%,2%,2%,5%'
FZF_DEFAULT_COMMAND="find . type f"

## Fzf as a file explorer
#  Uses mailcap(4) to determine the correct program
#  to open a file
#
#	- Use <Tab> / <STab> to select and entry
#	- Use C-n and C-p to move between entries
#	- v to view them, e to it them
#	- ctrl-g to print its filename to stdout and ctrl-y to copy its filename
#
# Note: Use find . -type f | nl -s ", " | fzf --multi --nth=2 --delimiter=", "
#	to see numbers printed on the right margin. Use {+2} instead of {+},
#	and echo {2} instead of echo {}
#
#--bind='del:execute(rm -ri {+})' \
# --bind="enter:execute(if [ -d {} ]; then cd {}; else run-mailcap {+}; fi)+abort" \
# --bind="enter:execute(if [ ! -d {} ]; then $PAGER {+}; fi)+abort" \

alias ifzf='LS_TREE="ls -R \"$PARAM\" | grep \":$\" | sed -e \"s/:$//\" -e \"s/[^-][^\/]*\//--/g\" -e \"s/^/   /\" -e \"s/-/|/\""; \
	TREE="find \"$PARAM\" -maxdepth 1 -type d"; \
	#FZF_O_COMMAND="find -type f";
	$FZF_O_COMMAND | fzf --multi  \
		--layout=reverse-list --info=inline --prompt="> " \
		--header="CTRL-c or ESC to quit" --border=horizontal \
		--preview="echo \"File(s) with index(es) {+n} and query {q}\" && ls -l {-1}" \
		--preview-window=border,right,55% \
		--pointer="$" --no-color \
		--bind="enter:execute(if [ ! -d {} ]; then $EDITOR {+}; fi)+accept" \
		--bind="ctrl-e:execute($EDITOR {+})" \
		--bind="ctrl-x:execute(ex {+})" \
		--bind="ctrl-v:execute($PAGER -+F {+})" \
		--bind="ctrl-g:accept" \
		--bind="ctrl-y:change-preview(echo {})" \
		--bind="ctrl-y:+execute-silent(echo -n {} | xclip -i)+abort" \
		--bind="ctrl-alt-y:change-preview(cat {})" \
		--bind="ctrl-alt-y:+execute-silent(cat {} | xclip -i)+abort" \
		--bind="ctrl-d:reload(find -type d)" \
		--bind="ctrl-d:+change-preview(PARAM=.; eval $TREE)" \
		--bind="ctrl-d:+refresh-preview" \
		--bind="ctrl-f:reload(eval $FZF_O_COMMAND)" \
		--bind="ctrl-r:reload(eval $FZF_O_COMMAND)" \
		--bind="ctrl-s:reload(eval $FZF_O_COMMAND)" \
		--bind="ctrl-s:+change-preview(stat {})" \
		--bind="ctrl-s:+refresh-preview" \
		--bind="ctrl-alt-v:reload(eval $FZF_O_COMMAND)" \
		--bind="ctrl-alt-v:+change-preview(cat {})" \
		--bind="ctrl-alt-v:+refresh-preview" \
		--bind="ctrl-T:toggle-preview" \
		--bind="ctrl-t:change-preview(tree -C {} | head -200)" \
		--bind="ctrl-l:change-preview(echo {})" \
		--bind="ctrl-a:select-all" \
		--bind="ctrl-x:deselect-all"'
# Use o to open files and go to cd into directories
# 	- Can use cd $(ifzf) like you would do with fzf
alias o='f() { FZF_O_COMMAND="find -type f"; ifzf; unset -f f; }; f'
alias go='f() { FZF_O_COMMAND="find $@ -type d"; ifzf; unset -f f; }; f'

##################
#alias o='f() { \
#	source ~/.kshrc; \
#	selection=$(ifzf) \
#	#selection=$(fzf -m); \
#	if [ -d "$selection"/. ]; then \
#		echo $selection;
#		cd $selection; \
#	else \
#		echo $selection; \
#	fi; }; f;'
##################

##############################
