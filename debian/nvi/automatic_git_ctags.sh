#!/bin/sh

# Initialize git template and hooks folders
git config --global init.templatedir '~/.git_template'
mkdir -p ~/.git_template/hooks

# Base script (Ctags execution)
rm ~/.git_template/hooks/ctags
cat >> ~/.git_template/hooks/ctags << END
#!/bin/sh
set -e
PATH="/usr/local/bin:$PATH"
trap 'rm -f "$$.tags"' EXIT
git ls-files | \
  ctags --tag-relative -L - -f"$$.tags" --languages=-javascript,sql
  mv "$$.tags" "tags"
END

# Create an alias for this script
git config --global alias.ctags '!.git/hooks/ctags'

# First three hooks: 
rm ~/.git_template/hooks/post-commit
rm ~/.git_template/hooks/post-merge
rm ~/.git_template/hooks/post-checkout
rm ~/.git_template/hooks/post-rewrite

cat >> ~/.git_template/hooks/post-commit << END
#!/bin/sh
.git/hooks/ctags >/dev/null 2>&1 &
END
cp ~/.git_template/hooks/post-commit ~/.git_template/hooks/post-merge
cp ~/.git_template/hooks/post-commit ~/.git_template/hooks/post-checkout

# Fourth hook
cat >> ~/.git_template/hooks/post-rewrite << END
#!/bin/sh
case "$1" in
  rebase) exec .git/hooks/post-merge ;;
esac
END

# Once all this hooks are created, executing git init
# will copy this hooks to the repository

# Remember to add the .git/TAGS relative path to your editor
# config, probably .vimrc/.exrc or .nexrc if you use a vim-based
# editor
