(defcfg
  ;; ** For Linux **
  ;;input  (device-file "/dev/input/by-id/usb-Dell_Dell_USB_Keyboard-event-kbd")
  input  (device-file "/dev/input/by-path/platform-i8042-serio-0-event-kbd")
  output (uinput-sink "KMonad seniply-iso-wide")
  cmp-seq cmp

  ;; ** For Windows **
  ;; input  (low-level-hook)
  ;; output (send-event-sink)

  ;; ** For MacOS **
  ;; input  (iokit-name "my-keyboard-product-string")
  ;; output (kext)

  fallthrough true
)

(defsrc
  esc      f1    f2    f3    f4    f5    f6    f7    f8    f9    f10   f11   f12   prnt   ins   del
  grv      1     2     3     4     5     6     7     8     9     0     -     =     bspc
  tab      q     w     e     r     t     y     u     i     o     p     [     ]
  caps     a     s     d     f     g     h     j     k     l     ;     '     \     ret
  lsft  nubs  z     x     c     v     b     n     m     ,     .     /     rsft
  lctl        lmet  lalt              spc               ralt  rctl        up
								     left down right
)

(defalias 
  l_dh (layer-switch colemak-dh-sp)
)

(defalias
  ext  (layer-toggle extend) ;; Bind 'ext' to the Extend Layer
  fun  (layer-toggle function) ;; Bind 'fun' to the Function Layer
  sym (layer-toggle symbols) ;; Bind 'sym' to Symbols Layer
  num (layer-toggle numbers) ;; Bind 'num' to Numbers Layer
  l_sym (layer-toggle lng-symbols)
)

;;(defalias 
  ;;ls_s (sticky-key 1500 lsft)
  ;;lc_s (sticky-key 1500 lctl)
  ;;la_s (sticky-key 1500 lalt)
  ;;ra_s (sticky-key 1500 ralt)
  ;;lm_s (sticky-key 1500 lmet)
  ;;lsc_s (around @lc_s @ls_s)
  ;;lac_s (around @lc_s @la_s)
;;)

(defalias
  cpy C-c
  pst C-v
  cut C-x
  scpy C-S-c
  spst C-S-v
  scut C-S-x
  udo C-z
  all C-a
  fnd C-f
  bk A-left
  fw A-right
  v+ VolumeUp
  v- VolumeDown 
  v0 Mute
  mpl play
  mps previoussong
  mns nextsong
  mrw rewind
  mpp pause
  mst stop
  b+ BrightnessUp
  b- BrightnessDown
  ra1 (around ralt 1)
  ra2 (around ralt 2)
  ra3 (around ralt 3)
  ra4 (around ralt 4)
  ra5 (around ralt 5)
  ra6 (around ralt 6)
  ra7 (around ralt 7)
  ra8 (around ralt 8)
  ra9 (around ralt 9)
  ra0 (around ralt 0)
)

(defalias
  / (tap-hold 90 / #(~ XX))
  ;; # (tap-hold 90 # #((tap-macro ! !) XX))
)

(deflayer colemak-dh-sp
  esc      f1    f2    f3    f4    f5    f6    f7    f8    f9    f10   f11   f12   prnt   ins   del
  grv      1     2     3     4     5     =     6     7     8     9     0     -     bspc
  tab      q     w     f     p     b     [     j     l     u     y     '     \
  lctl     a     r     s     t     g     ]     m     n     e     i     o     ;     ret
  lshift  z     x     c     d     v     spc   /    k      h     ,     .     rsft
  caps       lalt  lmet              @sym               rsft  rctl        up
								     left down right
)

(deflayer symbols
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        @     ^     &     $     %     \(    !    \_     +     *     _     _
  _        `     '     "     -    ~     \)     esc   :     ;     ret   _     _     _
  _       _      _     caps    @num    tab   |    \     bspc   del   _     _     _
  _           @l_sym  lalt            @num              ralt   _          _
                                                                       _  _  _
)

(deflayer lng-symbols
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        _     _     _     _     _     _     _     £     €     ú     _     _
  _        á     _     _     _     _     _     _     ñ     é     í     ó     _     _ 
  _    _      _     Ç     _     _     _    ¿     _     _     _     _     _
  _           _     _              @l_dh                 _  _     _
                                                               _  _  _
)

;; !! Add media keys
(deflayer extend
  _        @mpl  @mrw  @mps  @mns ejectcd refresh brdn brup www mail   prog1 prog2
  _        f1    f2    f3    f4    f5    f6    f7    f8    f9   f10    f11   f12   _   _   _
  _        esc   @bk   @fnd  @fw   ins   ssrq  pgup  home  up    end   caps  cmp
  _        _     _     _     _     _     slck  pgdn  lft   down  rght  del   menu  _
  _     @udo  @cut  @cpy  lmet  @pst  @udo  _     ret   bks   tab    cmp  ssrq
  _            _    _                 @num               ret   @fun  _     up
                                                                      left down right 
)

(deflayer function
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        @mst  @mps  @mpp  @mns  @b-   _     f12   f7    f8    f9    slck  _
  _        _     _     _     _     @b+   _     f11   f4    f5    f6    @l_dh _     _
  _     @v0   @v-   @scpy @v+   @spst _     _     f10   f1    f2    f3    @l_sym
  _           _     _                 _                 _     _        _
                                                                    _  _  _
)

(deflayer orsymbols
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        S-1   S-2   S-3   S-4   S-5   «     =     `     :     ;     +     _
  _        _     _     _     _     _   »     *     \(    {     [     -     _     _
  _     nubs  S-nubs \\   S-\\  S-7   nubs  _     ¬     \)    }     ]     \_
  _           _     @fun              @num              _     _        _
                                                                    _  _  _
)

(deflayer numbers
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        _     _     _     _     nlck  =     7     8     9     +     _     _
  _        _     _     _     _     _     *     4     5     6     -     _     _     _
  _     _     cmp   tab   bks   ret   _     0     1     2     3     /     _
  _           _     _                 _                 _     _        _
                                                                    _  _  _
)

(deflayer empty
  _        _     _     _     _     _     _     _     _     _     _     _     _     _   _   _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _
  _        _     _     _     _     _     _     _     _     _     _     _     _
  _        _     _     _     _     _     _     _     _     _     _     _     _     _ 
  _    _      _     _     _     _     _     _     _     _     _     _     _
  _          _    _                   _                 _     _        _
                                                                    _  _  _
)
