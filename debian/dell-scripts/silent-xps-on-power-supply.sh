#!/usr/bin/env/bash
# Usage:
#   - Make the script executable
#   - Append this line in your personal crontab (crontab -e)
#	* * * * * /path/to/myscript.sh
#	* * * * * $HOME/ports/viflask_dotfiles/debian/dell-scripts/disable-bios-fan-control-on-power-supply.sh
#   - You should use sudo crontab -e


# Execute this script using bash even if the user used sh or dash
if [ -z "$BASH_VERSION" ]
then
    exec bash "$0" "$@"
fi

# Run dell-bios-fan-control 0 at system boot
i=0

while [ $i -lt 5 ]; do # 5 five-second intervals in 1 minute => lt 12 and sleep 5
	#cat /sys/class/power_supply/BAT0/status | grep "Charging" 2>&1 /dev/null
	#rc=$?
	#echo ${rc}

	rc=$(cat /sys/class/power_supply/BAT0/status | sed -n '1p');
	echo $rc;

	#if [[ ${rc} -eq 0 ]];
	if [[ $rc =~ "Charging" ]];
	then
		echo "Disable Bios fan control"
		#/usr/bin/silent_xps -config /etc/silent_xps.json
		dell-bios-fan-control 0 
		i8kfan 0 0
		systemctl start i8kmon
		#i8kfan 1 1
	else
		echo "Enable Bios fan control"
		#pkill silent_xps
		dell-bios-fan-control 1 
		systemctl stop i8kmon
	fi

	sleep 12
	i=$(( i + 1 ))
done
