#!/bin/bash
# Usage:
#   - Make the script executable
#   - Append this line in your personal crontab (crontab -e)
#	* * * * * /path/to/myscript.sh
#	* * * * * $HOME/ports/viflask_dotfiles/debian/dell-scripts/disable-bios-fan-control-on-power-supply.sh
#   - You should use sudo crontab -e

# Run dell-bios-fan-control 0 at system boot
i=0

while [ $i -lt 60 ]; do # 5 five-second intervals in 1 minute => lt 12 and sleep 5
	cat /sys/class/power_supply/BAT0/status | grep "Charging" 2>&1 /dev/null;
	rc=$?;
	echo ${rc}

	if [ ${rc} -eq 1 ]
	then
		echo "Enable Bios fan control"
		dell-bios-fan-control 1
	else
		echo "Disable Bios fan control"
		dell-bios-fan-control 0
	fi

	sleep 1
	i=$(( i + 1 ))
done
