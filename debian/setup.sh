# !/bin/sh

export HOMEDIR="/home/daniel"
export NUSER="daniel"

# Install the necessary pkgs
#	- Debian based
apt install cwm lynx alacritty tmux ksh doas -y
#	- Void Linux
#xbps-install cwm lynx alacritty tmux ksh opendoas -y

# Shells
# Add ksh to /etc/shells
echo "/usr/bin/ksh" >> /etc/shells
# Change default shell to ksh
su -c "chsh -s /usr/bin/ksh" $NUSER
# Create doas.conf
echo "permit keepenv daniel as root" > /etc/doas.conf
# Copy ksh config files
ln -fs $HOMEDIR/ports/viflask_dotfiles/debian/.kshrc $HOMEDIR/.kshrc

# Scripts

# Config files
ln -fs $HOMEDIR/ports/viflask_dotfiles/openbsd/.cwmrc $HOMEDIR/.cwmrc
ln -fs $HOMEDIR/ports/viflask_dotfiles/.config/alacritty $HOMEDIR/.config/
ln -fs $HOMEDIR/ports/viflask_dotfiles/shells/tmux $HOMEDIR/.config/
mkdir -p $HOMEDIR/.config/xmobar
ln -fs $HOMEDIR/ports/viflask_dotfiles/openbsd/.xmobarrc $HOMEDIR/.config/xmobar/.xmobarrc

# Init files
# ln -fs $HOMEDIR/ports/viflask_dotfiles/startup.sh $HOMEDIR/startup.sh

# Traditional vi-like .exrc
ln -fs $HOMEDIR/ports/viflask_dotfiles/openbsd/.exrc $HOMEDIR/.exrc
ln -fs $HOMEDIR/ports/viflask_dotfiles/editors/ovi/.sexrc $HOMEDIR/.sexrc

# Tags
echo "tags" >> ~/.gitignore_global

# Lynx Browser
cp $HOMEDIR/ports/viflask_dotfiles/browsers/lynx/.lynxrc $HOMEDIR
cp $HOMEDIR/ports/viflask_dotfiles/browsers/lynx/lynx /usr/local/bin/
cp $HOMEDIR/ports/viflask_dotfiles/browsers/lynx/lynx-google /usr/local/bin/

# Git config
# cp $HOMEDIR/.config/.git
# ln -fs $HOMEDIR/ports/viflask_dotfiles/gits/.ssh/config $HOMEDIR/.config/.git/config
