#!/bin/sh

# Use less to view markdown files
#   - Integrate it with less adding the lines 7-11 to
#     /usr/bin/lesspipe

case "$1" in
    *.md)
        #pandoc -s -f markdown -t man "$1"|groff -T utf8 -man -
        pandoc -s -f gfm -t man "$1"|groff -t -T utf8 -man -
	# Add -k utf8 to groff to fix formatting issues
        ;;
    *)
        # We don't handle this format.
        exit 1
esac

# No further processing by lesspipe necessary
exit 0
