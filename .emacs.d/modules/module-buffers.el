;; SEE HTTPS://SMYTHP.COM/EMACS_BUFFERS/

;; IBUFFER
;;(global-set-key (kbd "C-x C-b") 'ibuffer)
;;(global-set-key (kbd "C-x b") 'switch-to-buffer)

;; IFLIPB
(use-package iflipb)
(global-set-key (kbd "C-c i") 'iflipb-next-buffer)
(global-set-key (kbd "C-c u") 'iflipb-previous-buffer)


;; Projectile
;;(use-package projectile
;;  :ensure t
;;  :config
;;    (when (file-directory-p "~/myports")
;;      (setq projectile-project-search-path '("~/myports")))
;;
;;    (setq projectile-switch-project-action #'projectile-dired)
;;    (projectile-mode +1))
;; Recommended keymap prefix on Windows/Linux
;;(define-key projectile-mode-map (kbd "M-p") 'projectile-command-map)



;; Eyebrows
(use-package eyebrowse
    ;;    :ensure t
    :diminish eyebrowse-mode
    :config
    (setq eyebrowse-new-workspace t)
    (progn
        (define-key eyebrowse-mode-map (kbd "C-c w 1") 'eyebrowse-switch-to-window-config-1)
        (define-key eyebrowse-mode-map (kbd "C-c w 2") 'eyebrowse-switch-to-window-config-2)
        (define-key eyebrowse-mode-map (kbd "C-c w 3") 'eyebrowse-switch-to-window-config-3)
        (define-key eyebrowse-mode-map (kbd "C-c w 4") 'eyebrowse-switch-to-window-config-4)

        (define-key eyebrowse-mode-map (kbd "C-c w o") 'eyebrowse-prev-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w p") 'eyebrowse-next-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w l") 'eyebrowse-last-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w q") 'eyebrowse-close-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w r") 'eyebrowse-rename-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w c") 'eyebrowse-create-window-config)
        (define-key eyebrowse-mode-map (kbd "C-c w C-c") 'eyebrowse-create-window-config)

        (eyebrowse-mode t)
        ;; Eyebrowse
        (setq eyebrowse-new-workspace t)
        (setq eyebrowse-mode-line-style 'current)))


;; Workgroups2
(use-package workgroups2)
;; Change prefix key (before activating WG)
(setq wg-prefix-key "M-w")
(setq wg-session-file "~/.emacs_workgroups")
(workgroups-mode 1)
;;; Keys:
;;   - prefix C-c: wg-create-workgroup
;;   - prefix C-v: wg-open-workgroup
;;   - prefix C-z: wg-kill-workgroup


;; Desktop.el
;; (require 'desktop)

;;; Automatically save and restore sessions
;; (setq desktop-dirname             "~/.emacs.d/sessions/"
;;       desktop-base-file-name      "emacs.desktop"
;;       desktop-base-lock-name      "lock"
;;       desktop-path                (list desktop-dirname)
;;       desktop-save                t
;;       desktop-files-not-to-save   "^$" ;reload tramp paths
;;       desktop-load-locked-desktop nil
;;       desktop-restore-eager       t
;;       desktop-auto-save-timeout   30)
;; (desktop-save-mode 0)


;;; Only when you want to open last session on startup
;;;(require 'nodesktopsaveorsessionwarning)
;;; Dont ask for desktop write ;; It doesnt save the session
;; (setq desktop-file-modtime (nth 5 (file-attributes (desktop-full-file-name))))

;;; Session management (not automatic)
;; (defvar my-desktop-session-dir
;;   (concat (getenv "HOME") "/.emacs.d/sessions/")
;;   "*Directory to save desktop sessions in")

;; (defvar my-desktop-session-name-hist nil
;;   "Desktop session name history")

;; (defun my-desktop-save (&optional name)
;;   "Save desktop with a name."
;;   (interactive)
;;   (unless name
;;     (setq name (my-desktop-get-session-name "Save session as: ")))
;;   (make-directory (concat my-desktop-session-dir name) t)
;;   (desktop-save (concat my-desktop-session-dir name) t))

;; (defun my-desktop-read (&optional name)
;;   "Read desktop with a name."
;;   (interactive)
;;   (unless name
;;     (setq name (my-desktop-get-session-name "Load session: ")))
;;   (desktop-read (concat my-desktop-session-dir name)))

;; (defun my-desktop-get-session-name (prompt)
;;   (completing-read prompt (and (file-exists-p my-desktop-session-dir)
;;                                (directory-files my-desktop-session-dir))
;;                    nil nil nil my-desktop-session-name-hist))


;;; Open Default / Last session (automatic save)
;; (defun my-desktop ()
;;   "Load the desktop and enable autosaving"
;;   (interactive)
;;   (let ((desktop-load-locked-desktop "ask"))
;;     (desktop-read)
;;     (desktop-save-mode 1)))



;; Manage buffers-mode in local project / global
;; Use if using eyebrowser
;; With perspective is kind of useless
;;Enable frame-bufs-mode by default
;;(use-package frame-bufs
;;    :ensure t
;;    :config
;;    (setq frame-bufs-mode t))
;; F for toogle in buffers list
;; o indicates selected frame buffer in global mode

;; frame-bufs= frame-purpose + frame-workflow


;;Buffer move (buffer-move pkg)
(use-package buffer-move
    :load-path "~/.emacs.d/packages/buffer-move.el"
    :ensure t)

;;; (setq buffer-move-behavior 'move) ;; Dont swap just move. Can C-x b RET to get other buffer

(global-set-key (kbd "M-S-<up>") 'buf-move-up)
(global-set-key (kbd "M-S-<down>") 'buf-move-down)
(global-set-key (kbd "M-S-<left>") 'buf-move-left)
(global-set-key (kbd "M-S-<right>") 'buf-move-right)

(global-set-key (kbd "M-K") 'buf-move-up)
(global-set-key (kbd "M-J") 'buf-move-down)
(global-set-key (kbd "M-H") 'buf-move-left)
(global-set-key (kbd "M-L") 'buf-move-right)


;; Worksgroups2
;;(use-package workgroups2
;;  :ensure t
;:;  :custom
  ;; Change prefix key (before activating WG)
;;  (wg-prefix-key "C-c w")
;;  (wg-delete-workgroup "C-k")
;;  (wg-session-file "~/.emacs.d/.emacs_workgroups")
;;  :config
;;  (workgroups-mode)
;;)

;;(setq wg-prefix-key "C-c w")
;;(worksgroup-mode 1)


(provide 'module-buffers)
