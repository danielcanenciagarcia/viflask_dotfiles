;; Active auto-completion with company-mode
(use-package company
    :ensure t)
(use-package company-quickhelp
    :ensure t)

(global-company-mode)
(company-tng-mode)
(company-complete)
(company-quickhelp-mode)

;; Behave like vim <TAB> to select
(add-hook 'after-init-hook 'company-tng-mode)
(company-tng-configure-default)

                                        ; See: https://github.com/company-mode/company-mode/wiki/Third-Party-Packages
;;;;;;;;;;;;;;;;;;;;;;
;;;;;; Backends ;;;;;;
;;;;;;;;;;;;;;;;;;;;;;

;;; Configure per major mode.
(defun yet-org-mode-company ()
    (setq-local company-backends '(company-dabbrev company-files)
        company-frontends
        '(company-pseudo-tooltip-unless-just-one-frontend-with-delay
          company-preview-frontend))
    (company-mode 1))
(add-hook 'org-mode-hook 'yet-org-mode-company)

(defun yet-c-mode-company ()
    (setq-local
        company-backends
        '((company-clang company-keywords company-dabbrev-code)
             company-dabbrev
             company-files)))
(add-hook 'c-mode-common-hook 'yet-c-mode-company)


(defun yet-php-mode-company ()
    (setq-local company-backends
        '((company-dabbrev-code company-keywords company-capf))))
(add-hook 'php-mode-hook 'yet-php-mode-company -50)


(defun yet-texinfo-mode-company ()
    (setq-local company-backends '(company-dabbrev))
    (company-mode 1))
(add-hook 'texinfo-mode-hook 'yet-texinfo-mode-company)

;; Keys
;; Switch to the next backend.
;(global-set-key (kbd "C-c c /") #'company-other-backend)
;; Switch to the company-files backend.
;(global-set-key (kbd "C-c c f") #'company-files)
;; Switch to the company-dabbrev backend.
;(global-set-key (kbd "C-c c d") #'company-dabbrev)
;; Switch to the company-capf backend.
;(global-set-key (kbd "C-c c c") #'company-capf)
;; A more mnemonic binding (same as for xref-find-definitions)
;; to pop-up buffer with the candidate's definition.
;(define-key company-active-map (kbd "M-.") #'company-show-location)


;;;;;;;;;;;;;;;;;;;;;;
;;;; Third Party ;;;;;
;;;;;;;;;;;;;;;;;;;;;;
;; Haskell
(use-package company-ghci
    :ensure t)
(push 'company-ghci company-backends)
(add-hook 'haskell-mode-hook 'company-mode)
;;; To get completions in the REPL
(add-hook 'haskell-interactive-mode-hook 'company-mode)


;; Company Prescient.el
(use-package company-prescient
    :ensure t
    :after company
    :config
    (company-prescient-mode +1))
;; Some fixes for Orderless when using more than one component
                                        ;(setq orderless-component-separator "[ &]")
                                        ;(defun just-one-face (fn &rest args)
                                        ;  (let ((orderless-match-faces [completions-common-part]))
                                        ;    (apply fn args)))
                                        ;(advice-add 'company-capf--candidates :around #'just-one-face)


;; Ctags company mode
;;(require 'company-etags)
;; Gtags
;;(require 'company-gtags)

(add-hook 'after-init-hook 'global-company-mode)
;; Use gtags ~/myproject or ctags ~/myproject for universal ctags

;; (company-active-map / company-search-map)
(customize-set-variable 'company-quick-access-modifier 'meta) ;; alt
(customize-set-variable 'company-show-quick-access t)
;; String to use company-quick-access-keys (0-9 by default)
(customize-set-variable 'company-quick-access-keys '("a" "b" "c" "d" "f" "e" "r" "q" "w" "g" "t"))

;; Enbale semantic mode: See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Semantic.html
(semantic-mode)
(setq
    ;; Quick-access
    company-show-quick-access 'left
    ;; Annotations
    company-tooltip-align-annotations t
    ;; Max number candidates
    company-tooltip-limit 15
    ;; Lines instead of scrollbar.
    company-tooltip-offset-display 'lines
    ;; Guarantee some candidates at screen output
    company-tooltip-minimum 4
    ;; Make them order correctly then
    company-tooltip-flip-when-above t
    ;; Right Margin
    company-tooltip-margin 2
    ;; Always displays a tooltip, regardless of completion
    ;; candidates
1    company-pseudo-tooltip-frontend t
    company-idle-delay 0
    company-tooltip-idle-delay 0
    ;;company-tooltip-idle-delay 100   ; Raise tooltip manually with TAB.
    company-require-match nil
    company-files-exclusions '(".git/" ".DS_Store")
    company-files-chop-trailing-slash nil
    company-tooltip-align-annotations t
    company-dabbrev-ignore-case nil
    company-dabbrev-downcase nil
    ;; company-dabbrev-ignore-case 'keep-prefix ; enj (Enjoy) -> enjoy
    company-dabbrev-other-buffers t
    company-dabbrev-minimum-length 2
    company-dabbrev-code-everywhere t

    company-frontends
    '(company-pseudo-tooltip-unless-just-one-frontend-with-delay
         company-preview-frontend
         company-echo-metadata-frontend)
    company-backends
    '((company-dabbrev company-keywords company-capf))
    company-transformers '(delete-consecutive-dups
                              company-sort-by-occurrence
                              company-sort-prefer-same-case-prefix)
    )

;; Quick-access
;; Candidates Search -> C-s
;; Candidates Filter (just show matching ones) -> C-M-s


;; Fix some things: See: https://github.com/oantolin/orderless

;; Set color. See: M-x list-colors-display
;; See: https://github.com/nsf/gocode/tree/master/emacs-company
;;	https://github.com/company-mode/company-mode/issues/1088
;;
;;	https://github.com/Doerthous/.emacs.d/blob/6804b782548bef12518065b703abe854c1adc2ce/dts-theme.el
;;	https://github.com/railwaycat/emacs-config/blob/37c5278d558653b289d7c0aa511ae2c33a97168d/themes/basic-theme.el

(defvar company-new-fg "navy")
(defvar company-new-bg "navajo white") ; v1: gray30
(defvar company-new-selection-bg "dark goldenrod") ;; v1: gray24
(defvar company-new-common-fg "dark red") ;; v1: light blue
(defvar company-new-annotation-fg "light blue") ;; v1: "light blue"
(defvar company-new-scrollbar-fg "LightGoldenrod2") ;; v1: gray26
(defvar company-new-scrollbar-bg "lemon chiffon") ;; v1: cornsilk
(defvar company-new-preview-fg (face-attribute 'default :foreground))
(defvar company-new-preview-bg "SpringGreen2")

(custom-set-faces
    `(company-tooltip ((t (:background ,company-new-bg :foreground ,company-new-fg))))
    `(company-tooltip-selection ((t (:background ,company-new-selection-bg))))
    `(company-tooltip-common ((t (:foreground ,company-new-common-fg))))
    `(company-tooltip-annotation ((t (:foreground ,company-new-annotation-fg))))
    `(company-scrollbar-fg ((t (:background ,company-new-scrollbar-fg))))
    `(company-scrollbar-bg ((t (:background ,company-new-scrollbar-bg))))
    `(company-preview ((t (:foreground ,company-new-preview-fg :background ,company-new-preview-bg))))
    `(company-preview-common ((t (:foreground ,company-new-common-fg :background ,company-new-preview-bg))))
    `(company-preview-search ((t (:foreground ,company-new-common-fg :background ,company-new-preview-bg))))
    `(company-tooltip-quick-access ((t (:background ,company-new-bg :foreground ,company-new-common-fg))))
    `(company-tooltip-quick-access-selection ((t (:background ,company-new-selection-bg :foreground ,company-new-scrollbar-bg :slant italic))))
    ;;`(company-echo-common ((,class :foreground ,magenta-alt-other)))
    ;;`(company-template-field ((,class :inherit modus-themes-intense-magenta)))
    ;;`(company-tooltip-annotation-selection ((,class :inherit bold :foreground ,fg-main)))
    ;;`(company-tooltip-common-selection ((,class :foreground ,fg-main)))
    ;;'(company-tooltip-mouse ((,class :inherit modus-themes-intense-blue)))
    ;;`(company-tooltip-search ((,class :inherit (modus-themes-search-success-lazy bold))))
    ;;`(company-tooltip-search-selection ((,class :inherit (modus-themes-search-success bold) :underline t)))
    ;;`(company-tooltip-selection ((,class :inherit (modus-themes-subtle-cyan bold))))
    )

;; Icons style (left margin)
(customize-set-variable 'company-format-margin-function #'company-text-icons-margin)
;; Options:     company-detect-icons-margin)
;;      company-text-icons-margin
;;      company-dot-icons-margin
;;      company-vscode-dark-icons-margin
;;      company-vscode-light-icons-margin

;; Left margin
;;(defvar company-text-icons-format "%s ")
(setq company-text-icons-format " %s ") ;; Doesn't work, set directly

;; Icons on the left
(setq company-text-icons-mapping
    '((array "a" font-lock-type-face)
         (boolean "b" font-lock-builtin-face)
         (class "c" font-lock-type-face)
         (color "#" success)
         (constant "c" font-lock-constant-face)
         (enum-member "e" font-lock-builtin-face)
         (enum "e" font-lock-builtin-face)
         (field "f" font-lock-variable-name-face)
         (file "f" font-lock-string-face)
         (folder "d" font-lock-doc-face)
         (interface "i" font-lock-type-face)
         (keyword "k" font-lock-keyword-face)
         (method "m" font-lock-function-name-face)
         (function "f" font-lock-function-name-face)
         (module "{" font-lock-type-face)
         (numeric "n" font-lock-builtin-face)
         (operator "o" font-lock-comment-delimiter-face)
         (parameter "p" font-lock-builtin-face)
         (property "p" font-lock-variable-name-face)
         (ruler "r" shadow)
         (snippet "S" font-lock-string-face)
         (string "s" font-lock-string-face)
         (struct "%" font-lock-variable-name-face)
         (text "w" shadow)
         (value "v" font-lock-builtin-face)
         ;;(value "+" "#FFFFFF" "#FFFFFF")
         (variable "v" font-lock-variable-name-face)
         ;;(variable "+" "#FFFFFF" "#FFFFFF")
         (t "." shadow)))
"Mapping of the text icons.
The format should be an alist of (KIND . CONF) where CONF is a list of the
form (ICON FG BG) which is used to propertize the icon to be shown for a
candidate of kind KIND. FG can either be color string or a face from which
we can get a color string (using the :foreground face-property). BG must be
of the same form as FG or a cons cell of (BG . BG-WHEN-SELECTED) which each
should be of the same form as FG.
The only mandatory element in CONF is ICON, you can omit both the FG and BG
fields without issue.
When BG is omitted and `company-text-icons-add-background' is non-nil, a BG
color will be generated using a gradient between the active tooltip color and
the FG color."
;;  :type 'list)

(setq company-text-icons-format " %s ")
;; Generate a bg color for icons
(setq company-text-icons-add-background 1)
;; Active auto-completion with company-mode
(use-package company
    :ensure t)
(use-package company-quickhelp
    :ensure t)

(global-company-mode)
(company-tng-mode)
(company-complete)
(company-quickhelp-mode)

;; Behave like vim <TAB> to select
(add-hook 'after-init-hook 'company-tng-mode)
(company-tng-configure-default)

                                        ; See: https://github.com/company-mode/company-mode/wiki/Third-Party-Packages
;;;;;;;;;;;;;;;;;;;;;;
;;;;;; Backends ;;;;;;
;;;;;;;;;;;;;;;;;;;;;;

;;; Configure per major mode.
(defun yet-org-mode-company ()
    (setq-local company-backends '(company-dabbrev company-files)
        company-frontends
        '(company-pseudo-tooltip-unless-just-one-frontend-with-delay
             company-preview-frontend))
    (company-mode 1))
(add-hook 'org-mode-hook 'yet-org-mode-company)

(defun yet-c-mode-company ()
    (setq-local
        company-backends
        '((company-clang company-keywords company-dabbrev-code)
             company-dabbrev
             company-files)))
(add-hook 'c-mode-common-hook 'yet-c-mode-company)


(defun yet-php-mode-company ()
    (setq-local company-backends
        '((company-dabbrev-code company-keywords company-capf))))
(add-hook 'php-mode-hook 'yet-php-mode-company -50)


(defun yet-texinfo-mode-company ()
    (setq-local company-backends '(company-dabbrev))
    (company-mode 1))
(add-hook 'texinfo-mode-hook 'yet-texinfo-mode-company)

;; Keys
;; Switch to the next backend.
(global-set-key (kbd "C-c c /") #'company-other-backend)
;; Switch to the company-files backend.
(global-set-key (kbd "C-c c f") #'company-files)
;; Switch to the company-dabbrev backend.
(global-set-key (kbd "C-c c d") #'company-dabbrev)
;; Switch to the company-capf backend.
(global-set-key (kbd "C-c c c") #'company-capf)
;; A more mnemonic binding (same as for xref-find-definitions)
;; to pop-up buffer with the candidate's definition.
(define-key company-active-map (kbd "M-.") #'company-show-location)


;;;;;;;;;;;;;;;;;;;;;;
;;;; Third Party ;;;;;
;;;;;;;;;;;;;;;;;;;;;;
;; Haskell
(use-package company-ghci
    :ensure t)
(push 'company-ghci company-backends)
(add-hook 'haskell-mode-hook 'company-mode)
;;; To get completions in the REPL
(add-hook 'haskell-interactive-mode-hook 'company-mode)


;; Company Prescient.el
(use-package company-prescient
    :ensure t
    :after company
    :config
    (company-prescient-mode +1))
;; Some fixes for Orderless when using more than one component
                                        ;(setq orderless-component-separator "[ &]")
                                        ;(defun just-one-face (fn &rest args)
                                        ;  (let ((orderless-match-faces [completions-common-part]))
                                        ;    (apply fn args)))
                                        ;(advice-add 'company-capf--candidates :around #'just-one-face)


;; Ctags company mode
;;(require 'company-etags)
;; Gtags
;;(require 'company-gtags)

(add-hook 'after-init-hook 'global-company-mode)
;; Use gtags ~/myproject or ctags ~/myproject for universal ctags

;; (company-active-map / company-search-map)
(customize-set-variable 'company-quick-access-modifier 'meta) ;; alt
(customize-set-variable 'company-show-quick-access t)
;; String to use company-quick-access-keys (0-9 by default)
(customize-set-variable 'company-quick-access-keys '("a" "b" "c" "d" "f" "e" "r" "q" "w" "g" "t"))

;; Enbale semantic mode: See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Semantic.html
(semantic-mode)
(setq
    ;; Quick-access
    company-show-quick-access 'left
    ;; Annotations
    company-tooltip-align-annotations t
    ;; Max number candidates
    company-tooltip-limit 15
    ;; Lines instead of scrollbar.
    company-tooltip-offset-display 'lines
    ;; Guarantee some candidates at screen output
    company-tooltip-minimum 4
    ;; Make them order correctly then
    company-tooltip-flip-when-above t
    ;; Right Margin
    company-tooltip-margin 2
    ;; Always displays a tooltip, regardless of completion
    ;; candidates
    company-pseudo-tooltip-frontend t
    company-idle-delay 0
    company-tooltip-idle-delay 0
    ;;company-tooltip-idle-delay 100   ; Raise tooltip manually with TAB.
    company-require-match nil
    company-files-exclusions '(".git/" ".DS_Store")
    company-files-chop-trailing-slash nil
    company-tooltip-align-annotations t
    company-dabbrev-ignore-case nil
    company-dabbrev-downcase nil
    ;; company-dabbrev-ignore-case 'keep-prefix ; enj (Enjoy) -> enjoy
    company-dabbrev-other-buffers t
    company-dabbrev-minimum-length 2
    company-dabbrev-code-everywhere t

    company-frontends
    '(company-pseudo-tooltip-unless-just-one-frontend-with-delay
         company-preview-if-just-one-frontend
         company-preview-frontend
         company-echo-metadata-frontend)
    company-backends
    '((company-dabbrev company-keywords company-capf))
    company-transformers '(delete-consecutive-dups
                              company-sort-by-occurrence
                              company-sort-prefer-same-case-prefix)
    )

;; Quick-access
;; Candidates Search -> C-s
;; Candidates Filter (just show matching ones) -> C-M-s


;; Fix some things: See: https://github.com/oantolin/orderless

;; Set color. See: M-x list-colors-display
;; See: https://github.com/nsf/gocode/tree/master/emacs-company
;;	https://github.com/company-mode/company-mode/issues/1088
;;
;;	https://github.com/Doerthous/.emacs.d/blob/6804b782548bef12518065b703abe854c1adc2ce/dts-theme.el
;;	https://github.com/railwaycat/emacs-config/blob/37c5278d558653b289d7c0aa511ae2c33a97168d/themes/basic-theme.el

(defvar company-new-fg "navy")
(defvar company-new-bg "navajo white") ; v1: gray30
(defvar company-new-selection-bg "dark goldenrod") ;; v1: gray24
(defvar company-new-common-fg "dark red") ;; v1: light blue
(defvar company-new-annotation-fg "light blue") ;; v1: "light blue"
(defvar company-new-scrollbar-fg "LightGoldenrod2") ;; v1: gray26
(defvar company-new-scrollbar-bg "lemon chiffon") ;; v1: cornsilk
(defvar company-new-preview-fg (face-attribute 'default :foreground))
(defvar company-new-preview-bg "SpringGreen2")

(custom-set-faces
    `(company-tooltip ((t (:background ,company-new-bg :foreground ,company-new-fg))))
    `(company-tooltip-selection ((t (:background ,company-new-selection-bg))))
    `(company-tooltip-common ((t (:foreground ,company-new-common-fg))))
    `(company-tooltip-annotation ((t (:foreground ,company-new-annotation-fg))))
    `(company-scrollbar-fg ((t (:background ,company-new-scrollbar-fg))))
    `(company-scrollbar-bg ((t (:background ,company-new-scrollbar-bg))))
    `(company-preview ((t (:foreground ,company-new-preview-fg :background ,company-new-preview-bg))))
    `(company-preview-common ((t (:foreground ,company-new-common-fg :background ,company-new-preview-bg))))
    `(company-preview-search ((t (:foreground ,company-new-common-fg :background ,company-new-preview-bg))))
    `(company-tooltip-quick-access ((t (:background ,company-new-bg :foreground ,company-new-common-fg))))
    `(company-tooltip-quick-access-selection ((t (:background ,company-new-selection-bg :foreground ,company-new-scrollbar-bg :slant italic))))
    ;;`(company-echo-common ((,class :foreground ,magenta-alt-other)))
    ;;`(company-template-field ((,class :inherit modus-themes-intense-magenta)))
    ;;`(company-tooltip-annotation-selection ((,class :inherit bold :foreground ,fg-main)))
    ;;`(company-tooltip-common-selection ((,class :foreground ,fg-main)))
    ;;'(company-tooltip-mouse ((,class :inherit modus-themes-intense-blue)))
    ;;`(company-tooltip-search ((,class :inherit (modus-themes-search-success-lazy bold))))
    ;;`(company-tooltip-search-selection ((,class :inherit (modus-themes-search-success bold) :underline t)))
    ;;`(company-tooltip-selection ((,class :inherit (modus-themes-subtle-cyan bold))))
    )

;; Icons style (left margin)
(customize-set-variable 'company-format-margin-function #'company-text-icons-margin)
;; Options:     company-detect-icons-margin)
;;      company-text-icons-margin
;;      company-dot-icons-margin
;;      company-vscode-dark-icons-margin
;;      company-vscode-light-icons-margin

;; Left margin
;;(defvar company-text-icons-format "%s ")
(setq company-text-icons-format " %s ") ;; Doesn't work, set directly

;; Icons on the left
(setq company-text-icons-mapping
    '((array "a" font-lock-type-face)
         (boolean "b" font-lock-builtin-face)
         (class "c" font-lock-type-face)
         (color "#" success)
         (constant "c" font-lock-constant-face)
         (enum-member "e" font-lock-builtin-face)
         (enum "e" font-lock-builtin-face)
         (field "f" font-lock-variable-name-face)
         (file "f" font-lock-string-face)
         (folder "d" font-lock-doc-face)
         (interface "i" font-lock-type-face)
         (keyword "k" font-lock-keyword-face)
         (method "m" font-lock-function-name-face)
         (function "f" font-lock-function-name-face)
         (module "{" font-lock-type-face)
         (numeric "n" font-lock-builtin-face)
         (operator "o" font-lock-comment-delimiter-face)
         (parameter "p" font-lock-builtin-face)
         (property "p" font-lock-variable-name-face)
         (ruler "r" shadow)
         (snippet "S" font-lock-string-face)
         (string "s" font-lock-string-face)
         (struct "%" font-lock-variable-name-face)
         (text "w" shadow)
         (value "v" font-lock-builtin-face)
         ;;(value "+" "#FFFFFF" "#FFFFFF")
         (variable "v" font-lock-variable-name-face)
         ;;(variable "+" "#FFFFFF" "#FFFFFF")
         (t "." shadow)))
"Mapping of the text icons.
The format should be an alist of (KIND . CONF) where CONF is a list of the
form (ICON FG BG) which is used to propertize the icon to be shown for a
candidate of kind KIND. FG can either be color string or a face from which
we can get a color string (using the :foreground face-property). BG must be
of the same form as FG or a cons cell of (BG . BG-WHEN-SELECTED) which each
should be of the same form as FG.
The only mandatory element in CONF is ICON, you can omit both the FG and BG
fields without issue.
When BG is omitted and `company-text-icons-add-background' is non-nil, a BG
color will be generated using a gradient between the active tooltip color and
the FG color."
;;  :type 'list)

(setq company-text-icons-format " %s ")
;; Generate a bg color for icons
(setq company-text-icons-add-background 1)


(provide 'module-company)
