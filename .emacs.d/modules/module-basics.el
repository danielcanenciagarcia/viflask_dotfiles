
(customize-set-variable 'blink-cursor-mode nil)
;; Set font
(set-face-attribute 'default nil :family "DejaVu Sans Mono")

;; Scrolling
(setq scroll-step 1)
(setq scroll-margin 5)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse

;; Save cursor position
(save-place-mode)

;; Move the mouse away if the cursor gets close
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Mouse-Avoidance.html
;; (mouse-avoidance-mode 'exile)

;; Disable-mouse
(use-package disable-mouse
  :ensure t
  :load-path "~/.emacs.d/packages/disable-mouse.el")
(global-disable-mouse-mode)
;; For evil
;(mapc #'disable-mouse-in-keymap
;  (list evil-motion-state-map
;        evil-normal-state-map
;        evil-visual-state-map
;        evil-insert-state-map))

;; Enable if using evil
;;(use-package nlinum-relative
;;  :ensure nil
;;  :load-path "~/.emacs.d/packages/nlinum-relative.el"
;;  :config
    ;; something else you want
;;    (nlinum-relative-setup-evil)
;;    (add-hook 'prog-mode-hook 'nlinum-relative-mode)
;;    (setq nlinum-relative-redisplay-delay 0
;;          nlinum-relative-current-symbol "->"
;;          nlinum-relative-offset 0))

;;(setq fringe-mode "left-only")
;;(scroll-bar-mode -1)        ; Disable visible scrollbar
;;(tool-bar-mode -1)          ; Disable the toolbar
;;(tooltip-mode -1)           ; Disable tooltips
;;(set-fringe-mode 10)        ; Give some breathing room
;;(menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell       nil
      ring-bell-function #'ignore)

;; No fringes
(set-fringe-mode 00)

;; Show numbers in left margin
(column-number-mode)
(global-display-line-numbers-mode t)

;; Fixed themes
(setq frame-resize-pixelwise t)

;; You will most likely need to adjust this font size for your system!
;;(defvar efs/default-font-size 180)
;;(defvar efs/default-variable-font-size 180)
;; Useless if not in exwm
;; Make frame transparency overridable
;;(defvar efs/frame-transparency '(75 . 75))

;; Set frame transparency
;;(set-frame-parameter (selected-frame) 'alpha efs/frame-transparency)
;;(add-to-list 'default-frame-alist `(alpha . ,efs/frame-transparency))
;;(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
;;(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))


;; Confirm when killing Emacs
(setq confirm-kill-emacs (lambda (prompt)
                           (y-or-n-p-with-timeout prompt 2 nil)))

(defun my-kill-emacs ()
  "save some buffers, then exit"
  (interactive)
  (save-some-buffers nil t)
  
  "save session for later (just mantains last used)"
  ;(desktop-save-in-desktop-dir)
  "remove to save all sessions"
  ;(desktop-clear)
  
  ;;Undonditional
  ;;(kill-emacs)
  ;;Conditinal
  (save-buffers-kill-terminal))
(global-set-key (kbd "C-x C-c") 'my-kill-emacs)


;; Compilation
;; See compile.el
;; Not now


(provide 'module-basics)


