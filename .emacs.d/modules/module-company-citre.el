

;; Active auto-completion with company-mode
(use-package company
    :ensure t)

(global-company-mode)
(company-tng-mode)
(company-complete)

;; Behave like vim <TAB> to select
(add-hook 'after-init-hook 'company-tng-mode)
(company-tng-configure-default)

(add-hook 'after-init-hook 'global-company-mode)


;; Frontends / Backends
(setq 
    company-frontends
    '(company-pseudo-tooltip-unless-just-one-frontend-with-delay
         company-preview-frontend
         company-echo-metadata-frontend)
    company-backends
    '((company-capf company-keywords))  ;; Just company capf to use with citre and ctags
    company-transformers '(delete-consecutive-dups
                              company-sort-by-occurrence
                              company-sort-prefer-same-case-prefix))

;; Keys


;; Some preferences
(setq
    ;; Quick-access
    company-show-quick-access 'left
    ;; Annotations
    company-tooltip-align-annotations t
    ;; Max number candidates
    company-tooltip-limit 15
    ;; Lines instead of scrollbar.
    company-tooltip-offset-display 'lines
    ;; Guarantee some candidates at screen output
    company-tooltip-minimum 4
    ;; Make them order correctly then
    company-tooltip-flip-when-above t
    ;; Right Margin
    company-tooltip-margin 2
    ;; Always displays a tooltip, regardless of completion
    ;; candidates
    company-pseudo-tooltip-frontend t
    company-idle-delay 0
    company-tooltip-idle-delay 0
    ;;company-tooltip-idle-delay 100   ; Raise tooltip manually with TAB.
    company-require-match nil)


;; Company Theme




(provide 'module-company-citre)
