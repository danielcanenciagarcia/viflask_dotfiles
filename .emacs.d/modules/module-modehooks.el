

;(add-hook 'after-init-hook #'global-flycheck-mode)
(use-package god-mode
    :ensure t)
(god-mode)

;;Toggle on all acitve buffers
(global-set-key (kbd "<escape>") #'god-mode-all)

;;Make sure no buffers are skipped
(setq god-exempt-major-modes nil)
(setq god-exempt-predicates nil)

;;Disable translation
(setq god-mode-enable-function-key-translation nil)
(require 'god-mode)

;;Cursors
(defun hook-update-cursor ()
    (cond ((or (bound-and-true-p god-mode)
               (bound-and-true-p god-global-mode))
           (set-cursor-color "dark orange"))
          (t (set-cursor-color "lime green"))))

(add-hook 'buffer-list-update-hook 'hook-update-cursor)

;; God mode keybindings
;; repeate
(define-key god-local-mode-map (kbd ".") #'repeat)
;; windows
(global-set-key (kbd "C-x C-1") #'delete-other-windows)
(global-set-key (kbd "C-x C-2") #'split-window-below)
(global-set-key (kbd "C-x C-3") #'split-window-right)
(global-set-key (kbd "C-x C-0") #'delete-window)
;; backward / fw paragraph
(define-key god-local-mode-map (kbd "[") #'backward-paragraph)
(define-key god-local-mode-map (kbd "]") #'forward-paragraph)

;; Global keymaps
(define-key god-local-mode-map (kbd "e") #'frog-jump-buffer)



;; no change with feebline
;;(defun my-god-mode-update-mode-line ()
;;  (cond
;;   (god-local-mode
;;    (set-face-attribute 'mode-line nil
;;                        :foreground "#604000"
;;                        :background "#fff29a")
;;    (set-face-attribute 'mode-line-inactive nil
;;                        :foreground "#3f3000"
;;                        :background "#fff3da"))
;;   (t
;;    (set-face-attribute 'mode-line nil
;;			:foreground "#0a0a0a"
;;			:background "#d7d7d7")
;;    (set-face-attribute 'mode-line-inactive nil
;;			:foreground "#404148"
;;			:background "#efefef"))))

;;(add-hook 'post-command-hook 'my-god-mode-update-mode-line)


(provide 'module-modehooks)
