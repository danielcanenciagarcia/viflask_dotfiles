
;; Install package
(use-package modalka
    :ensure t)


;; Change cursor
;;(setq-default cursor-type '(bar . 1))
;;(setq modalka-cursor-type 'box)


;; Enable at startup (except minibuffer)
;;(modalka-global-mode 1)

(provide 'module-modalka)
