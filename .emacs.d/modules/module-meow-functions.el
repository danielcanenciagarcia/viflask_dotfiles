(defun xah-toggle-letter-case ()
  "Toggle the letter case of current word or text selection.
Always cycle in this order: Init Caps, ALL CAPS, all lower.

URL `http://xahlee.info/emacs/emacs/modernization_upcase-word.html'
Version 2020-06-26"
  (interactive)
  (let (
        (deactivate-mark nil)
        $p1 $p2)
    (if (use-region-p)
        (setq $p1 (region-beginning) $p2 (region-end))
      (save-excursion
        (skip-chars-backward "[:alpha:]")
        (setq $p1 (point))
        (skip-chars-forward "[:alpha:]")
        (setq $p2 (point))))
    (when (not (eq last-command this-command))
      (put this-command 'state 0))
    (cond
     ((equal 0 (get this-command 'state))
      (upcase-initials-region $p1 $p2)
      (put this-command 'state 1))
     ((equal 1 (get this-command 'state))
      (upcase-region $p1 $p2)
      (put this-command 'state 2))
     ((equal 2 (get this-command 'state))
      (downcase-region $p1 $p2)
      (put this-command 'state 0)))))

;; meow-append
;;(defun new-meow-append ()
;;  (interactive)
;;  (end-of-line)
;;  (meow-insert))

;; meow-change-char
;;(defun meow-new-change-char (arg)
;;  (interactive "c")
  
;;  "Delete current char and switch to INSERT state."
;;  (when (< (point) (point-max))
;;    (meow-right-expand)
;;    (delete-region (region-beginning) (region-end))
    ;; Or
    ;;(delete-region (point) (1+ (point))) 
    ;; Select char
    ;(insert arg)
;;    (insert arg)))
(provide 'module-functions)
