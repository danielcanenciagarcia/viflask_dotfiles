;;Not being used
(use-package cyphejor
  :ensure t)

(setq
 cyphejor-rules
 '(:upcase
   ("bookmark"    "#")
   ("buffer"      "#")
   ("diff"        "#")
   ("dired"       "#")
   ("emacs"       "#")
   ("fundamental" "#")
   ("inferior"    "i" :prefix)
   ("interaction" "i" :prefix)
   ("interactive" "i" :prefix)
   ("lisp"        "#" :postfix)
   ("menu"        "#" :postfix)
   ("mode"        "")
   ("package"     "#")
   ("python"      "#")
   ("shell"       "sh" :postfix)
   ("text"        "#")
   ("wdired"      "##")))

(cyphejor-mode 1)

(provide 'module-cyphejor-mode)
