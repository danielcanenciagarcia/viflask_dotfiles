
;(use-package all-the-icons
;    :ensure t
;    :init
;    (unless (member "all-the-icons" (font-family-list))
;    (all-the-icons-install-fonts t)))


;(use-package 'all-the-icons-install-fonts)

;;(use-package cycle-themes
;;  :ensure t
;;  :load-path "/home/daniel/.emacs.d/packages/cycle-themes.el"
;;  :init (setq cycle-themes-theme-list
;;          '(darktooth))
;;  :config (cycle-themes-mode))

;; See:: https://github.com/xcodebuild/cycle-themes.el
;;(add-hook 'cycle-themes-after-cycle-hook
;;          #'(lambda ()
;;              (dolist (frame (frame-list))
;;                (set-face-attribute 'fringe frame 
;;                   :background (face-background 'default)))))

;; Dependencie for some themes
(use-package autothemer
  :ensure t)

;; Only works on gtk
;;(use-package base16-theme
;;  :ensure t)
;; For terminal
;;(setq base16-theme-256-color-source "colors")

;; Usage
;;(load-theme 'base16-default-dark t)


;; Smart-mode-line
;;(use-package smart-mode-line
;;  :ensure t
;;  :load-path "~/.emacs.d/packages/smart-mode-line.el"
;;  :config
;;  (setq sml/theme 'dark)
;;  (sml/setup))

;; Doom themes
;;(use-package doom-themes
;;  :ensure t
;;  :config
  ;; Global settings (defaults)
;;  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
;;        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  ;;(load-theme 'doom-sourcerer t)
  ;; Enable flashing mode-line on errors
  ;;(doom-themes-visual-bell-config)

  ;; Corrects (and improves) org-mode's native fontification.
  ;;(doom-themes-org-config)
;;)

;; Set base16 themes
;;(load-theme 'base16-3024 t)

;;bespin, brewer, base16-bright, embers, base16-shapeshifter, base16-pico
;;mexico-light, pop, phd, tube, unikitty

;;base16-grayscale-dark, base16-grayscale-light
;;base16-irblack
;;base16-isotope
;;base16-macintosh
;;base16-solarized-light
;;base16-unikitty-light
;;base16-woodland
;;base16-3024
;;base16-apathy
;;**base16-ashes
;;**base16-atelier-savanna-light
;;base16-atelier-seaside-light
;;base16-bespin
;;base16-brewer

;;**base16-default-light

;;base16-eighties
;;base16-embers


(provide 'module-mythemes)
