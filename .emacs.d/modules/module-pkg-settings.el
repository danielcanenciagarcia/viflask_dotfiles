
;;(use-package which-key
;;  :ensure t
;;  :quelpa (which-key 
;;            :fetcher git
;;            :url https://github.com/justbur/emacs-which-key)
;;  :init (which-key-mode)
;;  :diminish which-key-mode
;;  :config
;;  (setq which-key-idle-delay 1))


;;(use-package hydra
;;  :ensure t)
;;(defhydra hydra-text-scale (:timeout 4)
;;  "scale text"
;;  ("j" text-scale-increase "in")
;;  ("k" text-scale-decrease "out")
;;  ("f" nil "finished" :exit t))

;(efs/leader-keys
;  "ts" '(hydra-text-scale/body :which-key "scale text"))


(use-package term
  :config
  (setq explicit-shell-file-name "bash") ;; Change this to zsh, etc
  ;;(setq explicit-zsh-args '())         ;; Use 'explicit-<shell>-args for shell-specific args

  ;; Match the default Bash shell prompt.  Update this if you have a custom prompt
  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *"))


(provide 'module-pkg-settings)
