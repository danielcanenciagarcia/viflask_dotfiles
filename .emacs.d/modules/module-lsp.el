;;; module-lsp.el --- Module for LSP plugin and derivates

;;; Code:
(use-package lsp-mode
  :ensure t)

;; Setup
(defun my/setup-lsp-mode ()
  (message "my/setup-lsp-mode called")
  (flycheck-mode 1)
  ;;(yas-minor-mode-on)
  (flyspell-prog-mode)
  (lsp-diagnostics-mode 1)
  (lsp-completion-mode 1))

(use-package lsp-mode
  :ensure t
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :custom
  (lsp-log-io nil)
  (lsp-print-performance nil)
  (lsp-report-if-no-buffer nil)
  (lsp-keep-workspace-alive nil)
  (lsp-enable-snippet t)
  (lsp-auto-guess-root t)
  (lsp-restart 'iteractive)
  ;(lsp-session-file)
  (lsp-auto-configure nil)
  ;(lsp-document-sync-method)
  (lsp-auto-execute-action nil)
  (lsp-eldoce-render-all nil)
  (lsp-enable-completion-at-point t)
  (lsp-enable-xref t)
  (lsp-diagnostics-provider :flycheck)
  (lsp-enable-indentation t)
  (lsp-enable-on-type-formatting nil)
  (lsp-before-save-edits nil)
  (lsp-imenu-show-container-name t)
  (lsp-imenu-container-name-separator "/")
  (lsp-imenu-sort-methods '(kind name))
  (lsp-response-timeout 5)
  (lsp-enable-file-watchers nil)
  (lsp-server-trace nil)
  (lsp-semantic-highlighting nil)
  (lsp-enable-imenu t)
  (lsp-signature-auto-activate t)
  (lsp-signature-render-documentation nil)
  (lsp-enable-text-document-color nil)
  (lsp-completion-provider :capf)
  (gc-cons-threshold 100000000)
  (read-process-output-max (* 3 1024 1024))
  :hook ((lsp-mode . my/setup-lsp-mode)) ;;((prog-mode . lsp-deferred))
  :commands (lsp lsp-deferred))

;;; Language Servers
(defvar lsp-language-id-configuration
  '(
    (python-mode . "python")
    (c-mode . "c")
    (c++-mode . "c++")
   ))

(add-hook 'prog-mode-hook #'lsp-deferred)

;; Python
(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp-deferred))))
;; C/C++
(use-package ccls)
(setq ccls-executable "/usr/local/bin/ccls")

;; Customization
(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      lsp-idle-delay 0.1)  ;; clangd is fast

;; optionally
;;(use-package lsp-ui :commands lsp-ui-mode)

;; optionally if you want to use debugger
;;(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language




(provide 'module-lsp)
;;; lsp-ui.el ends here
