;; Diff mode: work with .patch / .diff files
(diff-mode +1)
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Diff-Mode.html

;; LogView Mode (vc-print-log output)
;; See: https://github.com/doublep/logview


;; VC Directory commands
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/VC-Directory-Commands.html

;; VC Manage Branches
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Branches.html

;; Ediff
