
;; GNUS
;; q: quit window / quit gnus correctly
;; c: set something as readed
;; L: view all messages (lower score)
;; I: raise score (show most important messages)
;; B-m: move email to delete
;; B-DEL: delete email permanently
;; In aspecific group:
;;   /s, //: limit to subject. Exclude with C-u -
;;   /a: limit to author
;;   /u: limit to unreaded
;;   /m: limit to a mark
;;   /t: limit by days older
;;   /w: pop previous limit
;;   /M: exclude al marked articles
;;   /T: include articles in current thread
;;   /c: include all cache articles
;;   /N: scan for new articles
;;   /b: limit to body

;; You need to activate cache
;; u: Mark message as important (pin email)
;; d: delete tick mark and set's unread mark
;; M-c: delete all the marks from a message


;; Reading a message / composing it
;; C-x C-s: save email as draft
;; C-c C-k: quit compusing email
;; C-c C-c: send email
;; C-u g: see raw unformated message
;; M-q: reformat a paragraph (composing)
;; g: return to normal mode
;; L: set lower score rule base on ... Hit ? for help 
;;    Ex: [a]uthor then e[xact] match or s[ubstring matching]
;;        and hit p[erpetual] to keep it for forever.
;; I: set higher score rule base on ...


;; Group Buffer at point:
;; (Doesnt work) G G: search email
;; G c: set custom options to the group


;; Topics
;; t: change to topic view
;; T n: create a topic at point
;; T m: move mail to topic

;; News
;; b: check for news



;; Tree view for groups.
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
;; Threads!  I hate reading un-threaded email -- especially mailing
;; lists.  This helps a ton!
(setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject)
(eval-after-load "mm-decode"
 '(progn
      (add-to-list 'mm-discouraged-alternatives "text/html")
      (add-to-list 'mm-discouraged-alternatives "text/richtext")))
;; Group format
(setq gnus-group-line-format "%P%M%S[%5t]%5y : %(%g%)\n")
;; Summary format
(setq gnus-summary-line-format ":%U%R %B %s %-60=|%4L |%-20,20f |%&user-date; \n")

;; Allow to scan new mail on the fly (e.g: \N)
(setq nnimap-get-new-mail t)



(provide 'module-mail)
