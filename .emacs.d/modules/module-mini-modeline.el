;; Minimodeline
(use-package mini-modeline
  :ensure t ;;:quelpa (mini-modeline :repo "kiennq/emacs-mini-modeline" :fetcher github)
  ;;:load-file ""
  :config
  (mini-modeline-mode t))

;; Dont chagne minibuffer faces
;;(setq mini-modeline-enhance-visual nil)
(setq mini-modeline-echo-duration 5)
(setq mini-modeline-display-gui-line nil)


;;  Github Vc Info
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Define faces.

(defface mode-line-vc-added
  `(
     (  ((class color))
        (:background "#FFAA55"  :foreground "orange")  )
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that have just been added to
  version-control.")

(defface mode-line-vc-modified
  `(
     (  ((class color))
        (:background "#F05B80"  :foreground "orange")  )   ; "#F04040" maybe?
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that are under version control
but which have been edited.")

(defface mode-line-vc-in-sync
  `(
     (  ((class color))
        (:background "#60CC60"  :foreground "orange")  )
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that are under version control
  and which are in sync with the respository.")


  ;;Custom Modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  Right side modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq mini-modeline-r-format
    '(""
      ;; Simple Github Control
  
      (:eval
        (if vc-mode
          (let* ((noback (replace-regexp-in-string (format "^ %s" (vc-backend buffer-file-name)) " " vc-mode))
                     (face (cond ((string-match "^ -" noback) 'mode-line-vc)
                      ((string-match "^ [:@]" noback) 'mode-line-vc-edit)
                      ((string-match "^ [!\\?]" noback) 'mode-line-vc-modified))))
                      (format " %s" (substring noback 2)))))
      
      "  "

      ;;"File name: "
      ;;(vc-state (buffer-file-name (current-buffer)))
      ;;"Branch: " 
      ;;(vc-working-revision (buffer-file-name (current-buffer)))

      ;; Eyebrowse indicator
      ;;(eyebrowse-mode (:eval (eyebrowse-mode-line-indicator)))
      ;; Perspective indicator
      (persp-mode (:eval (persp-mode-line)))

      " "
      display-time-string
      ;;battery-mode-line-string
      ;; Mail. See: https://www.emacswiki.org/emacs/DisplayTime
     )
  )

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  Define modeline:
  ;;      - Left side Modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq-default mini-modeline-l-format   
   '(" %z "
      "%e "
      "%b "
      "[%m] "
      compilation-status))

   ;;(set-face-attribute 'mode-line nil
   ;;                 :background "#353644"
   ;;                 :foreground "white"
   ;;                 :box '(:line-width 4 :color "#353644")
   ;;                 :overline nil
   ;;                 :underline nil)

   ;;(set-face-attribute 'mode-line-inactive nil
   ;;                 :background "#565063"
   ;;                 :foreground "white"
   ;;                 :box '(:line-width 4 :color "#565063")
   ;;                 :overline nil
   ;;                 :underline nil)

  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Eyebrowse
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (defun eyebrowse ()
    (eyebrowse-mode (:eval (eyebrowse-mode-line-indicator))))
  (defun perspective ()
    (persp-mode (:eval (persp-mode-line))))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Compilation Status
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (setq compilation-status 
  ;; major modes
    (list 
        '(:eval (propertize "%m" 'face 'font-lock-string-face
                               'help-echo buffer-file-coding-system))
        '("" mode-line-process)))


(provide 'module-mini-modeline)  

