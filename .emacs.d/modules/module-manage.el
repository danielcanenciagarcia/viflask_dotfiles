

;; Make sure packages are downloaded and installed before they are run
;; also frees you from having to put :ensure t after installing EVERY PACKAGE.
;(setq use-package-always-ensure t)

;; Kepp .emacs.d clean
;;(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
;;(setq auto-save-file-name-transforms
;;      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
;; for M-x recentf-open / M-x recentf-open-files
;(require 'recentf)
;(add-to-list 'recentf-exclude no-littering-var-directory)
;(add-to-list 'recentf-exclude no-littering-etc-directory)


;; Quelpa
;;(unless (package-installed-p 'quelpa)
;;  (with-temp-buffer
;;    (url-insert-file-contents "https://raw.githubusercontent.com/quelpa/quelpa/master/quelpa.el")
;;    (eval-buffer)
;;    (quelpa-self-upgrade)))

;; Quelpa use package
;;(quelpa
;; '(quelpa-use-package
;;   :fetcher git
;;   :url "https://github.com/quelpa/quelpa-use-package.git"))
;;(require 'quelpa-use-package)

;;(setq use-package-ensure-function 'quelpa)

(use-package auto-package-update
  :ensure t
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "07:00"))



;; Run upgrades evert 7 days
;(setq quelpa-upgrade-interval 7)
;(add-hook #'after-init-hook #'quelpa-upgrade-all-maybe)


(provide 'module-manage)


