(require 'multistate)

;; Unbind all emacs keybindings
;; (use-global-map (make-sparse-keymap)) ;; Dont use this
(dolist (key '("\C-a" "\C-b" "\C-c" "\C-d" "\C-e" "\C-f" "\C-g"
               "\C-h" "\C-k" "\C-l" "\C-n" "\C-o" "\C-p" "\C-q"
               "\C-t" "\C-u" "\C-v" "\C-x" "\C-z" "\e"))
  (global-unset-key key))


;;(global-set-key (kbd "C-x C-b") #'ibuffer)
(global-set-key (kbd "C-x b") #'switch-to-buffer)
(global-set-key (kbd "C-x b") #'bs-show)
(global-set-key (kbd "C-x C-c") #'my-kill-emacs)
;;(global-set-key (kbd "C-x c") #'my-kill-emacs)
(global-set-key (kbd "C-x C-s") #'save-buffer)
(global-set-key (kbd "C-x s") #'save-some-buffers)
(global-set-key (kbd "C-x C-f") #'find-file)
(global-set-key (kbd "C-x F") #'find-file-other-window)
(global-set-key (kbd "C-x C-r") #'find-file-read-only)
(global-set-key (kbd "C-x C-v") #'find-alternate-file)
(global-set-key (kbd "C-x 4 f") #'find-file-other-window)
(global-set-key (kbd "C-x 5 f") #'find-file-other-frame)
(global-set-key (kbd "C-x C-w") #'write-file)
(global-set-key (kbd "RET") #'newline)
(global-set-key (kbd "C-k") #'kill-line)
(global-set-key (kbd "<backspace>") #'delete-backward-char)
;; See: delete-horizontal-space, just-one-space, delete-blank-lines and delete-indentation
(global-set-key (kbd "C-x o") #'other-window)
(global-set-key (kbd "C-x k") #'kill-buffer)
(global-set-key (kbd "C-x 0") #'delete-window)
(global-set-key (kbd "C-x 1") #'delete-other-windows)
(global-set-key (kbd "C-c 2") #'split-window-below)
(global-set-key (kbd "C-c 3") #'split-window-above)
(global-set-key (kbd "M-x") #'execute-extended-command)
(global-set-key (kbd "C-u") #'universal-argument)
(global-set-key (kbd "C-g") #'keyboard-quit)
(global-set-key (kbd "ESC ESC") (kbd "C-g"))
;; Marks
(global-set-key (kbd "C-x C-x") #'exchange-point-and-mark)
;; Kill ring
(global-set-key (kbd "C-y") #'yank-from-kill-ring)
;; Registers
(global-set-key (kbd "C-x r c") #'copy-register)
(global-set-key (kbd "C-x r i") #'insert-register)
;; append-to-register [r]
;; prepend-to-register [r]
;; view-register [r]
;; store number -> number-to-register
(global-set-key (kbd "C-x r n") #'number-to-register) ;; need argument
(global-set-key (kbd "C-x r +") #'increment-register) ;; need argument
;; Bookmarks
;; Agenda
;; Dired
(global-set-key (kbd "C-x d") #'dired)
;; VC
;; Macros:
;;   C-u: re-execute and append keys, C-u C-u: append keys
(global-set-key (kbd "C-x m") #'kmacro-start-macro-or-insert-counter)
(global-set-key (kbd "C-x M") #'kmacro-end-or-call-macro)
;; Scrolling
(global-set-key (kbd "C-j") #'scroll-down-command)
(global-set-key (kbd "C-k") #'scroll-up-command)
(global-set-key (kbd "C-l") #'recenter-top-bottom)
(global-set-key (kbd "C-L") #'recenter-other-window)
(global-set-key (kbd "C-r") #'recenter)
;; See: reposition-window, recenter


;(defun kill-other-buffers ()
;    "Kill all other buffers."
;    (interactive)
;    (mapc 'kill-buffer
;          (delq (current-buffer)
;                (remove-if-not 'buffer-file-name (buffer-list)))))
;(global-set-key (kbd "C-x 1") 'kill-other-buffers)

(defun other-window-kill-buffer ()
  "Kill the buffer in the other window"
  (interactive)
  ;; Window selection is used because point goes to a different window
  ;; if more than 2 windows are present
  (let ((win-curr (selected-window))
        (win-other (next-window)))
    (select-window win-other)
    (kill-this-buffer)
    (select-window win-curr)))
(global-set-key (kbd "C-x K") 'other-window-kill-buffer)

;; Dont kill this buffers, bury them
(setq my-never-kill-buffers '("*scratch*" "*Messages*"))
(defun my-kill-buffer (buffer)
  "Protect some special buffers from getting killed."
  (interactive (list (current-buffer)))
  (if (member (buffer-name buffer) my-never-kill-buffers)
      (call-interactively 'bury-buffer buffer)
    (kill-buffer buffer)))

(global-set-key [remap kill-buffer] 'my-kill-buffer)


;; Visual line mode off: fix end-of-line
(visual-line-mode nil)
;; Specific multistate functions
(defun my-append ()
   "Move one character forward if we are not in the end of line,
    and enter insert mode"
   (interactive)
   (if (= (point) (save-excursion (end-of-line)
               (point)))
       (multistate-insert-state)
     (forward-char 1)))

(defun my-eol-append ()
   (interactive)
   (end-of-line)
   (multistate-insert-state))

(defun query-replace-regexp-whole-buffer ()
  "query-replace-regexp from the beginning of the buffer."
  (interactive)
  (goto-char (point-min))
  (call-interactively 'query-replace-regexp))

(defun replace-regexp-whole-buffer ()
  "replace-regexp from the beginning of the buffer."
  (interactive)
  (goto-char (point-min))
  (call-interactively 'replace-regexp))


(defun string-append-rectangle ()
  (interactive)
  (call-interactively 'rreplace-regexp "$")
)
(defun my-sol-insert ()
   (interactive)
   (beginning-of-line)
   (multistate-insert-state))

(defun kill-to-bol ()
  "Kill from point to beginning of line."
  (interactive)
  (kill-line 0))

(defun kill-to-eol ()
  "Kill from point to end of line."
  (interactive)
  (kill-line))

(defun kill-to-eol-no-save ()
  "Kill from point to end of line \
without placing it into the kill ring."
  (interactive)
  (delete-region
   (point)
   (line-end-position)))

(defun kill-to-bol-no-save ()
  "Kill from point to beginning of line \
without placing it into the kill ring."
  (interactive)
  (delete-region
   (line-beginning-position)
   (point)))

(defun delete-current-line ()
 "Deletes the current line without \
placing it into the kill ring"
 (interactive)
 (forward-line 0)
 (delete-char (- (line-end-position) (point)))
 (delete-blank-lines))


;; Just some functions to get in touch
(defun backward-kill-line (arg)
  "Kill line backward."
  (interactive)
  (kill-line (- 1)))

;; o/O vim keys
(defun insert-line-below (arg)
  "Insert an empty line below the current line."
  (interactive "p")
  (unless arg (setq arg 1))
  (save-excursion
    (end-of-line)
    (open-line arg)))
(defun insert-line-above (arg)
  "Insert an empty line above the current line."
  (interactive "p")
  (unless arg (setq arg 1))
  (save-excursion
    (end-of-line 0)
    (open-line arg)))

(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point))))

(defun backward-delete-word (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-word (- arg)))

;; kill-eol-no-save
(defun delete-line ()
  "Delete text from current position to end of line char.
This command does not push text to `kill-ring'."
  (delete-region
   (point)
   (progn (end-of-line 1) (point)))
  (delete-char 1))

;; kill-bol-no-save
(defun delete-line-backward ()
  "Delete text between the beginning of the line to the cursor position.
This command does not push text to `kill-ring'."
  (interactive)
  (let (p1 p2)
    (setq p1 (point))
    (beginning-of-line 1)
    (setq p2 (point))
    (delete-region p1 p2)))

(defun delete-whole-line (arg)
  "Delete whole line without pushing to kill-ring."
  (interactive "P")
  (unless arg (setq arg 1))
  (let (p1)
    (setq p1 (line-beginning-position))
    (next-line arg)
    (beginning-of-line)
    (delete-region p1 (point))))
(defun backward-delete-whole-line (arg)
  "Delete whole line backwards without pushing to kill-ring."
  (interactive "P")
  (unless arg (setq arg 1))
  (let (p1)
    (setq p1 (line-end-position))
    (previous-line arg)
    (end-of-line)
    (delete-region (point) p1)))
(defun backward-kill-whole-line (arg)
  "Delete whole line backwards without pushing to kill-ring."
  (interactive "P")
  (unless arg (setq arg 1))
  (let (p1)
    (setq p1 (line-end-position))
    (previous-line arg)
    (end-of-line)
    (kill-region (point) p1)))


(defun do-lines (command &optional start end)
  "Invoke COMMAND on the text of each line from START to END."
  (interactive
   (let* ((key  (read-key-sequence-vector "Hit key sequence: "))
          (cmd  (lookup-key global-map key t)))
     (when (numberp cmd) (error "Not a valid key sequence"))
     (unless (commandp cmd) (error "Key `%s' is not defined" (key-description key)))
     (if (use-region-p)
         (list cmd (region-beginning) (region-end))
       (list cmd (point-min) (point-max)))))
  (setq start  (copy-marker start)
        end    (copy-marker end))
  (save-excursion
    (goto-char start)
    (while (< (point) end)
      (funcall command (buffer-substring (line-beginning-position) (line-end-position)))
      (forward-line 1))))

(defun mark-whole-word (&optional arg allow-extend)
  "Like `mark-word', but selects whole words and skips over whitespace.
If you use a negative prefix arg then select words backward.
Otherwise select them forward.

If cursor starts in the middle of word then select that whole word.

If there is whitespace between the initial cursor position and the
first word (in the selection direction), it is skipped (not selected).

If the command is repeated or the mark is active, select the next NUM
words, where NUM is the numeric prefix argument.  (Negative NUM
selects backward.)"
  (interactive "P\np")
  (let ((num  (prefix-numeric-value arg)))
    (unless (eq last-command this-command)
      (if (natnump num)
          (skip-syntax-forward "\\s-")
        (skip-syntax-backward "\\s-")))
    (unless (or (eq last-command this-command)
                (if (natnump num)
                    (looking-at "\\b")
                  (looking-back "\\b")))
      (if (natnump num)
          (left-word)
        (right-word)))
    (mark-word arg allow-extend)))

;; Go into visual state always whem a selection happens
;;(defvar my-inhibit-multistate-visual-state-hook t)
;(defun multistate-visual-state-hook ()
;  (if my-inhibit-multistate-visual-state-hook (multistate-visual-state))
;)
;(add-hook 'activate-mark-hook 'multistate-visual-state-hook)
;; Get out of visual state and switch to normal state when the mark deactivates
;(add-hook 'deactivate-mark-hook 'multistate-visual-state-hook)

(defun er/expand-region-and-visual ()
  "Call er/expand region and go into visual state,
(the hook will do the job of changing states)"
  (interactive)
  (set (make-local-variable 'my-inhibit-multistate-visual-state-hook) t)
  (er/expand-region 1))

;; We ran into a problem in delete-selection state, this
;; function will be triggered when we enter this state,
;; so we are not in visual state.
(defun exchange-point-and-mark-delete-selection-state ()
  "Exchange point and mark and turn on delete-selection state."
  (interactive)
  (set (make-local-variable 'my-inhibit-multistate-visual-state-hook) nil)
  (exchange-point-and-mark)
)
;; Go to normal mode everytime emacs open a file
(add-hook 'find-file-hook 'multistate-normal-state)


;; Treat hypers (-) at part of the word. Can use in local function.
;; ***************************************
;;(global-superword-mode t)
(defun delete-word-at-point (arg)
  "Delete word at point and then delete word"
  (interactive "p")
  (unless arg (setq arg 1))
  (ignore-errors (my-delete-thing-at-point 'word))
  (delete-word arg))
(defun backward-delete-word-at-point ()
  "Delete word at point and then delete word backwards"
  (interactive "p")
  (unless arg (setq arg 1))
  (ignore-errors (my-delete-thing-at-point 'word))
  (backward-delete-word arg))

(defun my-delete-thing-at-point (thing)
  "Delete the `thing-at-point' for the specified kind of THING."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (if bounds
        (delete-region (car bounds) (cdr bounds))
      (error "No %s at point" thing))))

(defun my-kill-thing-at-point (thing)
  "Kill the `thing-at-point' for the specified kind of THING."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (if bounds
        (kill-region (car bounds) (cdr bounds))
      (error "No %s at point" thing))))

;; Word movement to use with append (can use global-superword-mode)
(defun aux-forward-word ()
  (forward-word)
  (forward-word)
  (backward-word))
(defun fixed-forward-word (arg)
  (interactive "p")
  (unless arg (setq arg 1))
  (dotimes (i arg)
    (aux-forward-word)))
(defun aux-backward-word ()
  (backward-word)
  (backward-word)
  (forward-word))
(defun fixed-backward-word (arg)
  (interactive "p")
  (unless arg (set arg 1))
  (dotimes (i arg)
    (aux-backward-word)))

(defun forward-last-in-word (arg)
  (interactive "p")
  (unless arg (setq arg 1))
  (dotimes (i arg)
    (aux-forward-word)
    (forward-word)
    (backward-char)))
(defun backward-last-in-word (arg)
    (interactive "p")
    (unless arg (setq arg 1))
    (dotimes (i arg)
        (aux-backward-word)
        (backward-char)))

;; Using global-superword-mode ??
(defun forward-last-in-word (arg)
    (interactive "p")
    (unless arg (setq arg 1))
    (set (make-local-variable 'global-superword-mode) t)
    (dotimes (i arg)
        (aux-forward-word)
        (forward-word)
        (backward-char)))
(defun backward-last-in-word (arg)
  (interactive "p")
  (unless arg (setq arg 1))
  (set (make-local-variable 'global-superword-mode) t)
  (aux-backward-word)
  (backward-char))



;; Requires custom-tab-width variable
(defun backspace-whitespace-to-tab-stop ()
  "Delete whitespace backwards to the next tab-stop, otherwise delete one character."
  (interactive)
  (if (or indent-tabs-mode
          (region-active-p)
          (save-excursion
            (> (point) (progn (back-to-indentation)
                              (point)))))
      (call-interactively 'backward-delete-char-untabify)
    (let ((movement (% (current-column) custom-tab-width))
          (p (point)))
      (when (= movement 0) (setq movement custom-tab-width))
      ;; Account for edge case near beginning of buffer
      (setq movement (min (- p 1) movement))
      (save-match-data
        (if (string-match "[^\t ]*\\([\t ]+\\)$" (buffer-substring-no-properties (- p movement) p))
            (backward-delete-char (- (match-end 1) (match-beginning 1)))
          (call-interactively 'backward-delete-char))))))

(defun cycle-mark-ring ()
  (interactive)
  (let ((current-prefix-arg 4)) ;; emulate C-u
    (call-interactively 'set-mark-command)
    )
  )

;; Search within a region
(defun isearch-forward-region-cleanup ()
  "turn off variable, widen"
  (if isearch-forward-region
      (widen))
  (setq isearch-forward-region nil))
(defvar isearch-forward-region nil
  "variable used to indicate we're in region search")
(add-hook 'isearch-mode-end-hook 'isearch-forward-region-cleanup)
(defun isearch-forward-region (&optional regexp-p no-recursive-edit)
  "Do an isearch-forward, but narrow to region first."
  (interactive "P\np")
  (narrow-to-region (point) (mark))
  (goto-char (point-min))
  (setq isearch-forward-region t)
  (isearch-mode t (not (null regexp-p)) nil (not no-recursive-edit)))

(defun search-forward-region (&optional regexp-p no-recursive-edit)
  "Do an isearch-forward, but narrow to region first."
  (interactive "P\np")
  (narrow-to-region (point) (mark))
  (goto-char (point-min))
  (setq search-forward-region t)
  (call-interactively 'search-forward))




;; C-k will kill a line always
;;(setq kill-whole-line t)
;; Expand Region
(require 'expand-region)
(global-set-key (kbd "C-_") 'er/expand-region)
;; contract region (back) -> C-u - C-_ / C-- C-_

;; MWIM
(require 'mwim)

;; CUA Mode
(setq cua-enable-cua-keys nil)
(setq cua-virtual-rectangle-edges nil)

;; Replace marked region with insertion
(delete-selection-mode t)

;; MOVEMENT
;; Go to last change
(require 'goto-last-change)
;; Go to Line
(use-package goto-line-preview
  :ensure t)
(global-set-key [remap goto-line] 'goto-line-preview)
;; Highlight-thing.el
;(use-package highlight-thing)
;(global-highlight-thing-mode)
;;(setq highlight-thing-prefer-active-region t)
;; cusotmize face hi-yellow
;;(setq highlight-thing-case-sensitive-p t)
;; See: https://github.com/fgeller/highlight-thing.el/pull/18
;;(put 'word 'highlight-thing-regex-fn "$[a-zA-Z_0-9]+")
;; highlight just surrounding matches
;;(setq highlight-thing-limit-to-region-in-large-buffers-p nil
;;      highlight-thing-narrow-region-lines 15
;;      highlight-thing-large-buffer-limit 5000)

;; MARKS
;; See: https://www.gnu.org/software/emacs/manual/html_node/emacs/Disabled-Transient-Mark.html
(setq transient-mark-mode t)
;; Cycle through marks in the mark ring.
(setq set-mark-command-repeat-pop t)
;; Allow marks to override paren highlights
(setq show-paren-priority 999)
;;(set-face-background 'region nil) ;; Dont highlight the region
;;(setq show-paren-priority -1)

;; Scrolling
;;keep cursor at same position when scrolling
(setq scroll-preserve-screen-position 1)
(setq next-screen-context-lines 4)


;; Modeline tweaks
(setq multistate-lighter-indicator "")
(setq multistate-lighter-format "---%s---")
;; Maybe remove motion command and append its commands to normal state
(use-package multistate
    :hook
    ;; enable selection is Visual state
    (multistate-visual-state-enter . (lambda () (set-mark (point))))
    (multistate-visual-state-exit . deactivate-mark)
    ;; delete-selection mode => so we can apply command on the selected region
    (multistate-delete-selection-state-enter . exchange-point-and-mark-delete-selection-state)
    (multistate-delete-selection-state-exit . deactivate-mark)
    ;; enable overwrite-mode in Replace state
    (multistate-replace-state-enter . overwrite-mode)
    (multistate-replace-state-exit .  (lambda () (overwrite-mode 0)))
    ;; enable selection in Rectangle state
    (multistate-cua-state-enter . cua-rectangle-mark-mode)
    (multistate-cua-state-exit .  cua-cancel)

    :init
    ;; Emacs state
    (multistate-define-state 'emacs
        :lighter "EMACS"
        :default t)
    ;; Insert state
    (multistate-define-state
        'insert
        :lighter "INSERT"
        :cursor 'bar
        :parent 'multistate-emacs-state-map)
    ;; Normal state
    (multistate-define-state
        'normal
        ;;:default t
        :lighter "NORMAL"
        :cursor 'hollow
        :parent 'multistate-suppress-map)
    ;; Replace state
    (multistate-define-state
        'replace
        :lighter "REPLACE"
        :cursor 'hbar)
    ;; Motion state
                                        ;(multistate-define-state
                                        ; 'motion
                                        ; :lighter "MOTION"
                                        ; :cursor 'hollow
                                        ; :parent 'multistate-suppress-map)
    ;; Visual state
    (multistate-define-state
        'visual
        :lighter "VISUAL"
        :cursor 'hollow
        :parent 'multistate-suppress-map)
    ;; Rectangle State
    (multistate-define-state
        'cua
        :lighter "CUA"
        :cursor 'hollow)
    ;;:parent 'multistate-supress-map)
    ;; Search State
    (multistate-define-state
        'search
        :lighter "SEARCH"
        :cursor 'hollow
        :parent 'multistate-suppress-map)
    ;; Delete Selection State
    (multistate-define-state
        'delete-selection
        :lighter "DS"
        :cursor 'hollow
        :parent 'multistate-suppress-map)
    ;; Delete State (d like commands)
    (multistate-define-state
        'delete
        :lighter "DELETE"
        :cursor 'hollow
        :parent 'multistate-suppress-map)
    ;; Change State (c like commands)
    (multistate-define-state
        'change
        :lighter "CHANGE"
        :cursor 'hollow
        :parent 'multistate-suppress-map)


    ;; Enable multistate-mode globally
    (multistate-global-mode 1)
    ;; Enter other-modes-state. Better use :default t
    ;; (multistate-emacs-state)
    :bind
    (:map multistate-emacs-state-map
        ("<escape>" . keyboard-quit)
        ("<backtab>" . ignore)
        ("<tab>" . ignore)
        ("M-<escape>" . multistate-normal-state))
    (:map multistate-insert-state-map
        ("<escape>" . multistate-normal-state)
        ;;("S-DEL" . delete-horizontal-space)
        ("<backtab>" . backspace-whitespace-to-tab-stop) ;; S-<tab>
        ("<tab>" . tab-to-tab-stop)    ;; Create function to tab-to-stop just when previous line is indented, otherwise indent-relative
        ("C-<tab>" . indent-relative))
    (:map multistate-normal-state-map
        ("<escape>" . keyboard-quit)
        ("z" . multistate-emacs-state)
        ("i" . multistate-insert-state)
        ("I" . my-sol-insert)
        ("a" . my-append)
        ("A" . my-eol-append)
        ("R" . multistate-replace-state)
        ("v" . multistate-visual-state)
        ("V" . multistate-cua-state)
        ("s" . multistate-search-state)
                                        ;("m" . multistate-motion-state)
        ("d" . multistate-delete-state)
        ("c" . multistate-change-state)
        ("C-z" . multistate-emacs-state)
                                        ;("/" . search-forward)
                                        ;("?" . search-backward)

        ;; Frequently used deletion commands
        ("x" . delete-char)
        ("X" . backward-delete-char)
        ;;("c" . replace-word)

        ;;;; Movement keys ;;;;
        ("S-j" . forward-paragraph)
        ("S-k" . backward-paragraph)
        ("h" . backward-char)
        ("j" . next-line)
        ("k" . previous-line)
        ("l" . forward-char)
        ("L" . forward-last-in-word)
        ("H" . backward-last-in-word)
        ("w" . forward-last-in-word)
        ("W" . backward-last-in-word)
        ("0" . mwim-beginning) ;; like 0 in vim ;; see: back-to-indentation
        ("$" . mwim-end)
        ("gg" . beginning-of-buffer)
        ("G" . end-of-buffer)
        ("gl" . goto-line)
        ;;;;;;;;;;;;;;;;;;;;;;;

        ;;;; Commands ;;;;;;;;;
        ;;(":w" . save-some-buffers)
        ;;(":wb" . save-buffer)
        ;;(":q" . my-kill-emacs)

        ;; Marks related
        ("z" . goto-last-change)
        ;; Native
        ("C-SPC" . pop-global-mark)   ;; go to last mark (global)
        ("SPC" . cycle-mark-ring)     ;; like C-u set-mark-command (local)

        ;; Registers (like vim marks)
        ("m" . point-to-register)
        ("'" . jumpt-to-register)

        ;;("W" . mark-whole-word)      ;; fg or bw (with - prefix)
        ("e" . er/expand-region-and-visual)     ;; C-_
                                        ;("E" . er/contract-region)   ;; create function to apply - prefix to e ;; C-- C-_


        ("y" . yank)                ;; C-y
        ("Y" . yank-from-kill-ring)

        ("o" . insert-line-below)
        ("O" . insert-line-above)
        ;;("O" . split-line)
        ("u" . undo)
        ("U" . undo-redo)
        ("." . repeat)


        ("S-<left>" . ignore)
        ("S-<right>" . ignore)
        ("S-<down>" . ignore)
        ("S-<up>" . ignore)
        ("<backspace>" . ignore)
        ("<tab>" . ignore)
        ("RET" . ignore)
        ("M-<delete>" . ignore)
        ("<delete>" . ignore)
        ("M-<DEL>" . ignore)
        ("<DEL>" . ignore))
    (:map multistate-delete-state-map
        ("<escape>" . multistate-normal-state)
        ("d" . delete-current-line)
                                        ;("x" . kill-word-at-point)
        ("w" . delete-word-at-point)
        ("W" . backward-delete-word-at-point)
        ("h" . delete-horizontal-space)
        ("D" . kill-to-eol-no-save)  ;; Create a function to accept negative prefix
        ("C-d" . kill-to-bol-no-save)

        ("K" . backward-delete-line)
        ("k" . delete-whole-line)    ;; C-k kill-line C-k C-k kill-whole-line
                                        ;("K" . backward-kill-line)
                                        ;("k" . kill-line)
        ("x" . delete-char)          ;; (C-d)
        ("X" . backward-delete-char))
    (:map multistate-change-state-map
        ("<escape>" . multistate-normal-state)
        ;;("" . )
        )
    ;;(:map multistate-motion-state-map
    ;;      ("<escape>" . multistate-normal-state))
    (:map multistate-replace-state-map
        ("<escape>" . multistate-normal-state))
    (:map multistate-search-state-map
        ("<escape>" . multistate-normal-state)
        ;;("s" . )
        ("r" . vr/replace)
        ("R" . vr/query-replace)
        ;; Use replace-regexp to use regex syntax
        ("a" . ctrlf-backward-default)
        ("e" . ctrlf-forward-default)
        ("A" . ctrlf-backward-regexp)
        ("E" . ctrlf-forward-regexp)
        ("p" . ctrlf-forward-symbol-at-point) ;; -, search backwards, prefix search nth occurence
        ("s" . ctrlf-forward-symbol)
        ("f" . search-forward) ;; C-u - search backward
        ("." . repeat))

    (:map multistate-delete-selection-state-map
        ("<escape>" . multistate-normal-state))
    (:map multistate-visual-state-map
        ("<escape>" . multistate-normal-state)
        ;; Delete-selection-mode
        ("r" . multistate-delete-selection-state)

        ;;;; Movement keys ;;;;
        ("S-j" . forward-paragraph)
        ("S-k" . backward-paragraph)
        ("h" . backward-char)
        ("j" . next-line)
        ("k" . previous-line)
        ("l" . forward-char)
        ("L" . forward-last-in-word)
        ("H" . backward-last-in-word)
        ;;("q" . backward-last-in-word)
        ;;("e" . forward-last-word)
        ("0" . mwim-beginning) ;; like 0 in vim ;; see: back-to-indentation
        ("$" . mwim-end)
        ("gg" . beginning-of-buffer)
        ("G" . end-of-buffer)
        ("gl" . goto-line)
        ;;;;;;;;;;;;;;;;;;;;;;;

        ;;;; Change region ;;;;
        (";" . comment-region)
        ("A" . string-append-rectangle)
        ("I" . string-insert-rectangle)
        ;;;;;;;;;;;;;;;;;;;;;;;

        ("e" . er/expand-region)
        ("E" . er/contract-region)   ;; create function to apply - prefix to e ;; C-- C-_
        ("f" . search-forward-region)
        ("C-<tab>" . indent-region)    ;; indent everything to start of region
        ("[?\\t]" . indent-rigidly-right-to-tab-stop) ;; See: indent-code-rigidly
        ("S-<tab>" . indent-rigidly-left-to-tab-stop)
                                        ;("v" . (message "Already in visual mode")) ;; Already in visual mode
                                        ;("d" . delete-region)
        ("x" . exchange-point-and-mark)
        ("m" . apply-macro-to-region-lines)

        ("y" . kill-ring-save)   ;; copy  (M-w)
        ("d" . kill-region)      ;; cut   (C-w)
        ("F" . fill-paragraph))  ;; join selection lines in one line
    (:map multistate-cua-state-map
        ("<escape>" . multistate-normal-state)
        ;;("C-v" . (message "Already in cua mode"))
        ("r" . cua-string-rectangle)        ;; (setq cua-delete-selection t) & change to insert mode without clearing selection
                                        ;(cua-replace-in-rectangle  ()))
        ("d" . cua-blank-rectangle)         ;; delete everthing
        ("c" . cua-fill-char-rectangle)     ;; replace with char
        ("R" . cua-replace-in-rectangle)    ;; regex replace
        ;; can do it without keys
        ;;("i" . cua-insert-char-rectangle) ;; insert/delete to left/right (default is left)
        ("x". cua-delete-char-rectangle)    ;; delete char to left/right of rectangle

        ("h" . left-char)
        ("j" . next-line)
        ("k" . previous-line)
        ("l" . right-char)))

;; Fix C-g
;;(global-set-key (kbd "C-g") (kbd "<escape>"))

;; Unbind some unneeded keys
;; "C-f" "C-b" "C-d" "M-d"

(provide 'module-multistate)
