;; Add new line when reaching end of file
;;(setq next-line-add-newlines t)
;; Always finish files with a new line
(setq require-final-newline t)

;; Our Custom Variable
(setq custom-tab-width 4)
;; Default tab width
(setq-default tab-width 4)
;; Language modes
(defun disable-tabs () (setq-default indent-tabs-mode nil))
(defun enable-tabs  ()
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq-default indent-tabs-mode t)
  (setq tab-width custom-tab-width))
;; Disable tabs in all modes
(add-hook 'prog-mode-hook 'disable-tabs)
;;  - Mode specific
;(add-hook 'lisp-mode-hook 'disable-tabs)
;(add-hook 'emacs-lisp-mode-hook 'disable-tabs)
;; Set custom size for all major modes
(require 'universal-indent-size)
(universal-indent-size-set custom-tab-width)
;; Delete tabs correctly
(setq backward-delete-char-untabify-method 'hungry)


;; Dtrt indent
(use-package dtrt-indent
  :ensure t)
(dtrt-indent-global-mode +1)
;(dtrt-indent-mode +1)

;; Smartparents
(use-package smartparens)
;(require 'smartparens-config)
;; Always start smartparens mode in js-mode.
(add-hook 'prog-mode-hook #'smartparens-strict-mode)


;; Highlight tabs
;(global-whitespace-mode)
;(setq whitespace-style '(face tabs tab-mark trailing))
;(custom-set-faces
; '(whitespace-tab ((t (:foreground "#c1c1c1")))))
;; dark: #636363 light: #c1c1c1
;(setq whitespace-display-mappings
;  '((tab-mark 9 [124 9] [92 9])))


;; Format-all -> M-x format-all-buffer
(use-package format-all
  :ensure t)
;; on save
;;(setq format-all-mode)


(flyspell-prog-mode)  ;; spell-checking in comments and strings
;;(electric-indent-mode nil) ;;turn on/off indenting new lines
;(setq-default electric-indent-inhibit t) ;; dont indent prev line on enter

;; Auto indent yanked text
(defun yank-and-indent ()
  "Yank and then indent the newly formed region according to mode."
  (interactive)
  (yank)
  (call-interactively 'indent-region))
(global-set-key [remap yank] 'yank-and-indent)


;; aggresive indent mode (Autoindent as u type)
(use-package aggressive-indent
  :ensure t)
;;(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
;;(add-hook 'css-mode-hook #'aggressive-indent-mode)

(global-aggressive-indent-mode 1)
;;(add-to-list 'aggressive-indent-excluded-modes 'html-mode)

;; Clean whitespaces at save
(use-package whitespace-cleanup-mode
  :ensure t)
(global-whitespace-cleanup-mode 1)
(setq whitespace-cleanup-mode-only-if-initially-clean nil)



;; CUSTOM LANGUAGES
;; json / js
(setq-default js-indent-level custom-tab-width)
;;M-x json-reformat-region
(use-package json-reformat
  :ensure t)
(defun json-setup ()
  (setq locale-coding-system 'utf-8)
)
(add-hook 'json-mode-hook 'json-setup)

;; html
(setq-default sgml-basic-offset custom-tab-width)
;; python
(setq python-indent-guess-indent-offset nil) ; default is t
(setq-default python-indent-offset custom-tab-width)
;;(add-hook 'python-mode-hook
;;      (lambda ()
;;        (setq-default indent-tabs-mode nil)
;;        (setq-default tab-width custom-tab-width)
;;        (setq-default python-indent custom-tab-width)))

;;haskell (haskell-mode-stylish)
(use-package haskell-mode
  :ensure t)

(defun haskell-setup ()
  ;;; Haskell-mode
  ;;(make-local-variable 'tab-stop-list)
  ;;(setq tab-stop-list (number-sequence 0 120 4))
  ;;(setq indent-line-function 'tab-to-tab-stop)
  (setq haskell-indent-spaces custom-tab-width)
  (haskell-indentation-mode -1) ;; turn off, just to be sure
  (haskell-indent-mode 1)       ;; turn on indent-mode

  ;; further customisations go here.  For example:
  ;;(setq locale-coding-system 'utf-8 )
  )
(add-hook 'haskell-mode-hook 'haskell-setup)

;; Evil mode
;; (setq-default evil-shift-width custom-tab-width)

;; Kotlin
;;(use-package kotlin-mode
;;  :ensure t)

(provide 'module-indent)
