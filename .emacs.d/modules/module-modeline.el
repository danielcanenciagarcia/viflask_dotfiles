
  ;;  Github Vc Info
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Define faces.

(defface mode-line-vc-added
  `(
     (  ((class color))
        (:background "#FFAA55"  :foreground "orange")  )
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that have just been added to
  version-control.")

(defface mode-line-vc-modified
  `(
     (  ((class color))
        (:background "#F05B80"  :foreground "orange")  )   ; "#F04040" maybe?
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that are under version control
but which have been edited.")

(defface mode-line-vc-in-sync
  `(
     (  ((class color))
        (:background "#60CC60"  :foreground "orange")  )
     (  t
        (:weight bold :underline t)  )
   )
  "VC status tag face for files that are under version control
  and which are in sync with the respository.")


  ;;Custom Modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  Right side modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq mode-line-end-spaces
    '(""
      ;; Simple Github Control
  
      (:eval
        (if vc-mode
          (let* ((noback (replace-regexp-in-string (format "^ %s" (vc-backend buffer-file-name)) " " vc-mode))
                     (face (cond ((string-match "^ -" noback) 'mode-line-vc)
                      ((string-match "^ [:@]" noback) 'mode-line-vc-edit)
                      ((string-match "^ [!\\?]" noback) 'mode-line-vc-modified))))
                      (format " %s" (substring noback 2)))))
      
      "  "

      ;;"File name: "
      ;;(vc-state (buffer-file-name (current-buffer)))
      ;;"Branch: " 
      ;;(vc-working-revision (buffer-file-name (current-buffer)))

      ;; Eyebrowse indicator
      (eyebrowse-mode (:eval (eyebrowse-mode-line-indicator)))
      ;; Perspective indicator
      ;;(persp-mode (:eval (persp-mode-line)))

      " "
      display-time-string
      battery-mode-line-string

     )
  )

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Padding control
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (defun my-mode-line/padding ()
    (let ((r-length (length (format-mode-line mode-line-end-spaces))))
      (propertize " "
       'display `(space :align-to (- right ,r-length)))))


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  Define modeline:
  ;;      - Left side Modeline
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq-default mode-line-format
   
   '(" %z "
      "%e "
      "%b "
      ;; the buffer name; the file name as a tool tip if you hover the mouse on it
      (:eval (propertize multistate--mode-line-message 'face
         '(:background "blue" :foreground "white")))
            ;;:box (:line-width (10 . 2) :color "green" :style nil))))
      " "
      "[%m] "
      compilation-status
      (:eval (my-mode-line/padding))
      mode-line-end-spaces))

   ;;(set-face-attribute 'mode-line nil
   ;;                 :background "#353644"
   ;;                 :foreground "white"
   ;;                 :box '(:line-width 4 :color "#353644")
   ;;                 :overline nil
   ;;                 :underline nil)

   ;;(set-face-attribute 'mode-line-inactive nil
   ;;                 :background "#565063"
   ;;                 :foreground "white"
   ;;                 :box '(:line-width 4 :color "#565063")
   ;;                 :overline nil
   ;;                 :underline nil)

  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Eyebrowse
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (defun eyebrowse ()
    (eyebrowse-mode (:eval (eyebrowse-mode-line-indicator))))
  (defun perspective ()
    (persp-mode (:eval (persp-mode-line))))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Compilation Status
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (setq compilation-status 
  ;; major modes
    (list 
        '(:eval (propertize "%m" 'face 'font-lock-string-face
                               'help-echo buffer-file-coding-system))
        '("" mode-line-process)))


  (provide 'module-modeline)
  
