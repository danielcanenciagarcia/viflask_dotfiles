;; Selectrum + Prescient

;; Prescient.el
(use-package prescient :ensure t)
;; Selectrum
(use-package selectrum
  :load-path "~/.emacs.d/packages/selectrum.el"
  :ensure t
  :config (selectrum-mode +1))
;; Don't use default filtering
(setq selectrum-prescient-enable-filtering nil)
;; to save your command history on disk, so the sorting gets more
;; intelligent over time
(prescient-persist-mode +1)
;; to make sorting and filtering more intelligent
(use-package selectrum-prescient
  :ensure t
  :config (selectrum-prescient-mode +1))

;; Orderless
;; Use the `orderless' completion style. Additionally enable
;; `partial-completion' for file path expansion. `partial-completion' is
;; important for wildcard support. Multiple files can be opened at once
;; with `find-file' if you enter a wildcard. You may also give the
;; `initials' completion style a try.
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion))))
  ;; Enable just in minibuffer
  :hook (minibuffer-setup . sanityinc/use-orderless-in-minibuffer)
  :config
  (setq completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion))))
  :preface
  (defun sanityinc/use-orderless-in-minibuffer ()
    (setq-local completion-styles '(substring orderless))))

;; For more efficient highlighting (only displayed candidates are highlighted)
;; See: https://www.reddit.com/r/emacs/comments/m9avdn/orderless_selectrum_prescient_and_consult_basic/
(setq orderless-skip-highlighting (lambda () selectrum-is-active))
(setq selectrum-refine-candidates-function #'orderless-filter)
(setq selectrum-highlight-candidates-function #'orderless-highlight-matches)




(provide 'module-selectrum-setup)
