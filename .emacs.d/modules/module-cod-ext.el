(use-package ein
	:ensure t
	:config
	(require 'ein)
	(require 'ein-notebook))

;; M-x ein:run
;; https://millejoh.github.io/emacs-ipython-notebook/#notebook-commands
;; C-c ' ein:edit-cell-contents

;; C-c C-c ein:worksheet-execute-cell

;; C-c C-' ein:worksheet-turn-on-autoexec
;; C-c C-e ein:worksheet-toggle-output
;; C-c C-v ein:worksheet-set-output-visibility-all
;;C-c C-l ein:worksheet-clear-output
;; C-c C-S-l ein:worksheet-clear-all-output
;; C-c C-; ein:shared-output-show-code-cell-at-point
;; C-c C-k ein:worksheet-kill-cell
;; C-c M-w ein:worksheet-copy-cell
;; C-c C-w ein:worksheet-copy-cell
;; C-c C-y ein:worksheet-yank-cell
;; C-c C-a ein:worksheet-insert-cell-above
;; C-c C-b ein:worksheet-insert-cell-below
;; C-c C-t ein:worksheet-toggle-cell-type
;; C-c C-u ein:worksheet-change-cell-type




(provide 'module-cod-ext)
