;; Imenu-list
(use-package imenu-list
    :config
    (setq imenu-list-focus-after-activation t
        imenu-list-auto-resize t)
    )
(global-set-key (kbd "C-ñ") #'imenu-list-smart-toggle)


;; Dired enhacements
(use-package dired-k
    :load-path "~/.emacs.d/packages/dired-k.el")
(define-key dired-mode-map (kbd "K") 'dired-k)
;; You can use dired-k alternative to revert-buffer
(define-key dired-mode-map (kbd "g") 'dired-k)
;; always execute dired-k when dired buffer is opened
(add-hook 'dired-initial-positionhook 'dired-k)
(add-hook 'dired-after-readin-hook #'dired-k-no-revert)
(setq dired-k-style 'git)
(setq dired-k-padding 2)

;; File Tree
;(require 'direx-k)
;(global-set-key (kbd "M-º") 'direx-project:jump-to-project-root-other-window)
;(define-key direx:direx-mode-map (kbd "K") 'direx-k)


;; Undo Tree ??
;;(require 'undo-tree)


;; Search: CTRLF
(use-package ctrlf)
(ctrlf-mode +1)
;; C-s / C-r: fw / bw search
;; C-M-s / C-M-r: fw / bw regexp search
;; M-s _: fw symbol search
;; M-s .: fw symbol search at point
;; In Search: C-s / C-r for next/prev match.


;; Selectrum + Company + Prescient
(require 'module-selectrum-setup)

;;; Completion
;;(require 'module-company)
;;(require 'module-company-dabbrev)
;;(global-set-key (kbd "S-SPC") 'dabbrev-expand) ;; complete just with buffers code

;; LSP
;;(require 'module-lsp)


;;; Ctags, etc. See Bult-In: https://www.emacswiki.org/emacs/BuildTags
;;; GGtags: https://github.com/leoliu/ggtags/wiki/Install-Global-with-support-for-universal-ctags
;; Need gtags, ctags and gctags(script)
(use-package ggtags)
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))

;; Imenu
(setq-local imenu-create-index-function #'ggtags-build-imenu-index)

;;; Keys: See: https://github.com/leoliu/ggtags
;; M-.: ggtags-find-tag-dwim
;; ggtags-find-tag-regexp, C-u to select directory
;; ggtags-find-file
;; ggtags-query-replace
;; For searches:
;; C-u ggtags-find-definition
;; C-u ggtags-find-reference
;; C-u ggtags-find-other-symbol
;; --> M-n, M-p (next/prev match)
;; --> M-{, M-} (n/p file)
;; --> M-= (move to the first file)
;; --> M-<, M-> (first/last match)
;; --> C-M-s / M-s s (isearch to find match)
;; --> M-, (abort and go back to start point)

;;; Dap-mode







;;; Company just for citre (capf backend)
;(require 'module-company-citre)
;;; Citre: Use just for Completion. See: https://github.com/universal-ctags/citre/blob/master/docs/user-manual/toc.md
;; Need ctags/readtags
;(add-to-list 'load-path "~/.emacs.d/packages/citre")
;(require 'citre)
;(require 'citre-config)
;(setq citre-use-project-root-when-creating-tags t)
;(setq citre-prompt-language-for-ctags-command t)
;; (setq citre-edit-tags-file-recipe  ;; customize the command
;; Set this if you want to always use one location to create a tags file.
;(setq citre-default-create-tags-file-location 'global-cache)
;; Generate tags everytime emacs opens a file
;(add-hook 'find-file-hook 'citre-update-this-tags-file)
;; Completion-at-point
;(defun my-completion-at-point ()
;  (interactive)
;  (let ((citre-capf-substr-completion t)
;        (completion-styles '(substring basic)))
;    (completion-at-point)))
;; Imenu
;(setq-default citre-enable-imenu-integration t)
;; Imenu list
;(use-package imenu-list)
;(global-set-key (kbd "C-x m") #'imenu-list-smart-toggle)
;(setq imenu-list-focus-after-activation t)
;(setq imenu-list-auto-resize t)

;; Keys
;(global-set-key (kbd "C-x c j") 'citre-jump)
;(global-set-key (kbd "C-x c J") 'citre-jump-back)
;(global-set-key (kbd "C-x c p") 'citre-ace-peek)
;(global-set-key (kbd "C-x c u") 'citre-update-this-tags-file)
;(global-set-key (kbd "C-x c c") 'my-completion-at-point)


;; Marginalia
;;; Enable richer annotations using the Marginalia package
(use-package marginalia
    :ensure t
    ;; Either bind `marginalia-cycle` globally or only in the minibuffer
    ;;:bind (("M-A" . marginalia-cycle)
    ;;       :map minibuffer-local-map
    ;;       ("M-A" . marginalia-cycle))
    :init
    ;; Must be in the :init section of use-package such that the mode gets
    ;; enabled right away. Note that this forces loading the package.
    (marginalia-mode +1))


(provide 'module-imenus)
