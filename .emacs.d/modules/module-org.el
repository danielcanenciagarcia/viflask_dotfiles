;; ORG MODE
;; org is the default mode for this file extensions 
(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))
;; Enable Org mode
(require 'org)
;; TODO Workflow stages
(setq org-todo-keywords
  '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))
;; GTD Tags
(setq org-tag-alist '(("@work" . ?w) ("@home" . ?h) ("laptop" . ?l)))
;; Tangle
;; Auto tangle plugin:
;;   Add: +auto_tangle: t
(use-package org-auto-tangle
  :ensure t
  :hook (org-mode . org-auto-tangle-mode)
  :config (setq org-auto-tangle-default t))

;; Clock In
;; Here’s some code to make this even easier. The following code clocks in whenever
;; you market task is started, and clocks out when you market a task as WAITING.
;; It also automatically market task is started if you clock in.
;; See: https://sachachua.com/blog/2007/12/clocking-time-with-emacs-org/
(eval-after-load 'org
  '(progn
     (defun wicked/org-clock-in-if-starting ()
       "Clock in when the task is marked STARTED."
       (when (and (string= state "STARTED")
		  (not (string= last-state state)))
	 (org-clock-in)))
     (add-hook 'org-after-todo-state-change-hook
	       'wicked/org-clock-in-if-starting)
     (defadvice org-clock-in (after wicked activate)
      "Set this task's status to 'STARTED'."
      (org-todo "STARTED"))
    (defun wicked/org-clock-out-if-waiting ()
      "Clock out when the task is marked WAITING."
      (when (and (string= state "WAITING")
                 (equal (marker-buffer org-clock-marker) (current-buffer))
                 (< (point) org-clock-marker)
	         (> (save-excursion (outline-next-heading) (point))
		    org-clock-marker)
		 (not (string= last-state state)))
	(org-clock-out)))
    (add-hook 'org-after-todo-state-change-hook
	      'wicked/org-clock-out-if-waiting)))
;; What if you forgot to clock into a task when you started?
;; No problem. Simply clock in and out of it, then edit the
;; starting timestamp for the task in your ~/organizer.org
;; file. To find a starting timestamp, move your cursor to
;; the task headline. If the task has been collapsed to a
;; single line, press TAB to expand it. Look for a line that
;; starts with CLOCK:, or a collapsed segment that starts
;; with :CLOCK:. If you see a collapsed segment, he expanded
;; by moving a cursor to it and pressing tab. Find the clock
;; entry you want to change, and if the timestamp, and press
;; C-c C-y (org-evaluate-time-range) to update the time
;; total.


;; Bookmarks
;; when bookmark is changed, automatically save it
(setq bookmark-save-flag 1)
;; bookmark-set -> add curr buffer to Bookmarks. file or directory
;; list-bookmarks:
;;      - d: mark curr item
;;      - x: remove all marked items
;;      - r: rename curr item
;;      - s: save the change
;; bookmark-jump -> open a bookmarked file


;; Registers
;;  - Persistent information
;;  - Can use them in macros
;; Keys: C-x r is our prefix
;;  - pfx s: store a selection in a reg.
;;  - pfx i: insert a reg
;;  - pfx n: store a number
;;  - pfx +: increment a register by 1
;;     * C-u [number] pfx i: increment a register by [number]

;;  - pfx SPC: store a position for a specific buffer
;;  - pfx j: jump to a position/window-conf stored in a reg
;;  - pfx w: store curr window configuration (per session)
;;  - pfx f: store curr window conf (persistent between sessions)

;; In a macro C-u [number], then <f3> and insert counter
;; (macro-insert-counter / C-x C-k i), u can achieve the
;; same principle if u just need to increment one number.


;; better-registers.el
;;  - Store macros
(require 'better-registers)
(better-registers-install-save-registers-hook)
(load better-registers-save-file)
(setq better-registers-use-C-r nil) ;; dont mess with C-r keybinding
;; pfx - -> decrement by 1 a register
;; pfx m -> store macro in register
;; pfx b -> store buffer in register
;; pfx p -> store filename in register
;; [f1] -> play macro if not playing
;; S-[f1] -> toogle macro
;; Save all registers, except window/frame records:
;;   - better-registers-save-registers
;; File: (setq better-registers-save-file "~/.emacsregisters.el")



;; ORG ROAM
;; New Functions:
;;  - insert-no-buffer
(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))
;; See: https://systemcrafters.net/build-a-second-brain-in-emacs/5-org-roam-hacks/


(use-package org-roam
  :ensure t
  :init (setq org-roam-v2-ack t)
  :custom
    (org-roam-directory "~/RoamNotes")
    (org-roam-completion-everywhere t)
    (org-roam-capture-templates
      '(("d" "default" plain "%?"
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
         :unnarrowed t)
        ("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add initial tasks\n\n* Dates\n\n"
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project")
         :unnarrowed t)
        ))
  :bind
    (("C-c n f" . org-roam-node-find)
     ;; Create a link in curr org buffer to other org files
     ;; and opens a new buffer
     ;;("C-c n i" . insert)
     ;; Exactly the same without opening a buffer
     ("C-c n i" . insert-no-buffer)
     ;; Complete nodes names without writing [[]]
     ("C-M-i" . completion-at-point)
     ;; Backlist Buffer (where my curr node has been referenced)
     ("C-c n l" . org-roam-buffer-toogle)
     ;; Create node for current top-level-heading at cursor
     ;;("" . org-roam-id-get-create)
     ;; Create alias for current top-level-heading node at point
     ;;("" . org-roam-alias-add)
    )
  :config (org-roam-setup))


(provide 'module-org)
