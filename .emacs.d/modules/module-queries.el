;; My keymap using which key

  ;;;;;;;;;;;;;;;;;;;;;;;
  ;; Replace keymap
  ;;;;;;;;;;;;;;;;;;;;;;;
  ;;(define-key some-map "letra" '("which key info" . original-command))

  ;;;;;;;;;;;;;;;;;;;;;;;
  ;; Empty keymap bind
  ;;;;;;;;;;;;;;;;;;;;;;;
  ;; (define-key some-map "C-r" '("replace selection" . (keymap)))



;  (defun new_frame ()
;    "Lists the contents of the current directory."
;    (interactive)
;    (shell-command "emacsclient -c -n -e '(switch-to-buffer nil)'"))

;  ;; Shell commands
;  (local-set-key (kbd "C-x") 'new_frame)

  ;; Multiple cursors
  (use-package multiple-cursors
	:ensure t)

  (require 'visual-regexp)

  ;; Searching with deadgrep
  ;;(use-package deadgrep
  ;;   :ensure t)

  ;; Search in a file
  ;;(global-set-key (kbd "<f5>") #'deadgrep)
  ;; Search for a file

  ;(use-package find-file-in-project
  ;:ensure t
  ;;:config
  ;;(setq ffip-prefer-ido-mode t)
  ;)

  (provide 'module-queries)
