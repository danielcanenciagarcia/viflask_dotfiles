;;; Windowrules --- Summary


;;; Commentary:
;;; See: https://www.gnu.org/software/emacs/manual/html_node/elisp/Frame-Layouts-with-Side-Windows.html
;;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Buffer-Display-Action-Alists.html
;;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Window-Parameters.html


;;; Code:
(setq display-buffer-alist
    `(;; no window
         ;; left side window
         ("\\*\\(Help\\|Apropos\\|Ilist\\)\\*"
             (display-buffer-reuse-window display-buffer-in-side-window)
             (window-width . 0.35)
             (side . right)
             ;;(dedicated . t)
             (slot . 0))
         ("\\`\\*Async Shell Command\\*\\'"
             (display-buffer-no-window))
         ;; top side window
         ("\\*\\(Flymake diagnostics\\|Package-Lint\\).*"
             (display-buffer-in-side-window)
             (window-height . 0.20)
             (side . top)
             (slot . -1))
         ("\\*Messages.*"
             (display-buffer-in-side-window)
             (window-height . 0.20)
             (side . top)
             (slot . 1))
         ("\\*\\(Backtrace\\|Warnings\\|Compile-Log\\|Flymake log\\)\\*"
             (display-buffer-in-side-window)
             (window-height . 0.20)
             (side . bottom)
             (slot . 1))
         ;; bottom side window
         ("\\*Org Select\\*"
             (display-buffer-in-side-window)
             (dedicated . t)
             (side . bottom)
             (slot . 0)
             (window-parameters . ((mode-line-format . none))))
         ("\\*\\(Output\\|Register Preview\\).*"
             (display-buffer-reuse-window display-buffer-at-bottom))
         ;; below current window
         ("\\*.*\\(e?shell\\|v?term\\).*"
             (display-buffer-reuse-window display-buffer-in-side-window)
             (window-height . 0.70)
             (side . bottom)
             (slot . -1))
         ("\\*\\vc-\\(incoming\\|outgoing\\|git : \\).*"
             (display-buffer-reuse-window display-buffer-in-side-window)
             ;; NOTE 2021-10-06: we cannot `fit-window-to-buffer' because
             ;; the height is not known in advance.
             (side . left)
             (window-height . 0.50))
         ("\\*\\(Calendar\\|Bookmark Annotation\\).*"
             (display-buffer-reuse-mode-window display-buffer-in-side-window)
             (window-height . 0.60)
             (side . right)
             (slot . 0))
         ("\\*scratch.*"
             (display-buffer-in-side-window)
             (window-height . 0.60)
             (side . right)
             (slot . 0))
         ;;(".*"
         ;; (display-buffer-reuse-window display-buffer-same-window))
         ))

;;(window-height . fit-window-to-buffer-max-half-frame)))))


;; Eyebrowse fix
(add-to-list 'window-persistent-parameters '(window-side . writable))
(add-to-list 'window-persistent-parameters '(window-slot . writable))


;; prompt wheter to allow switch to buffer in dedicated
;; window when called interactively
(setq switch-to-buffer-in-dedicated-window 'prompt)
;; Side windows are dedicated. Dedicated means functions for
;; displaying buffer can't use them.
;; Doesnt really make a difference if using Popper.


;; Set how display-buffer and derivates work
;;(custom-set-variables
;; '(display-buffer-base-action
;;   '((display-buffer--maybe-same-window display-buffer-reuse-window display-buffer--maybe-pop-up-frame-or-window display-buffer-in-previous-window display-buffer-below-selected display-buffer-at-bottom display-buffer-pop-up-frame)))
;; '(window-min-height 8)
;; '(window-min-width 40))

;; Stop pop up windows to fuck up our splits
;; Make popper windows go bottom
(custom-set-variables '(display-buffer-base-action
  '((display-buffer--maybe-same-window display-buffer-reuse-window
     display-buffer--maybe-pop-up-frame-or-window display-buffer-in-previous-window
     display-buffer-below-selected display-buffer-at-bottom display-buffer-pop-up-frame)))
    ;'((display-buffer--maybe-same-window display-buffer-reuse-window
    ;   display-buffer-same-window ;; If in normal window
        ;;display-buffer-use-least-recent-window ;; If in dedicated window (ex side window, popper popup) use least recently switched window
    ;   )))
    ;; display-buffer-in-child-frame: splits curr parent
    '(reusable-frames t)
    ;'(window-height 'fit-window-to-buffer)
    ;'(window-min-height 8)
    ;'(window-min-width 40)
    ;; If t, display-buffer calls dont use dedicated windows
    ;; For Use in custom functions, like split-file...
    ;'(dedicated f))
)

;; Popper
(use-package popper
  :ensure t ; or :straight t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))  ;; Convert buffer into popup
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "*scratch[1-9]*" ;; *scratchN* (see scratch own defun)
          ;;"*Ilist*"
          help-mode
          eshell-mode
          shell-mode
          vterm-mode
          term-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1)
  :config
  (setq popper-display-control nil))

;; popper-display-control: just pop up ibuffer
;; Projectile grouping
;;(setq popper-group-function #'popper-group-by-projectile)
;; Popper can hide buffers at first (can cycle through them
;; later)
;; Directory grouping
(setq popper-group-function #'popper-group-by-directory)

;; Expand window
(defun halve-other-window-height ()
  "Expand current window to use half of the other window's lines."
  (interactive)
  (enlarge-window (/ (window-height (next-window)) 2)))
;; See every window
(global-set-key (kbd "C-c v") 'halve-other-window-height)

;; Resize Windows
(global-set-key (kbd "C-M-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-M-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-M-<down>") 'shrink-window)
(global-set-key (kbd "C-M-<up>") 'enlarge-window)
(global-set-key (kbd "C-M-h") 'shrink-window-horizontally)
(global-set-key (kbd "C-M-l") 'enlarge-window-horizontally)
(global-set-key (kbd "C-M-j") 'shrink-window)
(global-set-key (kbd "C-M-k") 'enlarge-window)

;; WindMove
(require 'windmove) ;; Use S-direction to move cursor to other windows
(global-set-key (kbd "M-<left>")  'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<up>")    'windmove-up)
(global-set-key (kbd "M-<down>")  'windmove-down)
(global-set-key (kbd "M-h")  'windmove-left)
(global-set-key (kbd "M-l") 'windmove-right)
(global-set-key (kbd "M-k")    'windmove-up)
(global-set-key (kbd "M-j")  'windmove-down)

;; Rotate layouts
(require 'rotate)
(global-set-key (kbd "M-q") 'rotate-layout)
(global-set-key (kbd "M-w") 'rotate-window)


;; Change window layout
;; balance windows -> C-x +
(global-set-key (kbd "C-x -") 'fit-window-to-buffer) ; hyphen
;; Display buffers
(global-set-key (kbd "M-b") 'display-buffer)
(global-set-key (kbd "M-B") 'ibuffer)

;;(setq winner-dont-bind-my-keys t)
;;(winner-mode 1) ;; Undo / redo per window
;; Default: C-c left, C-c right -> undo /redo (dont use them)
;;(global-set-key (kbd "C-c o") 'winner-undo)
;;(global-set-key (kbd "C-c p") 'winner-redo)

;;(setq winner-ring-size 250) ;; max configs per frame
;;(setq winner-boring-buffers
;;        (append winner-boring-buffers '("*Help*", "*Apropos*", "*Warnings*", "*Calendar*", "*Messages*",
;;                                        "*IBuffer*", "*Buffer List*", "*info*", "*Compile-Log*")))
;;(setq winner-boring-buffers-regexp "\\*[hH]elm.*") ;; out of winner-mode


(defun delete-other-windows-vertically ()
  "Delete all windows above or below the current window."
  (interactive)
  (save-excursion
    (while (condition-case nil (windmove-up) (error nil))
      (delete-window))
    (while (condition-case nil (windmove-down) (error nil))
      (delete-window))))

(defun delete-other-windows-horizontally ()
  "Delete all windows left or right of the current window."
  (interactive)
  (save-excursion
    (while (condition-case nil (windmove-left) (error nil))
      (delete-window))
    (while (condition-case nil (windmove-right) (error nil))
      (delete-window))))

(defun delete-layout-windows (&optional arg)
    "If ARG is 2 delete all windows which are above or below the
current window.  If ARG is 3 delete all windows which are left or
right to the current window.  If no prefix arg is given, delete
all other windows."
    (interactive "p")
    (if (equal arg 2)
        (delete-other-windows-vertically)
        (if (equal arg 3)
            (delete-other-windows-horizontally)
            (delete-other-windows-vertically)
            (delete-other-windows-horizontally))))
  ;;(delete-other-windows-vertically))

;;(global-set-key (kbd "C-x 1") 'delete-layout-windows) ;; overrides kill-only


;; Consult
;;(use-package consult
;;  :ensure t)


;; Scratch generator
(defun scratch ()
  "create a new scratch buffer to work in. (could be *scratch* - *scratchX*)."
  (interactive)
  (let ((n 0)
        bufname)
    (while (progn
             (setq bufname (concat "*scratch"
                                   (if (= n 0) "" (int-to-string n))
                                   "*"))
             (setq n (1+ n))
             (get-buffer bufname)))
  ;;(switch-to-buffer (get-buffer-create bufname))
  (display-buffer (get-buffer-create bufname))
  (if (= n 1) initial-major-mode))) ; 1, because n was incremented


;; Split and Open File Functions
(defun split-file-above (filename)
  "Splits curr window & opens given file on its given side."
  (interactive "F")
  (let ((window-a (split-window-below))))
  (display-buffer (find-file filename) window-a)
  )

(defun split-file-below (filename)
  "Splits curr window & opens given file on its given side."
  (interactive "F")
  (split-window-below)
  (let ((window-a (buf-move-down))))
  (display-buffer (find-file filename) window-a)
  )

(defun split-file-left (filename)
  "Splits curr window & opens given file on its left side"
  (interactive "F")
  (split-file filename 'right))

(defun split-file-right (filename)
  "Splits curr window & opens given file on its right side"
  (interactive "F")
  (split-file filename 'left))

(defun split-file (filename side)
  "Splits curr window & opens given file on its given side"
  (let ((window-a nil)
        (window-b (selected-window)))
    (split-window window-b nil side)
    (display-buffer (find-file filename) window-a)))

;; File Split Keybindings :)
(global-set-key (kbd "C-x 2") 'split-file-below)
(global-set-key (kbd "C-x 3") 'split-file-right)


;; Function to open a file in other frame
;; using display-buffer-other-frame

;; Function to delete every other window visible in frame and place
;; our file
;; https://github.com/magit/magit/issues/1953
(provide 'module-windowrules)

;;; module-windowrules.el ends here
