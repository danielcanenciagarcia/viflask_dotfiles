
;;(defun haskell-lint-hook ()
;;    (local-set-key "\C-cl" 'hs-lint))
;;(add-hook 'haskell-mode-hook 'haskell-lint-hook)

;; See: https://www.flycheck.org/en/latest/languages.html#flycheck-languages
(use-package flycheck
  :ensure t)

;; C-c ! n / C-c ! p -> fw/bw through errors
;; C-c ! l -> See list of errors


;; Check complete setup -> C-C ! v  /  M-x flycheck-verify-setup
;; Debug                -> C-c ! C-c  /  flycheck-compile
(add-hook 'after-init-hook #'global-flycheck-mode)
(provide 'module-linters)
