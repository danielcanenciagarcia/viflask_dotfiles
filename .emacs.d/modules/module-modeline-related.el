
;  (use-package moody
;    :config
;      (setq x-underline-at-descent-line t)
;      (moody-replace-mode-line-buffer-identification)
;      (moody-replace-vc-mode))

;  (use-package minions
;    :init (minions-mode)
;    :config
;    (setq
;     minions-mode-line-lighter "&")
;     minions-direct '(flycheck-mode))   


;;  (use-package    feebleline
;;    :ensure       nil
;;    :load-path "~/.emacs.d/packages/feebleline.el"
;;    :config
;;                  (setq feebleline-msg-functions
;;                    '((feebleline-line-number       :post "" :fmt "%5s")
;;                    (feebleline-column-number       :pre ":" :fmt "%-2s")
;;                    (feebleline-file-directory      :face feebleline-dir-face :post "")
;;                    (feebleline-file-or-buffer-name :face font-lock-keyword-face :post "")
;;                    (feebleline-file-modified-star  :face font-lock-warning-face :post "")
;;                    (feebleline-git-branch          :face feebleline-git-face :pre " : ")
;; ;;                    (feebleline-mode-name           :pre "[" :post "]" :align right)
;;                    (feebleline-eyebrowse           :align right)
;;                    (feebleline-project-name        :align right)))
;;                    (feebleline-mode 1))
  
;minions-direct '()
;minions-mode-line-delimiters '("(" . ")")
;minions-mode-line-face nil))

;; Keybindings
;;  (global-set-key [S-down-mouse-3] 'minions-minor-modes-menu)



;; Display Time
;;(setq display-time t)
;;(setq display-time-format nil)
;; 24-hour format
(setq display-time-24hr-format t)
(setq display-time-format "%H:%M")
;; This causes the current time in the mode line to be displayed in
;; `egoge-display-time-face' to make it stand out visually.
;;(setq display-time-string-forms
;;      '((propertize (concat " " 24-hours ":" minutes " ")
;;            	    'face 'egoge-display-time)))
(setq display-time-default-load-average nil)

;; Mail
;; display-time-mode mail notification
(defface display-time-mail-face '((t (:background "ghost")))
    "If display-time-use-mail-icon is non-nil, its background colour is that
     of this face. Should be distinct from mode-line. Note that this does not seem
     to affect display-time-mail-string as claimed.")
(setq
 ; display-time-mail-file "/var/mail/username"
  display-time-use-mail-icon t
  display-time-mail-face 'display-time-mail-face)
(display-time-mode t)


(provide 'module-modeline-related)
