
;; Save everything before exiting emacs
;;(defun xah-save-all-unsaved ()
;;  "Save all unsaved files. no ask.
;;Version 2019-11-05"
;;  (interactive)
;;  (save-some-buffers t ))

;;(if (version< emacs-version "27")
;;    (add-hook 'focus-out-hook 'xah-save-all-unsaved)
;;  (setq after-focus-change-function 'xah-save-all-unsaved))

;; Stop creating # files
(setq create-lockfiles nil)

;; auto save often
;; save every 20 characters typed (this is the minimum)
;;(setq auto-save-interval 20)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; backup settings                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://www.emacswiki.org/emacs/BackupFiles
(defvar --backup-directory "~/.backups-emacs")
(if (not (file-exists-p --backup-directory))
        (make-directory --backup-directory t))
(setq
 backup-by-copying t     ; don't clobber symlinks
 backup-directory-alist `(("." . "~/.backups-emacs"))
 kept-new-versions 20    ; keep 20 latest versions
 kept-old-versions 0     ; don't bother with old versions
 delete-old-versions t   ; don't ask about deleting old versions
 version-control t       ; number backups
 vc-make-backup-files t  ; backupi version controlled files
 auto-save-default t     ; auto-save every buffer that visits a file
 auto-save-timeout 20    ; number of seconds idle time before auto-save (default: 30)
 auto-save-interval 200  ; number of keystrokes between auto-saves (default: 300)
)

;; make backup to a designated dir, mirroring the full path

(defun my-backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* (
        (backupRootDir "~/.backups-emacs/")
        (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath )) ; remove Windows driver letter in path, for example, "C:"
        (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") ))
        )
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath
  )
)

;;(setq make-backup-file-name-function 'my-backup-file-name)

;; Dont make backups of some files (gpg, vcf, ...)
(setq auto-mode-alist
      (append
       (list
        '("\\.\\(vcf\\|gpg\\)$" . sensitive-minor-mode)
        )
       auto-mode-alist))

;; Store by date
(defun make-backup-file-name (FILE)                                             
  (let ((dirname (concat --backup-directory                                    
                         (format-time-string "%y/%m/%d/"))))                    
    (if (not (file-exists-p dirname))                                           
        (make-directory dirname t))                                             
    (concat dirname (file-name-nondirectory FILE))))
;;(setq make-backup-file-name-function 'make-backup-file-name)

;; delete all file whose name end in ~. All subdir too.
;;cd ~/.emacs.d/ && find . -name "*~" -delete

(provide 'module-saving)
