;; Ido
(require 'ido)
(ido-mode t)
(ido-everywhere)
;; Make *Ido Completion* show only candidates
(setq ido-completion-buffer-all-completions t)
;; Use ido to complete commands via M-X
(global-set-key
 "\M-x"
 (lambda ()
   (interactive)
   (call-interactively
    (intern
     (ido-completing-read
      "M-x "
      (all-completions "" obarray 'commandp))))))

;; Maintain a list of recent files. C-x C-r to open from that list
(require 'recentf)
;; enable recent files mode.
(recentf-mode t)
;; resurrect killed buffers
(setq ido-use-virtual-buffers t)
; 50 files ought to be enough.
(setq recentf-max-saved-items 50)
;; show any name that has the chars you typed
(setq ido-enable-flex-matching t)
;; use current pane for newly opened file
(setq ido-default-file-method 'display)
;; use current pane for newly switched buffer
(setq ido-default-buffer-method 'other-window)
 ;; disable auto-merge
(setq ido-auto-merge-work-directories-length -1)
;; Flex matching & ignore prefix: aa -> alpha, gamma
(setq ido-enable-flex-matching t)
(setq ido-enable-prefix nil) 
;; Ignore every *..* buffer except ..
(setq my-unignored-buffers '("*ielm*" "*scratch*" "\\*.*\\(e?shell\\|v?term\\).*"))
(defun my-ido-ignore-func (name)
  "Ignore all non-user (a.k.a. *starred*) buffers except those listed in `my-unignored-buffers'."
  (and (string-match "^\*" name)
       (not (member name my-unignored-buffers))))
(setq ido-ignore-buffers '("\\` " my-ido-ignore-func))
;; Allow spaces in ido-find-file
(add-hook 'ido-make-file-list-hook
          (lambda ()
            (define-key ido-file-dir-completion-map (kbd "SPC") 'self-insert-command)))
;; Extensions ordering and ignored ones
(setq ido-file-extensions-order     '(".py" ".cc" ".h" ".tex" ".sh" ".org"
                                      ".el" ".tex" ".png"))
(setq completion-ignored-extensions '(".o" ".elc" "~" ".bin" ".bak"
                                      ".obj" ".map" ".a" ".so"
                                      ".mod" ".aux" ".out" ".pyg"))
(setq ido-ignore-extensions t)
;; Max matching items
(setq ido-max-prospects 10)
;; Prompt if a file buffer exist 
(setq ido-create-new-buffer 'prompt)
;; Root's files finding
(defadvice ido-find-file (after find-file-sudo activate)
  "Find file as root if necessary."
  (unless (and buffer-file-name
               (file-writable-p buffer-file-name))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(defun ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))


;; get rid of `find-file-read-only' and replace it with something
;; more seful.
(global-set-key (kbd "C-x C-r") 'ido-recentf-open)
;; In find file (C-x C-f ) -> C-j: accept typed, C-f: drop into regular find-file
(define-key minibuffer-local-completion-map " " 'self-insert-command) ;; make SPC behave as usual
;; C-a to toogle hidden files
(define-key ido-file-completion-map (kbd "C-d") 'ido-delete-backward-updir) ;; delete directories backwards
;; Navigate through history
(defun ido-my-keys ()
  (define-key ido-completion-map (kbd "<up>")   'ido-prev-match)
  (define-key ido-completion-map (kbd "<down>") 'ido-next-match))
(add-hook 'ido-setup-hook 'ido-my-keys)

(provide 'module-ido)
