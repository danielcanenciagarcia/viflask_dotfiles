;; Alinear
(global-set-key (kbd "C-x a") 'align-regexp)


;; INDENTATION
(defun simple-indent (start end)
  (interactive "r")
  (let ((size 4))
    (if (region-active-p)
        (indent-rigidly start end size)
      (if  (bolp)
          (indent-rigidly (line-beginning-position) (line-end-position) size)))))

(global-set-key (kbd "C-o") #'simple-indent)
(global-set-key (kbd "C-O") #'simple-unindent)

;; Snippets
;;(global-set-key (kbd "C-o") "my snippet")


;; Company Keys
;(define-key company-active-map (kbd "C-h") 'company-show-doc-buffer)
;; Default:  (ctags ~/path/to/project needed)
;;  - Completion will start automatically after you type a few letters. Use M-n and M-p to select,
;;    <return> to complete or <tab> to complete the common part. Search through the completions
;;    with C-s, C-r and C-o. Press M-(digit) to quickly complete with one of the first 10 candidates.

;; <TAB> / S-<TAB>   -> fw / bw (use with company-tgn)
;; Go to source code -> C-w
;(eval-after-load 'company
;    '(lambda ()
;        (define-key company-active-map (kbd "C-c h") #'company-quickhelp-manual-begin)
;        ;;(define-key company-active-map (kbd "M-n") 'company--select-next-and-warn)
;        ;;(define-key company-active-map (kbd "M-p") 'company--select-previous-and-warn)
;        ;;(define-key company-active-map (kbd "C-n") 'company-select-next-or-abort)
;        ;;(define-key company-active-map (kbd "C-p") 'company-select-previous-or-abort)
;        (define-key company-active-map (kbd "C-s") 'company-search-candidates)
;        (define-key company-active-map (kbd "C-M-s") 'company-filter-candidates)
;    ))

;; Workspaces (eyebrowse) ?
(global-set-key (kbd "M-e M-0") 'eyebrowse-switch-to-window-config-0)
(global-set-key (kbd "M-e M-1") 'eyebrowse-switch-to-window-config-1)
(global-set-key (kbd "M-e M-2") 'eyebrowse-switch-to-window-config-2)
(global-set-key (kbd "M-e M-3") 'eyebrowse-switch-to-window-config-3)
(global-set-key (kbd "M-e M-4") 'eyebrowse-switch-to-window-config-4)
(global-set-key (kbd "M-e M-5") 'eyebrowse-switch-to-window-config-5)
(global-set-key (kbd "M-e M-e") 'eyebrowse-create-window-config)
(global-set-key (kbd "M-e 0") 'eyebrowse-switch-to-window-config-0)
(global-set-key (kbd "M-e 1") 'eyebrowse-switch-to-window-config-1)
(global-set-key (kbd "M-e 2") 'eyebrowse-switch-to-window-config-2)
(global-set-key (kbd "M-e 3") 'eyebrowse-switch-to-window-config-3)
(global-set-key (kbd "M-e 4") 'eyebrowse-switch-to-window-config-4)
(global-set-key (kbd "M-e 5") 'eyebrowse-switch-to-window-config-5)
(global-set-key (kbd "M-e e") 'eyebrowse-create-window-config)

;;"C-c w N" 'eyebrowse-switch-to-window-config-N
;"C-c w o" 'previous
;"C-c w p" 'next
;"C-c w r" 'rename
;"C-c w q" 'close
;"C-c w l" 'last



(provide 'module-commonkeys)
