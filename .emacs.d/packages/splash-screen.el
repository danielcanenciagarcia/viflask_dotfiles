(defvar fancy-splash-image-template
  (expand-file-name "splash-images/emacs-e.png" emacs-dir)
  "Default template svg used for the splash image, with substitutions from ")


;; Or if you use use-package
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

; emacsclient -c
(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
;; Set the title
(setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
;; Set the banner
(setq dashboard-startup-banner fancy-splash-image-template)
;; Content is not centered by default. To center, set
(setq dashboard-center-content t)
;; Customize widgets & number of items
(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        ;(projects . 5)
                        (agenda . 5)
                        (registers . 5)))

;; Navigator
;;(setq dashboard-set-navigator t)
;; Info about the packages loaded..
;;(setq dashboard-set-init-info t)
;; Or other info
;;(setq dashboard-init-info "This is an init message!")
;; No Random footers
(setq dashboard-set-footer nil)
;; Custom footers
;;(setq dashboard-footer-messages '("Dashboard is pretty cool!"))
;; Switch Project Function
;;(setq dashboard-projects-switch-function 'projectile-persp-switch-project)

;; Org's mode agenda

;; Faces


(provide 'splash-screen)
