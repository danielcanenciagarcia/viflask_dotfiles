

;; Minibuffer
;(set-face-foreground 'minibuffer-prompt "navajo0");DarkOrange2")
;;(with-current-buffer " *Echo Area 0*"
;  (face-remap-add-relative 'default '(:inherit mode-line)))


; TODO: It's more complicated than the above.  I have to set the :background color                                     
;;       to "black" whether I'm in the GUI or terminal.  If I don't, then the colors                                    
;;       are less saturated than I like.  The amount of saturation is something like:                                   
;;                                                                                                                      
;;       least   frame-background-mode 'dark    default :background "black"              orange comments                
;;               frame-background-mode 'dark    default face (no background specified)   orange comments                
;;               frame-background-mode 'light   default face (no background specified)   red comments                   
;;       most    frame-background-mode 'light   default :background "black"              deep red comments              
;;                                                                                                                      
;;       The first 2 ones may be the same - I didn't compare side-by-side.                                              
;;                    

(setq frame-background-mode 'light)
;; Error in v 29.0.50
;;(set-face-attribute 'default nil :background "dark")


;; *Completions*
;(custom-set-faces
;	'(completions-common-part  (( ((background  dark)) :foreground "dark orchid")
;				    ( ((background light)) :foreground "DarkGoldenrod4" )));;firebrick"))));DarkGoldenrod4"))))

;; Minibuffer
(custom-set-faces
    '(minibuffer-prompt (( ((background  dark)) :foreground "navajo4")
	            	     ( ((background light)) :foreground "ivory")))) ;; :background "firebrick"))));DarkOrange2")



;; Cursor selection

;; Cursor


;; Modeline Theme (colors)

;; eyebrows
;(add-hook 'emacs-lisp-mode-hook
;          (lambda ()
;            (face-remap-add-relative
;             'eyebrowse-mode-line-active '(( ((background dark)) :foreground "navajo4")
;				          ( ((background light)) :foreground "navajo4"))))) ;;"navajo0"


;; colors

;; Theme for ELisp Mode Only
;(add-hook 'emacs-lisp-mode-hook 
;          (lambda ()
;           (face-remap-add-relative
;            'mode-line-inactive '((:foreground "sienna"
;                                   :background "wheat"
;                                   ;;:background "#166451"
;                                   :box '(:line-width 20 :color "DarkOrange2")
;                                   :overline nil
;                                   :underline nil) mode-line))))
;(add-hook 'emacs-lisp-mode-hook
;          (lambda ()
;            (face-remap-add-relative
;             'mode-line '((:foreground "sienna"
;                           :background "beige"
;                           :box '(:line-width 4 :color "#90b4b0")
;                           :overline nil
;                           :underline nil) mode-line-inactive))))


(custom-set-faces
   '(mode-line-inactive ((t (:foreground "#737063"
                             :background "snow"
                             ;:background "#166451"
                             :box '(:line-width 20 :color "DarkOrange2")
                             :overline nil
                             :underline nil))))
   '(mode-line ((t (:foreground "sienna"
                    :background "beige"
                    :box '(:line-width 4 :color "#90b4b0")
                    :overline nil
                    :underline nil))))
   ;'(minibuffer-prompt (( ((background  dark)) :foreground "navajo4")
   ;	            	      ((background light)) :foreground "ivory")) ;; :background "yellow2", "DarkOrange2"
   '(selectrum-current-candidate ((t (:foreground "firebrick2"
                                      :background "LightGoldenrodYellow"))))

)

        ;; Tao Yang Modeline
        ;; foreground -> #737063
        ;; background -> #ECE9E0

        ;; Light purple :#a68ec7
        ;; Dark purple  :#4d3969 / 693963
        ;; Dark red     :#693947
        ;; Dark green   :#606939
        ;; Dark brown   :#644016
        ;; Dark ocean   :#166460
        ;; Dark ocean green :#166451
        ;; Other purple :#64165f
        ;; Grenn grey   :#90b4b0
        ;; Light green  :#80d9cf
        ;; Dark gree    :#4e9e95 / #4e9e76
        ;; DArk Orange  :#dfab7a / #e38328

        ;; Border       :ff0000(red)
        ;;              :ff8000(orange)
        ;;              :8000ff(prple)
        ;;              :8000ff (lg green)
        ;;              :29d65b/6fe32d (green)

(provide 'my_theming)
