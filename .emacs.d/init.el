
(debug-on-entry 'display-warning)

;; Just make the startup a little bit
;; less ugly, specially for lucid emacs
(menu-bar-mode -1)
;; Only for gtk emacs
;;(toggle-scroll-bar -1)
(tool-bar-mode -1)

;;; Treat all themes as safe
(setq custom-safe-themes t)

(require 'package)                   ; Bring in to the environment all package management functions

;; A list of package repositories
(setq package-archives '(("org"   . "https://orgmode.org/elpa/")
                         ("elpa"  . "https://elpa.gnu.org/packages/")))

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)                 ; Initializes the package system and prepares it to be used
(setq package-enable-at-startup nil)

(unless package-archive-contents     ; Unless a package archive already exists,
  (package-refresh-contents))        ; Refresh package contents so that Emacs knows which packages to load


;; Initialize use-package on non-linux platforms
(unless (package-installed-p 'use-package)        ; Unless "use-package" is installed, install "use-package"
  (package-install 'use-package))

(require 'use-package)                            ; Once it's installed, we load it using require
(setq use-package-always-ensure t)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
  (when (file-exists-p custom-file)
    (load custom-file))

(make-temp-file "custom-file")


(defvar emacs-dir (file-name-directory "/home/daniel/.emacs.d/")
  "The root dir of the Emacs distribution.")

(defvar packages-dir (expand-file-name "packages" emacs-dir)
  "This directory houses all of the custom packages.")

;; load just one file under .emacs.d folder
(load-file 
  (expand-file-name "my-add-subdirs-to-list.el"
                    emacs-dir))
(my-add-subdirs-to-list packages-dir 'load-path)

(defvar modules-dir (expand-file-name "modules" emacs-dir)
  "This directory houses all of the modules.")

(defvar keep-track-dir (expand-file-name "packages/keep-track" emacs-dir)
  "This directory houses all themes.")

(defvar themes-dir (expand-file-name "themes" emacs-dir)
  "This directory houses all themes.")


(add-to-list 'load-path modules-dir)
(add-to-list 'load-path packages-dir)
(add-to-list 'load-path themes-dir)
(add-to-list 'load-path keep-track-dir)

;; Run emacs in server mode always
;;(server-start)
;; That goes well with Frames only
;; mode
;; see https://github.com/davidshepherd7/frames-only-mode



;; Packages
;;;;(require 'module-solarized)
;;;;(require 'module-cyphejor-mode)

;;(require 'module-manage)
;;;;(require 'module-mythemes)
(require 'module-basics)
(require 'module-pkg-settings)
(require 'module-queries)
(require 'module-saving)

(require 'module-imenus)

;;;(require 'icomplete-plus)



(require 'module-indent)
;;;;(require 'module-cod-ext) ;;ein pynotebook
(require 'module-linters)

;; Org Mode
(require 'module-org)
;; Mail
;;(require 'module-mail)

;; Smart mode line
;;;; (require 'smart-mode-line)
;; Feebleline
;;;;(require 'module-modeline-related)
;; Mini Modeline
;;;;(require 'module-mini-modeline)
;

;; Barebones mode line
(require 'module-modeline)
(require 'module-modeline-related)

;; Splash screen
(require 'splash-screen)


;; Modal Editing
;;(require 'module-multistate)
;; Meow
;;(require 'module-meow)
;;(require 'module-modalka)
;; Ryo Mode
;;(require 'module-ryo-mode)
;; God mode
;;;;(require 'module-modehooks)

;; No commands before this point
(require 'module-buffers)
(require 'module-windowrules)
(require 'module-commonkeys)
;; Terminals
;;(require 'terminals)

;; Encoding
;;(prefer-coding-system 'raw-text-unix)
(setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding (Chinese/Japanese/Korean characters)
(set-default-coding-systems 'ascii-unix)
(set-language-environment 'ASCII)
(set-selection-coding-system 'ascii-unix)
(setq locale-coding-system 'ascii-unix)
;;(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
;; Set encoding
(setq prefer-coding-system 'ascii)
(setq coding-system-for-read 'ascii)
(setq coding-system-for-write 'ascii)
