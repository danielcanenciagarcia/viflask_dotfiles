
;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold most-positive-fixnum)

;; Supress package cl is deprecated error
(setq byte-compile-warnings '(cl-functions))

;; Prevent the glimpse of un-styled Emacs by disabling these UI elements early.
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; make the left fringe 4 pixels wide and the right disappear
;; Used for ibuffer etc
(fringe-mode 00)


;; Ignore X resources; its settings would be redundant with the other settings
;; in this file and can conflict with later config (particularly where the
;; cursor color is concerned).
;; (advice-add #'x-apply-session-resources :override #'ignore)
(setq inhibit-x-resources nil)

;; Dont bother me except if it is an error
;(setq warning-minimum-level :emergency)


;; Set theme
;; Make background transparent TERMINAL;
;; terminal light ==> emacs light ...
;;(defun set-background-for-terminal (&optional frame)
;;  (or frame (setq frame (selected-frame)))
;;  "unsets the background color in terminal mode"
;;  (unless (display-graphic-p frame)
;;    (set-face-background 'default "unspecified-bg" frame)))
;;(add-hook 'after-make-frame-functions 'set-background-for-terminal)
;;(add-hook 'window-setup-hook 'set-background-for-terminal)

; TODO: It's more complicated than the above.  I have to set the :background color
;;       to "black" whether I'm in the GUI or terminal.  If I don't, then the colors
;;       are less saturated than I like.  The amount of saturation is something like:
;;
;;       least   frame-background-mode 'dark    default :background "black"              orange comments
;;               frame-background-mode 'dark    default face (no background specified)   orange comments
;;               frame-background-mode 'light   default face (no background specified)   red comments
;;       most    frame-background-mode 'light   default :background "black"              deep red comments
;;
;;       The first 2 ones may be the same - I didn't compare side-by-side.
;;

;;(setq frame-background-mode 'light)
;;(set-face-attribute 'default nil :background "black")
;; Or
;;(set-face-attribute 'default nil :background 'unspecified)

;;(setq frame-background-mode 'dark)
;;(set-face-attribute 'default nil :background "dark")
;; Or
;;(set-face-attribute 'default nil :background 'unspecified)


;;Transparency GUI
;;(setq transparency_level 0)
;;(defun my:change_transparency ()
;;  "Toggles transparency of Emacs between 3 settings (none, mild, moderate)."
;;  (interactive)
;;  (if (equal transparency_level 0)
;;      (progn (set-frame-parameter (selected-frame) 'alpha '(75 . 85))
;;         (setq transparency_level 1))
;;    (if (equal transparency_level 1)
;;    (progn (set-frame-parameter (selected-frame) 'alpha '(50 . 85))
;;           (setq transparency_level 2))
;;      (if (equal transparency_level 2)
;;      (progn (set-frame-parameter (selected-frame) 'alpha '(100 . 85))
;;         (setq transparency_level 0)))
;;      )))
;;(define-key global-map (kbd "C-c t") 'my:change_transparency)
;; Startup
;;(add-to-list 'default-frame-alist '(alpha . 90))
;; or
;;(set-frame-parameter (selected-frame) 'alpha '(85 85))
;;(add-to-list 'default-frame-alist '(alpha 85 85))


;; Needed for Themes
;;(load-file "/home/daniel/.emacs.d/packages/keep-track/dash.el")
;;(load-file "/home/daniel/.emacs.d/packages/keep-track/autothemer.el")


;; help font in org-mode, etc -> see poet theme github
;;(add-hook 'text-mode-hook
;;           (lambda ()
;;            (variable-pitch-mode 1)))
;; Custom fonts
;;(set-face-attribute 'default nil :family "DejaVu Sans Mono" :height 130)
;;(set-face-attribute 'fixed-pitch nil :family "DejaVu Sans Mono")
;;(set-face-attribute 'variable-pitch nil :family "IBM Plex Serif")

;;(load-file "~/.emacs.d/themes/tao-theme.el")
;;(load-file "~/.emacs.d/themes/tao-yang-theme.el")
;;(load-file "~/.emacs.d/themes/taylor-theme.el")
;;(load-file "~/.emacs.d/themes/madhat2r-theme.el")
(load-file "~/.emacs.d/themes/arjen-theme.el")
;; arjen, billw, boron, brutalist-dark, bubbleberry,
;; clarity, dark-erc, dark-gnus, dark-latop, darkburn,
;; distinguished, deep-blue, fantom, firecode, goldenrod,
;; hamburg, hc-zenburn, hemisu-dark, hemisu, hickey,
;; hober, hydrangealess(custom-color), 
;; ir-black, jsc-dark, julie, junio, kosmos, labburn,
;; late-night, lavenderless, lawrence, ld-dark,
;; lush, majapahit-dark, mandm, mbo, mellow
;; metalheart, midnight, minimal-dark, minsk,
;; monochrome, mustard, naysayer, naquadah,
;; nimbus, nofrils-dark, nordless, nyx,
;; parchment, peacock, poet-dark-monochrome,
;; poet-dark, pok-wob, reverse, rhuk, rimero,
;; sexy-monochrome, sketch-black, taming-mr-arneson,
;; taylor, tech49, ujelly, wilson, manoj-dark, misterioso,
;; modus-vivandi, tango-dark, tsdh-dark, wheatgrass, 
;; wombad


;; light: feng-shui, tao-yang, gandalf, greiner,
;; greymatters, habamax, hemisu-light, leuven, mac-classic,
;; majapahit-light, minimal-light, monotropic,
;; nofrils-light, nothing, oldlace, paper, pastelmac,
;; pierson, ritchie, sitaramv-nt, soft-stone, tommyh,
;; whateveryouwant, dichromacy, modus-operandi, tango,
;; whiteboard, 


;;(add-to-list 'load-path "/home/daniel/.emacs.d/themes")
;;(dolist (file (directory-files "/home/daniel/.emacs.d/themes"))
;;  (unless (or (string= "." file)
;;	      (string= ".." file))
;;    (load (file-name-sans-extension file))))


;; NO THEME
;;(global-font-lock-mode 0)

;;Dark
;;(load-theme 'poet-dark t)
;;(load-theme 'poet-dark-monochrome t)
;;(load-theme 'tao-yin t)
;;(load-theme 'Seventies t)
;;(load-theme 'lavenderless t)
;;(load-theme 'inverse-acme t)
;;(load-theme 'goldenrod t)
;;(load-theme 'bliss t) ;; bright n c
;;(load-theme 'assemblage t) ;; bright g c
;;(load-theme 'distinguished t) ;;highlight some var
;;(load-theme 'chocolateless t) ;;brown
;;(load-theme 'clues t) ;;blue
;;(load-theme 'ciapre-black t)
;;(load-theme 'quasi-monochrome t) ;;blue comments
;;(load-theme 'northcode t) ;; orange, blue
;;(load-theme 'Seventies t) ;; (yellow bright)
;;(load-theme 'darkless t) ;; grey, high c
;;(load-theme 'eltbus t) ;; (high s)
;;(load-theme 'sexy-monochrome t) ;;blue / l c
;;(load-theme 'bone-a-kite t) ;; color / h
;;(load-theme 'monochrome t) ;; blue-mono
;;(load-theme 'ld-dark t) ;; grenn-color
;;(load-theme 'lavenderless t) ;;purple h c
;;(load-theme 'laguna t) ;; ** blue-green
;;(load-theme 'junio t) ;; ir-black?
;;(load-theme 'kooten t) ;; red c
;;(load-theme 'darkburn t) ;; g c / low color
;;(load-theme 'metalheart t)
;;(load-theme 'hickey t) ;; blue
;;(load-theme 'jazz t)
;;(load-theme 'late-night t) ;; mono - loww color
;;(load-theme 'jsc-dark t) ;; bright


;; both -> yao(mono), eziam(mono ul / **light), tsdh(color), punpun(light gtk), hemisu(green),
;; hydandata, nofrils{acme(g comments), dark(l comt), darkless(mono, gc),
;; 	              light(mono, lc), sepia(l gc)},
;; sketch-{black,white}(light comments),
;; almost-mono-{cream, black, gray, white}


;; ------------------------------------------------------------
;; (zenburn type) -> labburn, hamburg, hc-zenburn, fantom,
;;                   firebelly(highstrings),
;;                   firecode, foggy-night, creamsody(yellow-blue)
;; (orange) -> tangerine, retro-orange, busybee
;; (green) -> retro-green, lawrence, bliss, assemblage
;; (yellow) -> ***inverse-acme, goldenrod, mellow(ul vars), jsc-dark
;; (blue / purple) -> hydrangealess, broceliande, noctilux


;; Bright mono

;;(load-theme 'retro-orange t) ;;
;;(load-theme 'tangerine t) ;; 
;;(load-theme 'busybee t) ;; 
;;(load-theme 'lawrence t) ;; ** green colors mono
;;(load-theme 'retro-green t) ;;
;;(load-theme 'goldenrod t) ;; orange-gold
;;(load-theme 'hydrangealess t) ;; prple c, blue mono
;;(load-theme 'broceliande t) ;; sea green colors mono


;; So black / So light / GC

;;(load-theme 'snow t) ;;
;;(load-theme 'snowish t) ;;
;;(load-theme 'taylor t) ;; *
;;(load-theme 'majapahit-light t) ;; 
;;(load-theme 'majapahit-dark t) ;; 
;;(load-theme 'oswald t) ;; nah
;;(load-theme 'reverse t) ;; nah
;;(load-theme 'ritchie t) ;; *
;;(load-theme 'taming-mr-arneson t) ;; *
(load-theme 'arjen t) ;; **
;;(load-theme 'greiner t) ;; ***
;;(load-theme 'snowish t) ;; 
;;(load-theme 'black-on-gray t) ;; ***
;;(load-theme 'hober t) ;; **
;;(load-theme 'ujelly t) ;; *
;;(load-theme 'billw t) ;; 
;;(load-theme 'calm-forest t) ;; **
;;(load-theme 'clarity t) ;; ***
;;(load-theme 'classic t) ;; **
;;(load-theme 'julie t)
;;(load-theme 'whateveryouwant t)
;;(load-theme 'sitaramv-nt t) ;; ****
;;(load-theme 'sitaramv-solaris t) ; ***
;;(load-theme 'rotor t) ;; *
;;(load-theme 'pok-wob t) ;; ****
;;(load-theme 'pok-wog t) ;; ***
;;(load-theme 'pierson t) ;;
;;(load-theme 'kingsajz t) ;;
;;(load-theme 'desert t) ;;
;;(load-theme 'dark-gnus t) ;; 
;;(load-theme 'dark-green t) ;; 
;;(load-theme 'dark-info t) ;; 
;;(load-theme 'dark-laptop t) ;; 
;;(load-theme 'deep-blue t) ;; 
;;(load-theme 'dark-erc t) ;; 
;;(load-theme 'dark-font-lock t) ;; 
;;(load-theme 'charcoal-black t) ;; 


;; Light
;;(load-theme 'paper t) ;; w red
;;(load-theme 'soft-stone t) ;; ** acme - mono
;;(load-theme 'gandalf t) ;; *** color, l c
;;(load-theme 'white t) ;; **** h c, mono
;;(load-theme 'notink t) ;; gray
;;(load-theme 'nothing t) ;; ** mono, lc
;;(load-theme 'einkless t) ;; **** mono, h strings g c 
;;(load-theme 'eink t);; **** mono, h c
;;(load-theme 'seagreenless t) ;; ** green c
;;(load-theme 'monotropic t) ;; ** mono l c
;;(load-theme 'greymatters t) ;; * mono, some blue, l c
;;(load-theme 'dakrone-light t) ;; * mono, some colors
;;(load-theme 'cloud t) ;; * blue modeline, some color
;;(load-theme 'feng-shui t) ;; ***** hg comments, str
;;(load-theme 'oldlace t) ;; ** mono, some blue
;;(load-theme 'sunny-day t) ;; acme, bright, mono
;;(load-theme 'whiteboard t) ;; blue, green
;;(load-theme 'katester t) ;; **** h c, u f
;;(load-theme 'habamax t) ;; ***blue
;;(load-theme 'parchment t) ;; ***mono, gray modeline
;;(load-theme 'white-sand t) ;; **red, l c
;;(load-theme 'silkworm t) ;; *color, l c
;;(load-theme 'plan9 t) ;; * acme, b m, g c
;;(load-theme 'light-soap t) ;; *** acme, l c, mono
;;(load-theme 'brutal t) ;; ** mono l c
;;(load-theme 'brutalist t) ;; *** mono, some blue, u
;;(load-theme 'monochrome-bright t) ;; * lc
;;(load-theme 'pastelmac t) ;; ** some color, g c
;;(load-theme 'occidental t) ;; * acme, color
;;(load-theme 'mac-classic t) ;; *** gc, blue, grey
;;(load-theme 'acme t) ;; ** a mono, n c
;;(load-theme 'chyla t) ;; ** green mod, lc
;;(load-theme 'mccarthy t) ;; *** n c, a mono
;;(load-theme 'minimal-light t) ;; * mono, lc
;;(load-theme 'iodine t) ;; *** l c, color, prple
;;(load-theme 'doneburn t) ;; *** a mono, n c
;;(load-theme 'faff t) ;; gc acme
;;(load-theme 'leuven t) ;; *** blue m, l c
;;(load-theme 'organic-green t) ;; ** purple-ornge, l c
;;(load-theme 'mistyday t) ;; **** h c, n c, b mod
;;(load-theme 'hemera t) ;; ** lc, bright cols
;;(load-theme 'tommyh t) ;; **** g c, b mod
;;(load-theme 'dichromacy t) ;; *** blue-ornge,  g c
;;(load-theme 'adwaita t) ;; ** n c, green-ble


;; Light
;;(load-theme 'tao-yang t)
;;(load-theme 'light-blue t)
;;(load-theme 'poet t)
;;(load-theme 'poet-monochrome t)
;;(load-theme 'tommyh t)
;;(load-theme 'eink t)


;; Some color
;;(load-theme 'naysayer t) ;; ** bright green / g c
;;(load-theme 'mellow t) ;; brown, yellow
;;(load-theme 'rhuk t) ;; ** orange
;;(load-theme 'madhat2r t) ;; ** 3025 l c
;;(load-theme 'minsk t) ;; ** green zenburn g c
;;(load-theme 'mustard t) ;; ** 3024 gc
;;(load-theme 'mustang t)
;;(load-theme 'pok-wob t) ;; ** yellow b c
;;(load-theme 'purple-haze t) *
;;(load-theme 'tech49 t) ;; *l c, no h
;;(load-theme 'spacegray t)
;;(load-theme 'seoul256 t) ;; ******
;;(load-theme 'reykjavik t) ;; *zenburn
;;(load-theme 'rimero t) ;; **zenburn bright 
;;(load-theme 'odersky t) ;; *blue-orange zenburn
;;(load-theme 'erosiond t) ;; ***
;;(load-theme 'mbo t) ;; ****
;;(load-theme 'mbo70s t) ;; zenburn mbo
;;(load-theme 'manoj-dark t) ;; **** colors - orange comments
;;(load-theme 'mandm t) ;; blue-green ****
;;(load-theme 'wilson t) ;; brown ****
;;(load-theme 'lethe t) ;; base16 g c
;;(load-theme 'suscolors t) ;; base16 zenburn
;;(load-theme 'deeper-blue t) ;; blue-green ****
;;(load-theme 'paganini t) ;; bright green-orange *****
;;(load-theme 'midnight t) ;; base16 zenburn
;;(load-theme 'peacock t) ;; zenburn orange-blue un var ***
;;(load-theme 'lush t) ;; 3024 g c ***
;;(load-theme 'wheatgrass t) ;; green-yllow g c ******
;;(load-theme 'billw t) ;; gc yllow-blue ******
;;(load-theme 'ir-black t) ;; purple-green ****



;; Base16
;;atelier-cave-light, atelier-dune-light, base16-atelier-estuary-light
;;base16-forest-light
;; only gtk -> (load-theme 'punpun-light t)



;; Load theming modifications before init.el
;;(load-file "~/.emacs.d/packages/my_theming.el")
;;(require 'my_theming)
