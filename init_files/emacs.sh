#!/bin/bash

# make CapsLock behave like Ctrl:
#setxkbmap -option ctrl:nocaps
#setxkbmap -option caps:swapescape

#xmodmap -e "keysym Caps_Lock = Escape" #set caps_lock as escape

xmodmap ~/ports/viflask_dotfiles/init_files/normal_emacs
# make short-pressed Ctrl behave like Escape:
xcape -e '#66=Escape'
#xcape -e 'Control_L=Escape'
#setxkbmap -option caps:noescape
